//---------------------------------------------------------------------------

#ifndef OperatoreH
#define OperatoreH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.OleCtrls.hpp>
#include <Vcl.OleServer.hpp>
#include "ColorImg.hpp"
#include <Vcl.Graphics.hpp>
#include "LayMU.h"
#include "MyShape.h"
//---------------------------------------------------------------------------
class TOperatoreForm : public TForm
{
__published:	// IDE-managed Components
	TDBCheckBox *DBCheckBox2;
	TPanel *Panel7;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TGroupBox *GroupBox1;
	TComboBox *ComboBox1;
	TStaticText *StaticText8;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit1;
	TSpinButton *SpinButton1;
	TTimer *TimerOverride;
	TCheckBox *CheckBox4;
	TTimer *TimerUpd;
	TBitBtn *BitBtn5;
	TGroupBox *GroupBox3;
	TShape *Shape4;
	TShape *Shape5;
	TShape *Shape6;
	TStaticText *StaticText10;
	TStaticText *StaticText11;
	TStaticText *StaticText12;
	TLabel *Label5;
	TShape *Shape1;
	TShape *Shape2;
	TStaticText *StaticText2;
	TStaticText *StaticText3;
	TfrmLayMU *frmLayMU1;
	TMemo *mProgramMU;
	TPanel *pnlLampeggia;
	TBitBtn *BitBtn6;
	TPanel *Panel2;
	TBitBtn *BitBtn1;
	TPanel *Panel4;
	TCheckBox *CheckBox1;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
	void __fastcall LabeledEdit2Click(TObject *Sender);
	void __fastcall SpinButton1DownClick(TObject *Sender);
	void __fastcall SpinButton1UpClick(TObject *Sender);
	void __fastcall TimerOverrideTimer(TObject *Sender);
	void __fastcall CheckBox4MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall LabeledEdit2Change(TObject *Sender);
	void __fastcall BitBtn5Click(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall BitBtn6MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall mProgramMUClick(TObject *Sender);
	void __fastcall MyShape1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall CheckBox1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
private:	// User declarations
public:		// User declarations
	__fastcall TOperatoreForm(TComponent* Owner);
	void CaricaComboBox(TComboBox *cb, int n);
	void ColoraPallet(TMyShape *i, bool presenza, bool disponibile, bool lavorato, bool missione, bool stato);
	void Aggiorna();
	int TrovaBufferLibero();
};
//---------------------------------------------------------------------------
extern PACKAGE TOperatoreForm *OperatoreForm;
//---------------------------------------------------------------------------
#endif
