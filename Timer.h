//---------------------------------------------------------------------------

#ifndef TimerH
#define TimerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Touch.Keyboard.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TTimerForm : public TForm
{
__published:	// IDE-managed Components
	TDBCheckBox *DBCheckBox2;
	TPanel *Panel7;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TTimer *Timer1;
	TTimer *TimerUpd;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TLabeledEdit *LabeledEdit6;
	TLabeledEdit *LabeledEdit7;
	TLabeledEdit *LabeledEdit8;
	TLabeledEdit *LabeledEdit9;
	TLabeledEdit *LabeledEdit10;
	TLabeledEdit *LabeledEdit11;
	TLabeledEdit *LabeledEdit13;
	TLabeledEdit *LabeledEdit14;
	TLabeledEdit *LabeledEdit15;
	TLabeledEdit *LabeledEdit16;
	TLabeledEdit *LabeledEdit12;
	TLabeledEdit *LabeledEdit17;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall LabeledEdit1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TTimerForm(TComponent* Owner);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TTimerForm *TimerForm;
//---------------------------------------------------------------------------
#endif
