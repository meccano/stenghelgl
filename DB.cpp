//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "inifiles.hpp"
#include "DB.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDBDataModule *DBDataModule;
//---------------------------------------------------------------------------
Variant ReadField(TADOQuery *q, AnsiString f)
{
	Variant res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------
int ReadInt(TADOQuery *q, AnsiString f)
{
	int res = 0;

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------
AnsiString ReadString(TADOQuery *q, AnsiString f)
{
	AnsiString res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->AsString;
	return res;
}
//---------------------------------------------------------------------------
__fastcall TDBDataModule::TDBDataModule(TComponent* Owner)
	: TDataModule(Owner)
{
	TIniFile *Ini;
	AnsiString ConnectionString;
	TCursor Save_Cursor = Screen->Cursor;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	ADOConnection1->Close();
	ConnectionString = Ini->ReadString("Database", "ConnectionString", "");
	if (ConnectionString != "")
		ADOConnection1->ConnectionString = ConnectionString;
	delete Ini;
	try {
		ADOConnection1->Open();
		AcquisisciSegnalazioni();
	} catch (...) {}
	Screen->Cursor = Save_Cursor;
	TimerDBCheck->Enabled = true;
	TimerClr->Enabled = true;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabella(AnsiString TableName, TRecordList &RecList) {
	AnsiString strsql, campo;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[i][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {}
   	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList) {
	AnsiString strsql, campo, chiave;
	TADOQuery *ADOQuery;
	Variant k;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				chiave = ADOQuery->FieldByName(KeyField)->AsString;
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[chiave][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::SalvaProgramma(AnsiString prog) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int i, affected;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Programmi (Programma) VALUES ('%s')", prog.c_str());
		ADOQuery->SQL->Text = strsql;
		affected = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::EliminaProgramma(AnsiString prog) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int i, affected;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Programmi WHERE Programma = '%s'", prog.c_str());
		ADOQuery->SQL->Text = strsql;
		affected = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::SalvaParametri(WORD *MW) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int i, affected;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 30; i < 423; i++) {
			strsql.printf("UPDATE Parametri SET Valore = %d WHERE Indice = %d", MW[i], i);
			ADOQuery->SQL->Text = strsql;
			affected = ADOQuery->ExecSQL();
			if (affected == 0) {
				strsql.printf("INSERT INTO Parametri (Indice, Valore) VALUES (%d,%d)", i, MW[i]);
				ADOQuery->SQL->Text = strsql;
				affected = ADOQuery->ExecSQL();
			}
		}
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


void TDBDataModule::LeggiParametri(WORD *MW) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int i, val;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Parametri");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			i = ReadInt(ADOQuery, "Indice");
			val = ReadInt(ADOQuery, "Valore");
			MW[i] = val;
			ADOQuery->Next();
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


void TDBDataModule::LeggiSegnalazioneAttiva(AnsiString &msg, int &all) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		msg = "";
		all = 0;
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP 1 * FROM Segnalazioni WHERE Acquisito IS NULL ORDER BY DataOra");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			msg = ReadString(ADOQuery, "Messaggio");
			all = ReadInt(ADOQuery, "Allarme");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


void TDBDataModule::AcquisisciSegnalazioneAttiva(int nmsg) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Segnalazioni SET Acquisito = Now() WHERE (N = '%d') AND Acquisito IS NULL", nmsg);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


void TDBDataModule::AcquisisciSegnalazioni() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Segnalazioni SET Acquisito = Now() WHERE Acquisito IS NULL");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


void TDBDataModule::Segnalazione(int n, AnsiString msg, int all) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP 1 * FROM Segnalazioni WHERE (N = %d) AND (Acquisito IS NULL)", n);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� inviato
			ADOQuery->Close();
			delete ADOQuery;
			return;
		}
		ADOQuery->Close();
		strsql.printf("INSERT INTO Segnalazioni (N, Messaggio, Allarme) VALUES (%d, '%s', %d)", n, msg, all);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}

void __fastcall TDBDataModule::TimerDBCheckTimer(TObject *Sender)
{
	AnsiString strsql;

	TimerDBCheck->Enabled = false;
	try {
		if (!ADOConnection1->Connected) {
			ADOConnection1->Open();
		}
	} catch(...) {};
	TimerDBCheck->Enabled = true;
}
//---------------------------------------------------------------------------

void TDBDataModule::MemorizzaProgrammaPallet(int pallet, AnsiString prg) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET PartProgram = '%s' WHERE (Pallet = %d)", prg, pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

AnsiString TDBDataModule::LeggiProgrammaPallet(int pallet) {
	AnsiString strsql, res = "";
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet WHERE (Pallet = %d)", pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadString(ADOQuery, "PartProgram");
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::ImpostaStatoPallet(int pallet, int stato) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET Stato = %d WHERE (Pallet = %d)", stato, pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiStatoPallet(int pallet) {
	AnsiString strsql;
	int res = 0;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet WHERE (Pallet = %d)", pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "Stato");
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiPalletInMacchina() {
	AnsiString strsql;
	int res = 0;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet WHERE (InMacchina <> 0)");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "Pallet");
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::ImpostaPalletInMacchina(int pallet, int InMacchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET InMacchina = %d WHERE (Pallet = %d)", InMacchina, pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::NessunPalletInMacchina() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET InMacchina = 0");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::MemorizzaEsclusioneProgramma(int val) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Impostazioni SET ProgrammaEscluso = %d", val);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::MemorizzaEsclusioneM30(int val) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Impostazioni SET M30Escluso = %d", val);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------


int TDBDataModule::LeggiEsclusioneProgramma() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Impostazioni");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ProgrammaEscluso");
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------
int TDBDataModule::LeggiEsclusioneM30() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Impostazioni");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "M30Escluso");
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::ClearHistory() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Segnalazioni WHERE h.HisDate < (Now() - 60)");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------



void __fastcall TDBDataModule::TimerClrTimer(TObject *Sender)
{
	TimerClr->Enabled = false;
	ClearHistory();
	TimerClr->Enabled = true;
}
//---------------------------------------------------------------------------

