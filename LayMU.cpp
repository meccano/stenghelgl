//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LayMU.h"
#include "Main.h"
#include <System.Math.hpp>
#include "math.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "MyShape"
#pragma resource "*.dfm"
TfrmLayMU *frmLayMU;
//---------------------------------------------------------------------------

__fastcall TfrmLayMU::TfrmLayMU(TComponent* Owner)
	: TFrame(Owner)
{
	int i, x, y;
	float a, a0;

	InitWidth = Width;
	InitHeight = Height;
	a0 = 0;          // MU DX
//	a0 = M_PI;       // MU SX
//	a0 = -M_PI_2;    // OP
	P = new TMyShape*[MainForm->NPallet];
	for(i = 0; i < MainForm->NPallet; i++) {
		a =  (2 * M_PI / MainForm->NPallet) * i + a0;
		x = Width / 2 + MainForm->RaggioTavola * Cos(a) - MainForm->DimPallet / 2 + MainForm->OffsetX;
		y = Height / 2 - MainForm->RaggioTavola * Sin(a) - MainForm->DimPallet / 2 + MainForm->OffsetY;
		P[i] = (TMyShape*)new TMyShape(this);
		P[i]->Parent = this;
		P[i]->Name = "P" + IntToStr(i + 1);
		P[i]->Top = y;
		P[i]->Left = x;
		P[i]->Width = MainForm->DimPallet;
		P[i]->Height = MainForm->DimPallet;
		P[i]->Angle = RadToDeg(-a);
		P[i]->Shape = mstRoundSquare;
		P[i]->Pen->Width = 3;
		P[i]->Font->Size = 36;
		P[i]->Font->Color = clSkyBlue;
		P[i]->Font->Style = TFontStyles() << fsBold;
		P[i]->Text = i + 1;
	}
	imgMU->Picture->LoadFromFile(MainForm->ImgSfondoMU);
	imgOP->Picture->LoadFromFile(MainForm->ImgSfondoOP);
}
//---------------------------------------------------------------------------

void TfrmLayMU::Repos(int Tipo)
{
	int i, x, y;
	float a, a0, k;

	switch(Tipo) {
	case 0:
		a0 = 0;          // MU DX
		imgMU->Visible = true;
		imgOP->Visible = false;
		break;
	case 1:
		a0 = M_PI;       // MU SX
		imgMU->Visible = true;
		imgOP->Visible = false;
		break;
	default:
		a0 = -M_PI_2;    // OP
		imgMU->Visible = false;
		imgOP->Visible = true;
		break;
	}
	k = ((float)Width / (float)InitWidth + (float)Height/(float)InitHeight) / 2.0;
	for(i = 0; i < MainForm->NPallet; i++) {
		a =  (2 * M_PI / MainForm->NPallet) * i + a0;
		x = InitWidth / 2 + MainForm->RaggioTavola * Cos(a) - MainForm->DimPallet / 2 + MainForm->OffsetX;
		y = InitHeight / 2 - MainForm->RaggioTavola * Sin(a) - MainForm->DimPallet / 2 + MainForm->OffsetY;
		P[i]->Top = y * k + 0.5;
		P[i]->Left = x * k + 0.5;
		P[i]->Width = MainForm->DimPallet * k + 0.5;
		P[i]->Height = MainForm->DimPallet * k + 0.5;
		P[i]->Angle = RadToDeg(-a);
		P[i]->Font->Size = 16 + 20 * k; // + 36;
	}
}
//---------------------------------------------------------------------------

__fastcall TfrmLayMU::~TfrmLayMU() {
	int i;

	for(i = 0; i < MainForm->NPallet; i++) {
		delete P[i];
	}
	delete []P;
}
//---------------------------------------------------------------------------

