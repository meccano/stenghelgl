//---------------------------------------------------------------------------
#pragma hdrstop

#include "stdio.h"
#include <System.IOUtils.hpp>
#include <System.SyncObjs.hpp>
#include "io.h"
#include "time.h"
#include "LogTxt.h"
#include <System.DateUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
FILE *logfile;
AnsiString ultimo_log = "";
TCriticalSection *Lock = new TCriticalSection();
//---------------------------------------------------------------------------

int	ApriLog() {
	time_t t;
	struct tm *lt;
	AnsiString filename;
	char timestr[100];

	t = time(NULL);
	lt = localtime(&t);
	if (!DirectoryExists("Log")) {
		CreateDir("Log");
	}
	strftime(timestr, 100, "Log\\%Y%m%d.log", lt);
	filename = timestr;
	if ((logfile = fopen(filename.c_str(), "a+")) == NULL) {
		return 1;
	}
	return 0;
}
//---------------------------------------------------------------------------

void ChiudiLog()
{
	fflush(logfile);
	fclose(logfile);
}
//---------------------------------------------------------------------------

void LogTxt(AnsiString logstr)
{
	time_t t;
	struct tm *lt;
	char timestr[100];

	Lock->Acquire();
	try {
		if (logstr != ultimo_log) {
			if (ApriLog() == 0) {
				t = time(NULL);
				lt = localtime(&t);
				strftime(timestr, 100, "%H:%M:%S", lt);
				fprintf(logfile, "[%s.%03d] %s\n", timestr, MilliSecondOfTheSecond(Now()),logstr.c_str());
				ChiudiLog();
				ultimo_log = logstr;
			}
        }
	} catch (...) {}
	Lock->Release();
}
//---------------------------------------------------------------------------

void LogTxt(char *s, ...)
{
	va_list argptr;
	AnsiString logstr;

    //Costruisco la stringa
    va_start(argptr, s);
    logstr.vprintf(s, argptr);
    va_end(argptr);

    //Log
    LogTxt(logstr);
}
//---------------------------------------------------------------------------

void CancellaLogVecchi(int giorni)
{
	TStringDynArray files;
	time_t t;
	char timestr[100];
	struct tm *lt;
	AnsiString FileName, Limit;
	double secondi;

	secondi = giorni * 24 * 60 * 60;
	t = time(NULL) - (long)secondi;
	lt = localtime(&t);
	strftime(timestr, 100, "%Y%m%d.log", lt);
	Limit = timestr;

	if (!DirectoryExists("Log")) {
		return;
	}
	files = TDirectory::GetFiles("Log", "*.*", TSearchOption::soTopDirectoryOnly);
	for (int i = 0; i < files.Length; i++) {
		FileName = ExtractFileName(files[i]);
		if (FileName <  Limit) {
        	DeleteFile(files[i]);
		}
	}
}
//---------------------------------------------------------------------------
