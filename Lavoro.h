//---------------------------------------------------------------------------

#ifndef LavoroH
#define LavoroH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Touch.Keyboard.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.ComCtrls.hpp>
#include "ColorImg.hpp"
#include <Vcl.Graphics.hpp>
#include "LayMU.h"
//---------------------------------------------------------------------------
class TLavoroForm : public TForm
{
__published:	// IDE-managed Components
	TDBCheckBox *DBCheckBox2;
	TPanel *Panel7;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TTimer *Timer1;
	TTimer *TimerUpd;
	TGroupBox *GroupBox1;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TBitBtn *BitBtn1;
	TGroupBox *GroupBox2;
	TComboBox *ComboBox4;
	TStaticText *StaticText5;
	TBitBtn *BitBtn5;
	TBitBtn *BitBtn6;
	TBitBtn *BitBtn7;
	TStaticText *StaticText6;
	TCheckBox *CheckBox1;
	TBitBtn *BitBtn8;
	TfrmLayMU *frmLayMU1;
	TGroupBox *GroupBox3;
	TStaticText *StaticText1;
	TComboBox *ComboBox1;
	TStaticText *StaticText2;
	TComboBox *ComboBox2;
	TStaticText *StaticText3;
	TComboBox *ComboBox3;
	TBitBtn *BitBtn4;
	TBitBtn *BitBtn2;
	TGroupBox *GroupBox4;
	TStaticText *StaticText4;
	TComboBox *ComboBox5;
	TGroupBox *GroupBox5;
	TStringGrid *StringGrid1;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
	void __fastcall ComboBox2Change(TObject *Sender);
	void __fastcall ComboBox3Change(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall BitBtn4Click(TObject *Sender);
	void __fastcall ComboBox4Change(TObject *Sender);
	void __fastcall BitBtn5Click(TObject *Sender);
	void __fastcall BitBtn6Click(TObject *Sender);
	void __fastcall CheckBox1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label1Click(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall StringGrid1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall BitBtn7MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall BitBtn7MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall ComboBox5Change(TObject *Sender);
	void __fastcall BitBtn8Click(TObject *Sender);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State);
	void __fastcall MyShape1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
private:	// User declarations
public:		// User declarations
	__fastcall TLavoroForm(TComponent* Owner);
	void CaricaComboBox(TComboBox *cb, int n);
	void ColoraPallet(TMyShape *i, bool presenza, bool disponibile, bool lavorato, bool missione, bool stato);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TLavoroForm *LavoroForm;
//---------------------------------------------------------------------------
#endif
