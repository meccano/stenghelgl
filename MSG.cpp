//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MSG.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMSGForm *MSGForm;
//---------------------------------------------------------------------------
__fastcall TMSGForm::TMSGForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMSGForm::Timer1Timer(TObject *Sender)
{
	Timer1->Enabled = false;
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TMSGForm::FormActivate(TObject *Sender)
{
	Timer1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMSGForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

