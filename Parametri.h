//---------------------------------------------------------------------------

#ifndef ParametriH
#define ParametriH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Touch.Keyboard.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TParametriForm : public TForm
{
__published:	// IDE-managed Components
	TDBCheckBox *DBCheckBox2;
	TPanel *Panel7;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TTimer *Timer1;
	TTimer *TimerUpd;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TLabeledEdit *LabeledEdit6;
	TLabeledEdit *LabeledEdit7;
	TLabeledEdit *LabeledEdit8;
	TLabeledEdit *LabeledEdit9;
	TLabeledEdit *LabeledEdit10;
	TLabeledEdit *LabeledEdit11;
	TLabeledEdit *LabeledEdit12;
	TLabeledEdit *LabeledEdit13;
	TLabeledEdit *LabeledEdit14;
	TLabeledEdit *LabeledEdit15;
	TLabeledEdit *LabeledEdit16;
	TLabeledEdit *LabeledEdit17;
	TTabControl *TabControl1;
	TLabeledEdit *LabeledEdit21;
	TLabeledEdit *LabeledEdit22;
	TLabeledEdit *LabeledEdit23;
	TLabeledEdit *LabeledEdit24;
	TLabeledEdit *LabeledEdit25;
	TLabeledEdit *LabeledEdit26;
	TLabeledEdit *LabeledEdit27;
	TLabeledEdit *LabeledEdit28;
	TLabeledEdit *LabeledEdit29;
	TLabeledEdit *LabeledEdit30;
	TLabeledEdit *LabeledEdit31;
	TLabeledEdit *LabeledEdit32;
	TLabeledEdit *LabeledEdit33;
	TLabeledEdit *LabeledEdit34;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TLabeledEdit *LabeledEdit18;
	TLabeledEdit *LabeledEdit19;
	TLabeledEdit *LabeledEdit20;
	TLabeledEdit *LabeledEdit35;
	TLabeledEdit *LabeledEdit36;
	TLabeledEdit *LabeledEdit37;
	TLabeledEdit *LabeledEdit38;
	TLabeledEdit *LabeledEdit39;
	TLabeledEdit *LabeledEdit40;
	TLabeledEdit *LabeledEdit41;
	TLabeledEdit *LabeledEdit42;
	TLabeledEdit *LabeledEdit43;
	TLabeledEdit *LabeledEdit44;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall LabeledEdit2Click(TObject *Sender);
	void __fastcall LabeledEdit1Click(TObject *Sender);
	void __fastcall LabeledEdit21Click(TObject *Sender);
	void __fastcall LabeledEdit24Click(TObject *Sender);
	void __fastcall LabeledEdit30Click(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall LEPalletNumberClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TParametriForm(TComponent* Owner);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TParametriForm *ParametriForm;
//---------------------------------------------------------------------------
#endif
