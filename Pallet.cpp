//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Pallet.h"
#pragma package(smart_init)

#define NESSUNA_PRENOTAZIONE	0
#define PRENOTAZIONE_DEPOSITO	1
#define PRENOTAZIONE_PRELIEVO  	2
#define POSTAZIONE_BLOCCATA  	3

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TPallet *)
{
        new TPallet(NULL);
}
//---------------------------------------------------------------------------
__fastcall TPallet::TPallet(TComponent* Owner)
        : TShape(Owner)
{
	Corsia = 0;
	Postazione = 0;
	Piano = 0;
	IdPallet = 0;
	Stato = 0;
	IdPostazione = 0;
    IdArea = 0;
}
//---------------------------------------------------------------------------
void __fastcall TPallet::SetIdPallet(long value)
{
	int pn;

	if (FIdPallet != value) {
		FIdPallet = value;
		if (FIdPallet == 0)
			Brush->Color = clWhite;
		else {
			pn = Piano & 0x0F;
			if (pn > 1)
				Brush->Color = 0x0050A0D0;
			else
				Brush->Color = 0x002570C0;
		}
	}
}
//------------------------------------------------------------------------------
void __fastcall TPallet::SetStato(int value)
{
	if (FStato != value) {
		FStato = value;
		switch(FStato) {
		case NESSUNA_PRENOTAZIONE:
			Pen->Color = clBlack;
			Pen->Width = 1;
			break;
		case PRENOTAZIONE_DEPOSITO:
			Pen->Color = clBlue;
			Pen->Width = 3;
			break;
		case PRENOTAZIONE_PRELIEVO:
			Pen->Color = clLime;
			Pen->Width = 3;
			break;
		case POSTAZIONE_BLOCCATA:
			Pen->Color = clRed;
			Pen->Width = 2;
			break;
		}
	}
}
//------------------------------------------------------------------------------
namespace Pallet{
	void __fastcall PACKAGE Register()
	{
		 TComponentClass classes[1] = {__classid(TPallet)};
		 RegisterComponents("Additional", classes, 0);
	}
}
//---------------------------------------------------------------------------

