//---------------------------------------------------------------------------

#ifndef ProgramSelectDBH
#define ProgramSelectDBH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------
class TProgramSelectDBDlg : public TForm
{
__published:	// IDE-managed Components
	TListBox *LBProgrammi;
	TLabeledEdit *LEProgrammaSelezionato;
	TBitBtn *BitBtn2;
	TBitBtn *BitBtn1;
	TLabel *Label1;
	TLabeledEdit *LabeledEdit1;
	TBitBtn *BitBtn4;
	TBitBtn *BitBtn3;
	TTimer *TimerUpd;
	TCheckBox *CheckBox1;
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall LBProgrammiClick(TObject *Sender);
	void __fastcall BitBtn4Click(TObject *Sender);
	void __fastcall LabeledEdit1Click(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TProgramSelectDBDlg(TComponent* Owner);
	void  CaricaProgrammi();
};
//---------------------------------------------------------------------------
extern PACKAGE TProgramSelectDBDlg *ProgramSelectDBDlg;
//---------------------------------------------------------------------------
#endif
