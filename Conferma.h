//---------------------------------------------------------------------------

#ifndef ConfermaH
#define ConfermaH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------
class TConfermaForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Testo;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TConfermaForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern int ConfermaDlg(AnsiString Titolo, AnsiString Testo);
extern PACKAGE TConfermaForm *ConfermaForm;
//---------------------------------------------------------------------------
#endif
