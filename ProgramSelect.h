//---------------------------------------------------------------------------

#ifndef ProgramSelectH
#define ProgramSelectH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------
class TProgramSelectDlg : public TForm
{
__published:	// IDE-managed Components
	TListBox *LBFolders;
	TListBox *LBFiles;
	TTimer *TimerUpd;
	TLabeledEdit *LEFolder;
	TLabel *Label1;
	TLabel *Label2;
	TLabeledEdit *LEFile;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall LBFoldersClick(TObject *Sender);
	void __fastcall LBFilesClick(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TProgramSelectDlg(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TProgramSelectDlg *ProgramSelectDlg;
//---------------------------------------------------------------------------
#endif
