//---------------------------------------------------------------------------

#ifndef PalletH
#define PalletH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TPallet : public TShape
{
	private:
		int FCorsia;
		int FPostazione;
		int FPiano;
		long FIdPallet;
		int FStato;
		int FIdPostazione;
		int FIdArea;
		void __fastcall SetIdPallet(long value);
		void __fastcall SetStato(int value);
	protected:
	public:
		__fastcall TPallet(TComponent* Owner);
	__published:
		__property int Corsia = {read=FCorsia, write=FCorsia};
		__property int Postazione = {read=FPostazione, write=FPostazione};
		__property int Piano = {read=FPiano, write=FPiano};
		__property long IdPallet = {read=FIdPallet, write=SetIdPallet};
		__property int Stato = {read=FStato, write=SetStato};
		__property int IdPostazione = {read=FIdPostazione, write=FIdPostazione};
		__property int IdArea = {read=FIdArea, write=FIdArea};
		__property PopupMenu;
		__property OnDblClick;
		__property OnDragOver;
		__property OnEndDrag;
		__property OnMouseUp;
		__property OnMouseDown;
};
//---------------------------------------------------------------------------
#endif
