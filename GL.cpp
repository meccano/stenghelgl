//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#pragma package(smart_init) // madExcept
#pragma link "madExcept"
#pragma link "madLinkDisAsm"
#pragma link "madListHardware"
#pragma link "madListProcesses"
#pragma link "madListModules"
//---------------------------------------------------------------------

#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
#include <IniFiles.hpp>
#include "Splash.h"
#include "gnugettext.hpp"
USEFORM("Password.cpp", PasswordDlg);
USEFORM("ProgramSelect.cpp", ProgramSelectDlg);
USEFORM("Parametri.cpp", ParametriForm);
USEFORM("MSG.cpp", MSGForm);
USEFORM("Operatore.cpp", OperatoreForm);
USEFORM("StoricoSegnalazioni.cpp", StoricoSegnalazioniForm);
USEFORM("Timer.cpp", TimerForm);
USEFORM("c:\program files (x86)\embarcadero\studio\14.0\ObjRepos\EN\Cpp\okcancl1.CPP", OKBottomDlg);
USEFORM("Splash.cpp", SplashForm);
USEFORM("ProgramSelectDB.cpp", ProgramSelectDBDlg);
USEFORM("Segnalazioni.cpp", SegnalazioniForm);
USEFORM("Sinottico.cpp", SinotticoForm);
USEFORM("Modbus.cpp", ModBusDM); /* TDataModule: File Type */
USEFORM("InsAlpha.cpp", InsAlphaForm);
USEFORM("InsNum.cpp", InsNumForm);
USEFORM("Chiusura.cpp", ChiusuraForm);
USEFORM("Conferma.cpp", ConfermaForm);
USEFORM("DB.cpp", DBDataModule); /* TDataModule: File Type */
USEFORM("main.cpp", MainForm);
USEFORM("Manuali.cpp", ManualiForm);
USEFORM("Lavoro.cpp", LavoroForm);
USEFORM("LayMU.cpp", frmLayMU); /* TFrame: File Type */
USEFORM("GenericKeyboard.cpp", fGenericKeyboard);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	HANDLE mutex;
	AnsiString MutexName = ExtractFileName(Application->ExeName).UpperCase();
	AnsiString lang;
	TIniFile *Ini;
	mutex = OpenMutex(MUTEX_ALL_ACCESS, false, MutexName.c_str());
	if (mutex == NULL) {
		mutex = CreateMutex(NULL, true, MutexName.c_str());
	} else {
		exit(EXIT_SUCCESS);
	}
	try
	{
		SplashForm = new TSplashForm(Application);
		SplashForm->Show();
		Application->Initialize();
		SplashForm->Update();
		SplashForm->Close();
		Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
		lang = Ini->ReadString("GENERALE", "Language", "it");
		delete(Ini);
		UseLanguage(lang);
		Application->CreateForm(__classid(TModBusDM), &ModBusDM);
		Application->CreateForm(__classid(TDBDataModule), &DBDataModule);
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TChiusuraForm), &ChiusuraForm);
		Application->CreateForm(__classid(TConfermaForm), &ConfermaForm);
		Application->CreateForm(__classid(TInsAlphaForm), &InsAlphaForm);
		Application->CreateForm(__classid(TInsNumForm), &InsNumForm);
		Application->CreateForm(__classid(TPasswordDlg), &PasswordDlg);
		Application->CreateForm(__classid(TSplashForm), &SplashForm);
		Application->CreateForm(__classid(TMSGForm), &MSGForm);
		Application->CreateForm(__classid(TProgramSelectDBDlg), &ProgramSelectDBDlg);
		Application->CreateForm(__classid(TfGenericKeyboard), &fGenericKeyboard);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------
