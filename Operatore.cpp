//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Operatore.h"
#include "ModBus.h"
#include "InsNum.h"
#include "conferma.h"
#include "DB.h"
#include "ProgramSelect.h"
#include "ProgramSelectDB.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LayMU"
#pragma link "MyShape"
#pragma resource "*.dfm"
TOperatoreForm *OperatoreForm;
//---------------------------------------------------------------------------
__fastcall TOperatoreForm::TOperatoreForm(TComponent* Owner)
	: TForm(Owner)
{
	int i;

	for(i = 0; i < MainForm->NPallet; i++) {
        frmLayMU1->P[i]->OnMouseDown = &MyShape1MouseDown;
    }
}
//---------------------------------------------------------------------------

void TOperatoreForm::CaricaComboBox(TComboBox *cb, int n)
{
	int i;

	cb->Clear();
	for (i = 1; i <= n ; i++) {
		cb->Items->Add(i);
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::FormActivate(TObject *Sender)
{
	WindowState = wsMaximized;
	CaricaComboBox(ComboBox1, MainForm->NPallet);

	CheckBox1->Checked = DBDataModule->LeggiEsclusioneM30();
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::BitBtn3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void TOperatoreForm::ColoraPallet(TMyShape *i, bool presenza, bool disponibile, bool lavorato, bool missione, bool stato)
{
	i->OnlyText = !presenza;
	if (missione) {
		i->Brush->Color = clBlue;
	} else if (disponibile) {
		i->Brush->Color = clGreen;
	} else if (lavorato) {
		if (stato == 1) {
			i->Brush->Color = clPurple;
		} else if (stato == 2) {
			i->Brush->Color = clMaroon;
		} else {
			i->Brush->Color = clRed;
		}
	} else {
		i->Brush->Color = clGray;
	}
}
//---------------------------------------------------------------------------

void TOperatoreForm::Aggiorna() {
	int pos, posm, poso, i, j, n, mw, bit;
	bool *presenza, *disponibile, *lavorato, *missione, *stato;
	TfrmLayMU* fm;
	TIndexList TabPallet;

	presenza = new bool[MainForm->NPallet];
	disponibile = new bool[MainForm->NPallet];
	lavorato = new bool[MainForm->NPallet];
	missione = new bool[MainForm->NPallet];
	stato = new bool[MainForm->NPallet];
	posm = ModBusDM->MW[855]; // MU
	poso = ModBusDM->MW[856]; // OP
	fm = frmLayMU1;
	DBDataModule->CaricaTabellaK("Pallet", "Pallet", TabPallet);
	if (posm) {
		pos = posm;
		fm->Repos(MainForm->PosMacchina);
	} else {
		pos = poso;
		fm->Repos(2);
	}
	n = (pos > 1) ? pos : 1;
	for(i = 0; i < MainForm->NPallet; i++) {
		fm->P[i]->Text = n;
		stato[i] = StrToIntDef(TabPallet[n]["Stato"], 0);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	n = (pos > 1) ? pos : 1;
	for(j = 0; j < MainForm->NPallet; j++) {
		mw = 891 + (n - 1) / 4;
		bit = (n - 1) % 4 * 4;
		presenza[j]    = ModBusDM->GetBit(mw, bit);
		disponibile[j] = ModBusDM->GetBit(mw, bit + 1);
		lavorato[j]    = ModBusDM->GetBit(mw, bit + 2);
		missione[j]    = ModBusDM->GetBit(mw, bit + 3);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	for(i = 0; i < MainForm->NPallet; i++) {
		ColoraPallet(fm->P[i], presenza[i], disponibile[i], lavorato[i], missione[i], stato[i]);
	}
	delete []presenza;
	delete []disponibile;
	delete []lavorato;
	delete []missione;
	delete []stato;
	if (!ComboBox1->DroppedDown)
		ComboBox1->ItemIndex = ModBusDM->MW[506] - 1;
	if (!TimerOverride->Enabled)
		LabeledEdit1->Text = ModBusDM->MW[30];
	LabeledEdit2->Text = ModBusDM->MW[507];
	CheckBox4->Checked = DBDataModule->LeggiEsclusioneProgramma();
	if (ModBusDM->GetBit(603, 11)) {
		if(pnlLampeggia->Color == clLime){
			pnlLampeggia->Color = clYellow;
		}else{
			pnlLampeggia->Color = clLime;
		}
	} else {
		pnlLampeggia->Color = GroupBox1->Color;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::ComboBox1Change(TObject *Sender)
{
	ModBusDM->SetWord(506, StrToFloat(ComboBox1->Text));
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::LabeledEdit2Click(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------


void __fastcall TOperatoreForm::SpinButton1DownClick(TObject *Sender)
{
	int vo;

	vo = StrToIntDef(LabeledEdit1->Text, 0);
	if (vo > 0) {
		vo = vo - 1;
		LabeledEdit1->Text = vo;
		TimerOverride->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::SpinButton1UpClick(TObject *Sender)
{
	int vo;

	vo = StrToIntDef(LabeledEdit1->Text, 0);
	if (vo < 100) {
		vo = vo + 1;
		LabeledEdit1->Text = vo;
		TimerOverride->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::TimerOverrideTimer(TObject *Sender)
{
	ModBusDM->SetWord(30, StrToFloat(LabeledEdit1->Text));
	TimerOverride->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::CheckBox4MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	if (CheckBox4->Checked) {
		DBDataModule->MemorizzaEsclusioneProgramma(1);
	} else {
		DBDataModule->MemorizzaEsclusioneProgramma(0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::LabeledEdit2Change(TObject *Sender)
{
	AnsiString prg;

	prg = DBDataModule->LeggiProgrammaPallet(ModBusDM->MW[507]);
	mProgramMU->Text = prg;
}
//---------------------------------------------------------------------------


int TOperatoreForm::TrovaBufferLibero()
{
	int i, res = 0;
	bool bFound = false;

	for (i = 0; i < MainForm->NPallet && !bFound; i++)
    {
		if (ModBusDM->MW[1100 + i] == 0) {
			res = i + 1;
			bFound = true;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::BitBtn5Click(TObject *Sender)
{
	int buf;

	if (ConfermaDlg(_("CONFERMA"), _("Esegui operazione carico/scarico pallet ") + LabeledEdit2->Text + "?") == mrOk) {
		ModBusDM->SetBit(509, 1);
		buf = TrovaBufferLibero();
		if (buf > 0) {
			ModBusDM->SetWord(500, buf);
			ModBusDM->SetWord(501, LabeledEdit2->Text.ToInt());
			ModBusDM->SetWord(502, 0);
			ModBusDM->SetBit(503, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::BitBtn1Click(TObject *Sender)
{
	if (ConfermaDlg(_("CONFERMA"), _("Porto pallet ") + LabeledEdit2->Text + _(" in postazione operatore?")) == mrOk) {
		ModBusDM->SetBit(509, 1);
		ModBusDM->SetWord(504, LabeledEdit2->Text.ToInt());
		ModBusDM->SetBit(505, 0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::BitBtn6MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->SetBit(505, 2);
	ModBusDM->SetWord(506, StrToFloat(LabeledEdit2->Text));
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::BitBtn6MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->ResetBit(505, 2);
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::mProgramMUClick(TObject *Sender)
{
	ProgramSelectDBDlg->CaricaProgrammi();
	if (ProgramSelectDBDlg->ShowModal() == IDOK) {
		mProgramMU->Text = ProgramSelectDBDlg->LEProgrammaSelezionato->Text;
		DBDataModule->MemorizzaProgrammaPallet(ModBusDM->MW[507], mProgramMU->Text);
	}
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::MyShape1MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->SetWord(506, StrToFloat(((TMyShape*)Sender)->Text));
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TOperatoreForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------


void __fastcall TOperatoreForm::CheckBox1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	if (CheckBox1->Checked) {
		DBDataModule->MemorizzaEsclusioneM30(1);
		ModBusDM->SetBit(10,3);
	} else {
		DBDataModule->MemorizzaEsclusioneM30(0);
		ModBusDM->ResetBit(10,3);
	}
}
//---------------------------------------------------------------------------

