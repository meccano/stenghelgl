//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "DB.h"
#include "StoricoSegnalazioni.h"
#include "inifiles.hpp"
#include "sinottico.h"
#include "modbus.h"
#include "keyboardlock.h"
#include "manuali.h"
#include "parametri.h"
#include "password.h"
#include "MSG.h"
#include "chiusura.h"
#include "lavoro.h"
#include "timer.h"
#include "segnalazioni.h"
#include "ping.h"
#include "operatore.h"
#include <stdio.h>
#include <windows.h>
#include <winbase.h>
#include <tlhelp32.h>
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#define NativeScreenHeight	768
//---------------------------------------------------------------------------
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
	: TForm(Owner)
{
	TP_GlobalIgnoreClassProperty(__classid(TAction), "Category");
	TP_GlobalIgnoreClassProperty(__classid(TControl), "HelpKeyword");
	TP_GlobalIgnoreClassProperty(__classid(TNotebook), "Pages");

	TP_GlobalIgnoreClassProperty(__classid(TControl), "ImeName");
	TP_GlobalIgnoreClass(__classid(TFont));

	TP_GlobalIgnoreClassProperty(__classid(TColumn), "FieldName");

	TP_GlobalIgnoreClassProperty(__classid(TField), "DefaultExpression");
	TP_GlobalIgnoreClassProperty(__classid(TField), "FieldName");
	TP_GlobalIgnoreClassProperty(__classid(TField), "KeyFields");
	TP_GlobalIgnoreClassProperty(__classid(TField), "DisplayName");
	TP_GlobalIgnoreClassProperty(__classid(TField), "LookupKeyFields");
	TP_GlobalIgnoreClassProperty(__classid(TField), "LookupResultField");
	TP_GlobalIgnoreClassProperty(__classid(TField), "Origin");
	TP_GlobalIgnoreClass(__classid(TParam));
	TP_GlobalIgnoreClassProperty(__classid(TFieldDef), "Name");

	TP_GlobalIgnoreClassProperty(__classid(TDBComboBox), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBCheckBox), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBEdit), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBImage), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBListBox), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBLookupControl), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBLookupControl), "KeyField");
	TP_GlobalIgnoreClassProperty(__classid(TDBLookupControl), "ListField");
	TP_GlobalIgnoreClassProperty(__classid(TDBMemo), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBRadioGroup), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBRichEdit), "DataField");
	TP_GlobalIgnoreClassProperty(__classid(TDBText), "DataField");

	TP_GlobalIgnoreClass (__classid(TADOConnection));
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "CommandText");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "ConnectionString");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "DatasetField");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "Filter");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "IndexFieldNames");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "IndexName");
	TP_GlobalIgnoreClassProperty(__classid(TADOQuery), "MasterFields");
	TP_GlobalIgnoreClassProperty(__classid(TADOTable), "IndexFieldNames");
	TP_GlobalIgnoreClassProperty(__classid(TADOTable), "IndexName");
	TP_GlobalIgnoreClassProperty(__classid(TADOTable), "MasterFields");
	TP_GlobalIgnoreClassProperty(__classid(TADOTable), "TableName");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "CommandText");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "ConnectionString");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "DatasetField");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "Filter");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "IndexFieldNames");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "IndexName");
	TP_GlobalIgnoreClassProperty(__classid(TADODataSet), "MasterFields");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StatusBarDrawPanel(TStatusBar *StatusBar,
      TStatusPanel *Panel, const TRect &Rect)
{
	static int n = 0;
	static int prev = 0;
	TCanvas *pCanvas = StatusBar->Canvas;
	int t, l;

	switch (Panel->Index) {
	case 0:
		pCanvas->Brush->Color = clBtnFace;
		pCanvas->Font->Color = clBlack;
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 1:
		switch(pwdlevel) {
			case 1:
				n = 1;
				break;
			case 2:
				n = 2;
				break;
			default:
				n = 0;
		}
		pCanvas->FillRect(Rect);
		ImageList1->Draw(StatusBar->Canvas, Rect.Left, Rect.Top, n);
		break;
	case 2:
		if (DBDataModule->ADOConnection1->Connected) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		} else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 3:
		if (!ModBusDM->PLCErr) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		} else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 4:
//		if (LSV2DM->Connected) {
//			pCanvas->Brush->Color = clLime;
//			pCanvas->Font->Color = clBlack;
//		} else {
//			pCanvas->Brush->Color = clRed;
//			pCanvas->Font->Color = clWhite;
//		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 5:
		if (nall == 0) {
			pCanvas->Brush->Color = clBtnFace;
			pCanvas->Font->Color = clBlack;
			Panel->Text = _("Nessun allarme");
		} else if (nall <= 96 ) {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
			pCanvas->Font->Style = TFontStyles() << fsBold;
			Panel->Text = ModBusDM->alltxt[nall];
		} else {
			pCanvas->Brush->Color = clYellow;
			pCanvas->Font->Color = clBlack;
			Panel->Text = ModBusDM->alltxt[nall];
		}
		pCanvas->FillRect(Rect);
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + 2, Rect.top + t, Panel->Text);
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerOraTimer(TObject *Sender)
{
	StatusBar->Panels->Items[0]->Text = FormatDateTime("dddd d mmmm yyyy h':'mm':'ss", Now());
	UpdControls();
}
//---------------------------------------------------------------------------

void TMainForm::VerificaSegnalazioni()
{
	AnsiString msg;

	nall = ModBusDM->NextActiveAlarm(nall);
	StatusBar->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerSgnTimer(TObject *Sender)
{
	TimerSgn->Enabled = false;
	VerificaSegnalazioni();
	if (nall) {
		TimerSgn->Interval = 1500;
	} else {
		TimerSgn->Interval = 100;
	}
	TimerSgn->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StoricoSegnalazioniExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	StoricoSegnalazioniForm = new TStoricoSegnalazioniForm(this);
	StoricoSegnalazioniForm->Show();
	StoricoSegnalazioniForm->WindowState = wsMinimized;
	StoricoSegnalazioniForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SinotticoExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	SinotticoForm = new TSinotticoForm(this);
	SinotticoForm->Show();
	SinotticoForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
						   NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	SinotticoForm->ScaleBy(96, Screen->PixelsPerInch);
	SinotticoForm->WindowState = wsMinimized;
	SinotticoForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ApplicationEvents1Message(tagMSG &Msg,
	  bool &Handled)
{
   short i;
   if (Msg.message == WM_MOUSEWHEEL) {
		Msg.message = WM_KEYDOWN;
		Msg.lParam = 0;
		i = HIWORD(Msg.wParam) ;
		if (i > 0)
			Msg.wParam = VK_UP;
		else
			Msg.wParam = VK_DOWN;
		Handled = False;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	int i;
	TIniFile *Ini;
	TCursor Save_Cursor = Screen->Cursor;
	AnsiString lang;

	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	MUAddress = Ini->ReadString("MU", "MUAddress", "127.0.0.1");
	MUPort = Ini->ReadInteger("MU", "MUPort", 19000);
	ExtPrg = Ini->ReadString("GENERALE", "ExtPrg", "");
	ExtLabel = Ini->ReadString("GENERALE", "ExtLabel", "");
	ExtPrg2 = Ini->ReadString("GENERALE", "ExtPrg2", "");
	ExtLabel2 = Ini->ReadString("GENERALE", "ExtLabel2", "");
	NPallet = Ini->ReadInteger("GENERALE", "NPallet", 6);
	DimPallet    = Ini->ReadInteger("GENERALE", "DimPallet", 105);
	RaggioTavola = Ini->ReadInteger("GENERALE", "RaggioTavola", 284);
	PosMacchina  = Ini->ReadInteger("GENERALE", "PosMacchina", 0);
	ImgSfondoMU  = Ini->ReadString("GENERALE" , "ImgSfondoMU", "lay.png");
	ImgSfondoOP  = Ini->ReadString("GENERALE" , "ImgSfondoOP", "lay.png");
	ImgSfondoMain= Ini->ReadString("GENERALE" , "ImgSfondoMain", "lay.png");
	OffsetX  	 = Ini->ReadInteger("GENERALE", "OffsetX", 0);
	OffsetY    	 = Ini->ReadInteger("GENERALE" , "OffsetY", 0);
	delete Ini;
 	Image1->Picture->LoadFromFile(MainForm->ImgSfondoMain);
	if (ExtPrg != "") {
		BitBtn6->Visible = true;
		BitBtn6->Caption = ExtLabel;
	} else {
		BitBtn6->Visible = false;
	}
	if (ExtPrg2 != "") {
		BitBtn7->Visible = true;
		BitBtn7->Caption = ExtLabel2;
	} else {
		BitBtn7->Visible = false;
	}
	//LSV2DM->Connect();
	FormatSettings.DecimalSeparator = '.';
	FormatSettings.TimeSeparator = ':';
	if (DebugHook) {
		pwdlevel = 2;
	} else {
		pwdlevel = 0;
	}
	PWD = "";
	MUconnected = 0;
	LoadPassword();
	if (!DebugHook) {
		LockKeyboard();
		ShowCursor(false);
	}
	lang = GetCurrentLanguage();
	if (lang == "it") {
		cbLingua->ItemIndex = 0;
	} else if (lang == "en") {
		cbLingua->ItemIndex = 1;
	} else if (lang == "hr") {
		cbLingua->ItemIndex = 2;
	}
	TranslateComponent(this);
	Screen->Cursor = Save_Cursor;
	UpdControls();
	TimerSgn->Enabled = true;
}
//---------------------------------------------------------------------------

void TMainForm::SavePassword() {
	FILE *f;
	char s[100] = "";
	int i;

	if (f = fopen("pwd.txt", "w")) {
		for (i = 0; i < PWD.Length(); i++) {
			s[i] = PWD.c_str()[i] + 1 - (i % 3);
		}
		fprintf(f, "%s\n", s);
		fclose(f);
	}
}

void TMainForm::LoadPassword() {
	FILE *f;
	char s[100] = "";
	int i;

	if (f = fopen("pwd.txt", "r")) {
		fscanf(f, "%s", s);
		PWD = "";
		for (i = 0; i < strlen(s); i++) {
			PWD += (char)(s[i] - 1 + (i % 3));
		}
		fclose(f);
	}
}

void __fastcall TMainForm::ManualiExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	ManualiForm = new TManualiForm(this);
	ManualiForm->Show();
	ManualiForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
						NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	ManualiForm->ScaleBy(96, Screen->PixelsPerInch);
	ManualiForm->WindowState = wsMinimized;
	ManualiForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ParametriExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	ParametriForm = new TParametriForm(this);
	ParametriForm->Show();
	ParametriForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
						   NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	ParametriForm->ScaleBy(96, Screen->PixelsPerInch);
	ParametriForm->WindowState = wsMinimized;
	ParametriForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn12Click(TObject *Sender)
{
	int res;
	AnsiString NewPwd;

	PasswordDlg->BitBtn3->Visible = true;
	PasswordDlg->Label1->Caption = _("Digita la password");
	PasswordDlg->Password->Text = "";
	res = PasswordDlg->ShowModal();
	if (res == IDOK) {
		if (PasswordDlg->Password->Text == "meccanosrl") {
			pwdlevel = 2;
			ShowCursor(true);
		} else if (PasswordDlg->Password->Text == PWD)
			pwdlevel = 1;
		if (pwdlevel > 0) {
			UnlockKeyboard();
			if (pwdlevel == 2)
				TimerPwd->Enabled = false;
			else
				TimerPwd->Enabled = true;
		} else {
			MainForm->MSG("Password errata");
		}
	} else if (res == IDYES) {
		if ((PasswordDlg->Password->Text == "meccanosrl") ||
			(PasswordDlg->Password->Text == PWD)) {
			PasswordDlg->BitBtn3->Visible = false;
			PasswordDlg->Label1->Caption = _("Digita la nuova password");
			PasswordDlg->Password->Text = "";
			res = PasswordDlg->ShowModal();
			if (res == IDOK) {
				NewPwd = PasswordDlg->Password->Text;
				PasswordDlg->Label1->Caption = _("Conferma la nuova password");
				PasswordDlg->Password->Text = "";
				res = PasswordDlg->ShowModal();
				if (res == IDOK) {
					if (NewPwd == PasswordDlg->Password->Text) {
						PWD = NewPwd;
						SavePassword();
					} else
						MainForm->MSG(_("Errore nella conferma password"));
				}
			}
		} else {
			MainForm->MSG(_("Password errata"));
		}
	} else {
		TimerPwdTimer(NULL);
	}
}
//---------------------------------------------------------------------------

void TMainForm::UpdControls() {
	BitBtn11->Enabled = (MainForm->pwdlevel != 0);
//	BitBtn13->Enabled = (MainForm->pwdlevel != 0);
	BitBtn3->Enabled = (MainForm->pwdlevel != 0);
	GroupBox1->Visible = (MainForm->pwdlevel != 0) && (MDIChildCount == 0);
	StatusBar->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerPwdTimer(TObject *Sender)
{
	TimerPwd->Enabled = false;
	pwdlevel = 0;
	UpdControls();
	if (!DebugHook) {
		LockKeyboard();
		ShowCursor(false);
	}
}
//---------------------------------------------------------------------------

void TMainForm::MSG(AnsiString msg) {
	MSGForm->Panel1->Caption = msg;
	MSGForm->ShowModal();
}
//---------------------------------------------------------------------------

void TMainForm::RestartTimer()
{
	if (TimerPwd->Enabled) {
		TimerPwd->Enabled = false;
		TimerPwd->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn13Click(TObject *Sender)
{
//	if (pwdlevel != 0) {
		Close();
//	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	int res;

/*	if (pwdlevel <= 0) {
		CanClose = false;
		return;
	}
*/
	res = ChiusuraForm->ShowModal();
	CanClose = ((res == IDYES) || (res == IDOK));
	if (CanClose) {
		ShowCursor(true);
	}
	if (res == IDYES) {
		HANDLE hToken;
		TOKEN_PRIVILEGES tkp;

		// Get a token for this process.
		if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		{
		  // Get the LUID for the shutdown privilege.
			LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);

		  tkp.PrivilegeCount = 1;  // one privilege to set
		  tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		   // Get the shutdown privilege for this process.
			 AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

		   ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::LavoroExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	LavoroForm = new TLavoroForm(this);
	LavoroForm->Show();
	LavoroForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
						NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	LavoroForm->ScaleBy(96, Screen->PixelsPerInch);
	LavoroForm->WindowState = wsMinimized;
	LavoroForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	TimerForm = new TTimerForm(this);
	TimerForm->Show();
	TimerForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
					   NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	TimerForm->ScaleBy(96, Screen->PixelsPerInch);
	TimerForm->WindowState = wsMinimized;
	TimerForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::AllarmiExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	SegnalazioniForm = new TSegnalazioniForm(this);
	SegnalazioniForm->Show();
	SegnalazioniForm->WindowState = wsMinimized;
	SegnalazioniForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ApplicationEvents1ActionExecute(TBasicAction *Action, bool &Handled)

{
	RestartTimer();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::OperatoreExecute(TObject *Sender)
{
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		MDIChildren[i]->Close();
	}
	OperatoreForm = new TOperatoreForm(this);
	OperatoreForm->Show();
	OperatoreForm->ScaleBy(Screen->Height - Image4->Height - ToolBar1->Height - StatusBar->Height,
						   NativeScreenHeight - Image4->Height - ToolBar1->Height - StatusBar->Height);
	OperatoreForm->ScaleBy(96, Screen->PixelsPerInch);
	OperatoreForm->WindowState = wsMinimized;
	OperatoreForm->WindowState = wsMaximized;
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

int GetProcessByName(AnsiString pName)
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (stricmp(entry.szExeFile, pName.c_str()) == 0)
			{
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

				// Do stuff..
				TerminateProcess(hProcess, 0);

				CloseHandle(hProcess);
			}
		}
	}

	CloseHandle(snapshot);

	return 0;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn6Click(TObject *Sender)
{
	GetProcessByName(ExtractFileName(ExtPrg));
	ShellExecute(Handle, "Open", ExtPrg.c_str(), "", "", SW_RESTORE);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn7Click(TObject *Sender)
{
	GetProcessByName(ExtractFileName(ExtPrg2));
	ShellExecute(Handle, "Open", ExtPrg2.c_str(), "", "", SW_RESTORE);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::cbLinguaChange(TObject *Sender)
{
	AnsiString lang, FileName;
	TIniFile *Ini;

	switch(cbLingua->ItemIndex) {
		case 1:
			lang = "en";
			break;
		case 2:
			lang = "hr";
			break;
		default:
			lang = "it";
			break;
	}
	UseLanguage(lang);
	RetranslateChildren();
	FileName.printf("Allarmi-%s.txt", lang.c_str());
	ModBusDM->LeggiTestiAllarmi(FileName);
	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	Ini->WriteString("GENERALE", "Language", lang);
	delete(Ini);
}
//---------------------------------------------------------------------------

void TMainForm::RetranslateChildren()
{
	for(int i = MDIChildCount - 1; i >= 0; i--) {
		RetranslateComponent(MDIChildren[i]);
	}
	for(int i = 0; i < ComponentCount; i++) {
		if (dynamic_cast<TForm *>(Components[i]) != NULL) {
			RetranslateComponent(Components[i]);
		}
	}
	RetranslateComponent(this);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ToolBar1Click(TObject *Sender)
{
ModBusDM->WriteWorkProgram(L"12345-1");
}
//---------------------------------------------------------------------------

