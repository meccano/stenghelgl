//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "StoricoSegnalazioni.h"
#include "insalpha.h"
#include "DB.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TStoricoSegnalazioniForm *StoricoSegnalazioniForm;
//---------------------------------------------------------------------------
__fastcall TStoricoSegnalazioniForm::TStoricoSegnalazioniForm(TComponent* Owner)
	: TForm(Owner)
{
	DateTimePicker1->Date = Now();
	DateTimePicker3->Date = Now();
}
//---------------------------------------------------------------------------
void __fastcall TStoricoSegnalazioniForm::FormActivate(TObject *Sender)
{
	Aggiorna();
	WindowState = wsMaximized;
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	ADOQuery1->Close();
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::DBGrid1DrawColumnCell(TObject *Sender,
	  const TRect &Rect, int DataCol, TColumn *Column,
	  TGridDrawState State)
{
	if (ADOQuery1->RecordCount == 0) {
		return;
	}
	if (ADOQuery1Acquisito->IsNull && ADOQuery1Allarme->AsInteger) {
		DBGrid1->Canvas->Font->Color = clWhite;
		DBGrid1->Canvas->Brush->Color = clRed;
	} else if (ADOQuery1Acquisito->IsNull && !ADOQuery1Allarme->AsInteger) {
		DBGrid1->Canvas->Font->Color = clBlack;
		DBGrid1->Canvas->Brush->Color = clYellow;
	} else {
		DBGrid1->Canvas->Font->Color = clBlack;
		DBGrid1->Canvas->Brush->Color = clWhite;
	}
	DBGrid1->Canvas->TextRect(Rect,Rect.left+2,Rect.Top+2,Column->Field->AsString);
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::Aggiorna()
{
	TADOQuery *ADOQuery;
	AnsiString cond, da, a, strsql;
	int n;

	ADOQuery1->Close();
	da = FormatDateTime("mm/dd/yyyy", DateTimePicker1->Date) + " " + FormatDateTime("hh:nn:ss", DateTimePicker2->Time);
	a = FormatDateTime("mm/dd/yyyy", DateTimePicker3->Date) + " " + FormatDateTime("hh:nn:ss", DateTimePicker4->Time);
	cond.printf("WHERE (DataOra >= #%s#) AND (DataOra <= #%s#)", da, a);
	if (Edit1->Text != "")
		cond = cond + " AND (Messaggio LIKE '%" + Edit1->Text + "%')";
	strsql = "SELECT * FROM Segnalazioni " + cond + " ORDER BY DataOra DESC";
	ADOQuery1->SQL->Text = strsql;
	ADOQuery1->Open();
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::BitBtn1Click(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::BitBtn3Click(TObject *Sender)
{
	Close();	
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::Edit1Click(TObject *Sender)
{
	InsAlphaForm->Valore->EditLabel->Caption = _("Digita il testo da ricercare");
	InsAlphaForm->Valore->Text = Edit1->Text;
	if (InsAlphaForm->ShowModal() == IDOK) {
		Edit1->Text = InsAlphaForm->Valore->Text;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

