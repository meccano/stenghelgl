//---------------------------------------------------------------------------

#ifndef ManualiH
#define ManualiH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------
class TManualiForm : public TForm
{
__published:	// IDE-managed Components
	TDBCheckBox *DBCheckBox2;
	TPanel *Panel7;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TTimer *Timer1;
	TPanel *Panel2;
	TPanel *Panel4;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel8;
	TTimer *TimerUpd;
	TLabeledEdit *LabeledEdit1;
	TGroupBox *GroupBox1;
	TPanel *Panel9;
	TPanel *Panel10;
	TPanel *Panel11;
	TPanel *Panel12;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TCheckBox *CheckBox2;
	TPanel *Panel13;
	TLabeledEdit *LabeledEdit6;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TPanel *Panel17;
	TPanel *Panel18;
	TPanel *Panel19;
	TPanel *Panel20;
	TPanel *Panel21;
	TCheckBox *CheckBox1;
	TShape *sAgganciSbloccato;
	TStaticText *StaticText12;
	TShape *sAgganciBloccato;
	TStaticText *StaticText1;
	TShape *sInnestiAvanti;
	TStaticText *StaticText2;
	TShape *sInnestiIndietro;
	TStaticText *StaticText3;
	TShape *sPulizia;
	TStaticText *StaticText4;
	TShape *sAttivoSblocco;
	TStaticText *StaticText5;
	TShape *sSbloccoPallet;
	TStaticText *StaticText6;
	TShape *sCtrlPalletSbloccato;
	TStaticText *StaticText7;
	TShape *sEvPresenzaPallet;
	TStaticText *StaticText8;
	TStaticText *StaticText9;
	TShape *sEvControlloPEL;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall PanelMX2MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall CheckBox2MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall PanelMX3MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall PanelMX4MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall Panel21MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel13MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TManualiForm(TComponent* Owner);
	void ColoraPallino(int stato, TShape *t, TColor ColorON = clLime, TColor ColorOFF = clGray);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TManualiForm *ManualiForm;
//---------------------------------------------------------------------------
#endif
