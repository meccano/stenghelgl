//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Manuali.h"
#include "ClientData.h"
#include "ModBus.h"
#include "Main.h"
#include "Conferma.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TManualiForm *ManualiForm;
//---------------------------------------------------------------------------
__fastcall TManualiForm::TManualiForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TManualiForm::FormActivate(TObject *Sender)
{
	Aggiorna();
	WindowState = wsMaximized;
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	ModBusDM->SetWord(2, 0);
	ModBusDM->SetWord(3, 0);
	ModBusDM->SetWord(4, 0);
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::BitBtn3Click(TObject *Sender)
{
	Close();	
}
//---------------------------------------------------------------------------

void TManualiForm::ColoraPallino(int stato, TShape *t, TColor ColorON, TColor ColorOFF)
{
	if (stato != 0) {
		t->Brush->Color = ColorON;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------


void TManualiForm::Aggiorna() {
	GroupBox1->Visible = (MainForm->pwdlevel > 0);
//	Panel13->Visible = (MainForm->pwdlevel > 0);
	CheckBox2->Visible =  (MainForm->pwdlevel > 0);
	CheckBox1->Visible =  (MainForm->pwdlevel > 0);
	Panel2->Color = ((ModBusDM->GetBit(2, 0))? clLime: clBtnFace);
	Panel4->Color = ((ModBusDM->GetBit(2, 1))? clLime: clBtnFace);
	Panel5->Color = ((ModBusDM->GetBit(2, 2))? clLime: clBtnFace);
	Panel6->Color = ((ModBusDM->GetBit(2, 3))? clLime: clBtnFace);
	Panel8->Color = ((ModBusDM->GetBit(2, 4))? clLime: clBtnFace);
	Panel9->Color = ((ModBusDM->GetBit(3, 0))? clLime: clBtnFace);
	Panel10->Color = ((ModBusDM->GetBit(3, 1))? clLime: clBtnFace);
	Panel11->Color = ((ModBusDM->GetBit(3, 2))? clLime: clBtnFace);
	Panel12->Color = ((ModBusDM->GetBit(3, 3))? clLime: clBtnFace);
	Panel13->Color = ((ModBusDM->GetBit(4, 0))? clLime: clBtnFace);
	Panel14->Color = ((ModBusDM->GetBit(2, 5))? clLime: clBtnFace);
	Panel15->Color = ((ModBusDM->GetBit(2, 6))? clLime: clBtnFace);
	Panel16->Color = ((ModBusDM->GetBit(2, 7))? clLime: clBtnFace);
	Panel17->Color = ((ModBusDM->GetBit(2, 8))? clLime: clBtnFace);
	Panel18->Color = ((ModBusDM->GetBit(2, 9))? clLime: clBtnFace);
	Panel19->Color = ((ModBusDM->GetBit(4, 1))? clLime: clBtnFace);
	Panel20->Color = ((ModBusDM->GetBit(4, 2))? clLime: clBtnFace);
	CheckBox2->Checked = (ModBusDM->GetBit(10, 1));
	CheckBox1->Checked = (ModBusDM->GetBit(10, 2));
	LabeledEdit1->Text = FloatToStrF(ModBusDM->GetDWord(450) / 100.0, ffFixed, 15, 2);
	LabeledEdit2->Text = FloatToStrF(ModBusDM->GetDWord(451) / 100.0, ffFixed, 15, 2);
	LabeledEdit3->Text = FloatToStrF(ModBusDM->GetDWord(452) / 100.0, ffFixed, 15, 2);
	LabeledEdit4->Text = FloatToStrF(ModBusDM->GetDWord(458) / 100.0, ffFixed, 15, 2);
	LabeledEdit5->Text = FloatToStrF(ModBusDM->GetDWord(459) / 100.0, ffFixed, 15, 2);
	LabeledEdit6->Text = FloatToStrF(ModBusDM->GetDWord(21) / 100.0, ffFixed, 15, 2);

	ColoraPallino(ModBusDM->GetBit(603, 0), sAgganciSbloccato);
	ColoraPallino(ModBusDM->GetBit(603, 1), sAgganciBloccato);
	ColoraPallino(ModBusDM->GetBit(603, 2), sAttivoSblocco);
	ColoraPallino(ModBusDM->GetBit(603, 3), sPulizia);
	ColoraPallino(ModBusDM->GetBit(603, 4), sInnestiAvanti);
	ColoraPallino(ModBusDM->GetBit(603, 5), sInnestiIndietro);
	ColoraPallino(ModBusDM->GetBit(603, 6), sSbloccoPallet);
	ColoraPallino(ModBusDM->GetBit(603, 7), sCtrlPalletSbloccato);
	ColoraPallino(ModBusDM->GetBit(603, 8), sEvControlloPEL);
	ColoraPallino(ModBusDM->GetBit(603, 9), sEvPresenzaPallet);
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::PanelMX2MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;
	if (ModBusDM->GetBit(2, p->Tag)) {
		ModBusDM->ResetBit(2, p->Tag);
		p->Color = clBtnFace;
	} else {
		ModBusDM->SetWord(2, 0);
		ModBusDM->SetBit(2, p->Tag);
		p->Color = clLime;
	}
}
//---------------------------------------------------------------------------


void __fastcall TManualiForm::CheckBox2MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TCheckBox *cb;

	cb = (TCheckBox*)Sender;
	if (cb->Checked) {
		ModBusDM->SetBit(10, cb->Tag);
	} else {
		ModBusDM->ResetBit(10, cb->Tag);
	}
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::PanelMX3MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;
	if (ModBusDM->GetBit(3, p->Tag)) {
		ModBusDM->ResetBit(3, p->Tag);
		p->Color = clBtnFace;
	} else {
		ModBusDM->SetBit(3, p->Tag);
		p->Color = clLime;
	}
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::PanelMX4MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;
	if (ModBusDM->GetBit(4, p->Tag)) {
		ModBusDM->ResetBit(4, p->Tag);
		p->Color = clBtnFace;
	} else {
		ModBusDM->SetBit(4, p->Tag);
		p->Color = clLime;
	}
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::FormDeactivate(TObject *Sender)
{
	ModBusDM->SetWord(2, 0);
	ModBusDM->SetWord(3, 0);
	ModBusDM->SetWord(4, 0);
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::Panel21MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;
	if (ModBusDM->GetBit(3, p->Tag)) {
		ModBusDM->ResetBit(3, p->Tag);
		p->Color = clBtnFace;
	} else if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente eseguire il comando?\nIMPORTANTE: PREMERE EMERGENZA")) == mrOk) {
		ModBusDM->SetBit(3, p->Tag);
		p->Color = clLime;
	}
}
//---------------------------------------------------------------------------

void __fastcall TManualiForm::Panel13MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;
	if (ModBusDM->GetBit(4, p->Tag)) {
		ModBusDM->ResetBit(4, p->Tag);
		p->Color = clBtnFace;
	} else {
		ModBusDM->SetBit(4, p->Tag);
		p->Color = clLime;
	}
}
//---------------------------------------------------------------------------


void __fastcall TManualiForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------


