//---------------------------------------------------------------------------

#ifndef SegnalazioniH
#define SegnalazioniH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <DBClient.hpp>
//---------------------------------------------------------------------------
class TSegnalazioniForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel2;
	TTimer *Timer1;
	TPanel *Panel4;
	TGroupBox *GroupBox2;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit3;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TLabeledEdit *LabeledEdit9;
	TLabeledEdit *LabeledEdit10;
	TLabeledEdit *LabeledEdit11;
	TLabeledEdit *LabeledEdit12;
	TPanel *Panel1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TBitBtn *BitBtn3;
	TTimer *Timer2;
	TStringGrid *StringGrid1;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Timer2Timer(TObject *Sender);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TSegnalazioniForm(TComponent* Owner);
	void Aggiorna();
	void AggiornaPLC();
};
//---------------------------------------------------------------------------
extern PACKAGE TSegnalazioniForm *SegnalazioniForm;
//---------------------------------------------------------------------------
#endif
