//---------------------------------------------------------------------------

#ifndef StoricoSegnalazioniH
#define StoricoSegnalazioniH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <DBClient.hpp>
//---------------------------------------------------------------------------
class TStoricoSegnalazioniForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TDBGrid *DBGrid1;
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TPanel *Panel2;
	TLabel *Label1;
	TDateTimePicker *DateTimePicker1;
	TLabel *Label2;
	TDateTimePicker *DateTimePicker2;
	TLabel *Label3;
	TDateTimePicker *DateTimePicker3;
	TLabel *Label4;
	TDateTimePicker *DateTimePicker4;
	TBitBtn *BitBtn1;
	TLabel *Label5;
	TEdit *Edit1;
	TPanel *Panel3;
	TBevel *Bevel1;
	TDateTimeField *ADOQuery1DataOra;
	TWideStringField *ADOQuery1Messaggio;
	TIntegerField *ADOQuery1Allarme;
	TDateTimeField *ADOQuery1Acquisito;
	TBitBtn *BitBtn3;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall DBGrid1DrawColumnCell(TObject *Sender, const TRect &Rect,
          int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall Edit1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TStoricoSegnalazioniForm(TComponent* Owner);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TStoricoSegnalazioniForm *StoricoSegnalazioniForm;
//---------------------------------------------------------------------------
#endif
