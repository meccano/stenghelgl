//----------------------------------------------------------------------------
#ifndef MainH
#define MainH
//----------------------------------------------------------------------------
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Messages.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <StdCtrls.hpp>
#include <Menus.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <jpeg.hpp>
#include <AppEvnts.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.GIFImg.hpp>
#include <Vcl.OleCtrls.hpp>
#include <System.Actions.hpp>
//----------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
    TStatusBar *StatusBar;
	TToolBar *ToolBar1;
	TActionList *ActionList1;
	TFileExit *FileExit1;
	TAction *About;
	TTimer *TimerOra;
	TImageList *ImageList1;
	TTimer *TimerSgn;
	TBitBtn *BitBtn9;
	TAction *StoricoSegnalazioni;
	TAction *Sinottico;
	TBitBtn *BitBtn10;
	TApplicationEvents *ApplicationEvents1;
	TImage *Image1;
	TImage *Image2;
	TBitBtn *BitBtn13;
	TBitBtn *BitBtn12;
	TBitBtn *BitBtn11;
	TAction *Manuali;
	TBitBtn *BitBtn1;
	TAction *Parametri;
	TTimer *TimerPwd;
	TPanel *Panel1;
	TImage *Image3;
	TBitBtn *BitBtn2;
	TAction *Lavoro;
	TBitBtn *BitBtn3;
	TAction *Timer;
	TBitBtn *BitBtn4;
	TAction *Allarmi;
	TImage *Image4;
	TBitBtn *BitBtn5;
	TAction *Operatore;
	TToolButton *ToolButton1;
	TBitBtn *BitBtn6;
	TBitBtn *BitBtn7;
	TImageList *ImageList2;
	TGroupBox *GroupBox1;
	TComboBoxEx *cbLingua;
	void __fastcall StatusBarDrawPanel(TStatusBar *StatusBar,
          TStatusPanel *Panel, const TRect &Rect);
	void __fastcall TimerOraTimer(TObject *Sender);
	void __fastcall TimerSgnTimer(TObject *Sender);
	void __fastcall StoricoSegnalazioniExecute(TObject *Sender);
	void __fastcall SinotticoExecute(TObject *Sender);
	void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ManualiExecute(TObject *Sender);
	void __fastcall ParametriExecute(TObject *Sender);
	void __fastcall BitBtn12Click(TObject *Sender);
	void __fastcall TimerPwdTimer(TObject *Sender);
	void __fastcall BitBtn13Click(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall LavoroExecute(TObject *Sender);
	void __fastcall TimerExecute(TObject *Sender);
	void __fastcall AllarmiExecute(TObject *Sender);
	void __fastcall ApplicationEvents1ActionExecute(TBasicAction *Action, bool &Handled);
	void __fastcall OperatoreExecute(TObject *Sender);
	void __fastcall BitBtn6Click(TObject *Sender);
	void __fastcall BitBtn7Click(TObject *Sender);
	void __fastcall cbLinguaChange(TObject *Sender);
	void __fastcall ToolBar1Click(TObject *Sender);
private:
public:
	virtual __fastcall TMainForm(TComponent *Owner);
	void SavePassword();
	void LoadPassword();
	void VerificaSegnalazioni();
	void MSG(AnsiString msg);
	void RestartTimer();
	void UpdControls();
	void RetranslateChildren();
	int nall, pwdlevel, MUconnected;
	AnsiString PWD, MUAddress, ExtPrg, ExtLabel, ExtPrg2, ExtLabel2;
	int MUPort;
	int NPallet;
	int DimPallet;
	int RaggioTavola;
	int PosMacchina;
	int OffsetX;
	int OffsetY;
	AnsiString ImgSfondoMU, ImgSfondoOP, ImgSfondoMain;
};
//----------------------------------------------------------------------------
extern TMainForm *MainForm;
//----------------------------------------------------------------------------
#endif
