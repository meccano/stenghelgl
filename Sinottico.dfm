object SinotticoForm: TSinotticoForm
  Left = 459
  Top = 109
  Caption = 'Sinottico'
  ClientHeight = 649
  ClientWidth = 1011
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 19
  object DBCheckBox2: TDBCheckBox
    Left = 734
    Top = 8
    Width = 17
    Height = 17
    Color = clBtnFace
    DataField = 'Ribaltamento'
    ParentColor = False
    TabOrder = 0
    ValueChecked = '1'
    ValueUnchecked = '0'
    Visible = False
  end
  object Panel7: TPanel
    Left = 0
    Top = 0
    Width = 1011
    Height = 49
    Align = alTop
    Alignment = taLeftJustify
    Caption = '   Sinottico Dati PLC'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 858
    Top = 49
    Width = 153
    Height = 600
    Align = alRight
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 500
      Width = 151
      Height = 99
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 151
        Height = 10
        Align = alTop
        Shape = bsTopLine
        ExplicitWidth = 223
      end
      object BitBtn3: TBitBtn
        Left = 16
        Top = 18
        Width = 116
        Height = 65
        Caption = 'Chiudi'
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C006000000000000000000000000000000000000FFFFFF6763CF
          1712B61813BA1812BA1812BA1812BA1712B91712B91712B91711B91711B91711
          B91711B91711B81711B81711B81711B81711B81711B81711B71610B36662CDFF
          FFFF6966D4201DD52724ED2624ED2623ED2622ED2622ED2621ED2621ED2520ED
          2520ED251FED251EED241EED241DED241DED241DED241CED241CED241CED241C
          ED241CED1E17D36763D11A16BD2929EE2829EE2828EE2828EE2220DA2321DF27
          26ED2725ED2725ED2724ED2724ED2624ED2623ED2622ED2622ED2621ED1A15C2
          211CDF2520ED251FED251EED241EED1812B91D19C42A2EEE2A2DEE2A2DEE2526
          E1140FAA140FAA2525E1292AEE2929EE2929EE2829EE2828EE2828EE2827ED28
          26ED1A16C1130DA6140FAA2320E02724ED2624ED2623ED1A15C01E1BC72C33EE
          2C32EE272BE21611AF6C69CA403CBA1611AF2629E22B2FEE2A2EEE2A2DEE2A2D
          EE2A2CEE2A2CEE1C19C5140FABA7A5DF6C69CA1610AF2524E12828EE2828EE1C
          17C31F1DC92D37EE2930E31813B46D6ACDFFFFFFF0F0FA423DBF1813B4282CE3
          2C33EE2C32EE2C31EE2C31EE2324D71611B0A8A5E1FFFFFFFFFFFF6D6ACD1812
          B42526DF2A2CEE1D19C6201FCC2F3DEF2225CE1812B6A9A6E4FFFFFFFFFFFFF1
          F0FA6E6BD11915B92930E42D37EE2D37EE2528D91812B6A8A6E3FFFFFFFFFFFF
          F1F0FB433FC41914B9282CE32C31EE1F1BC92120CE3141EF3141EF2225CE1A14
          BBA9A7E6FFFFFFFFFFFFFFFFFF706CD41B16BF2B34E5272CDB1A14BBA9A7E5FF
          FFFFFFFFFFFFFFFF4540C81B16BE2931E42D37EE2D36EE1F1DCB2222D13346EF
          3245EF3245EF2427D11B15C0AAA7E8FFFFFFFFFFFFFFFFFF716DD71D17C31B15
          C0AAA7E7FFFFFFFFFFFFFFFFFF716DD81D17C32B35E62F3CEE2F3BEE2E3BEE21
          1ECD2423D3344AEF344AEF3449EF3449EF2932DC1D16C4AAA8E9FFFFFFFFFFFF
          FFFFFF716DDA716DDAFFFFFFFFFFFFFFFFFF726DDB1E18C72D3AE73141EF3141
          EF3040EF3040EF2220D02525D5374EF0374DEF364DEF364CEF364CEF2D38E01E
          16C9ABA8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF726EDD1F19CB2F3EE8
          3346EF3346EF3245EF3245EF3244EF2322D35656E04C61F23951F03950F03850
          F0384FF0384FF02E3AE21F17CD817DE3FFFFFFFFFFFFFFFFFFFFFFFF736EE01F
          17CD3141E8354BEF354BEF344AEF344AEF3449EF475AF15454DE5D5DE36D7EF4
          6679F45066F23E55F03A52F03A52F03547EA211CD3736FE2FFFFFFFFFFFFFFFF
          FFFFFFFF817DE52018D12F3AE4384EF0374EF03A50F04D60F16274F36879F35B
          5BE15E5EE56E81F56E81F56E80F46E80F45E73F34E5EEE302BDA746FE5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABA8EF2F27D84752EA5C6FF36B7CF46B7C
          F46B7CF46A7CF45D5CE35F5FE77184F57083F57083F57083F56B79F25A56E497
          94EDFFFFFFFFFFFFFFFFFF9E9BEF9490EDFFFFFFFFFFFFFFFFFFC1BFF45953E3
          656EEE6D7EF46C7EF46C7EF46C7EF45D5DE55F60E97286F57286F57186F56D7C
          F25B56E79794EFFFFFFFFFFFFFFFFFFFC1BFF65953E65953E69794F0FFFFFFFF
          FFFFFFFFFFC1BFF65953E66164EC6E81F56E80F46E80F45E5EE76060EA7489F5
          7488F56E7EF35B56EA7873EDF5F4FEFFFFFFFFFFFFC1BFF75953E96973F06D7C
          F35B56EA9794F1FFFFFFFFFFFFFFFFFFC1BFF75953E96265EE7083F57083F55F
          5FE86061EC758BF57080F35C57EC7974EFF5F4FEFFFFFFFFFFFFC1BFF85A54EC
          6A75F17488F57488F56F7EF35C57EC9894F3FFFFFFFFFFFFFFFFFFC1BFF85A54
          EC646AF07185F55F60EA6162EE778DF56E7BF35C58EF9894F5FFFFFFFFFFFFC1
          BFF95A54EE6A73F2758BF5758AF5758AF5758AF57080F45C57EF7974F2F5F4FE
          FFFFFF9894F55C57EF6F7FF47388F56060EB6263EF798FF5798FF57384F45D58
          F09895F6C2BFFA5B55F0666AF2788EF5778DF5778DF5778DF5778CF5778CF571
          82F45D58F07A75F39895F65C58F07081F4758AF5758AF56061ED6263F07B92F6
          7B91F67B91F67586F55D59F25B55F2676BF37A90F57A90F5798FF5798FF5798F
          F5798FF5798FF5788EF57384F45D59F25D59F27283F4778DF5778DF5778CF561
          62EE6161F17D93F67D93F67D93F67D93F67787F56A70F47C92F67C92F67C92F6
          7C92F67B91F67B91F67A91F57A90F57A90F57A90F57485F4707EF4798FF5798F
          F5798FF5798FF5605FEF9797F6717CF47E94F67E94F67E94F67E94F67E94F67E
          94F67E94F67E94F67D93F67D93F67D93F67D93F67D93F67D92F67C92F67C92F6
          7C92F67C92F67B91F67B91F66F7AF39796F5FFFFFF9897F86261F36465F36465
          F36465F36465F36465F36465F36465F36465F36465F36465F36465F36465F364
          65F36465F36465F36465F26464F26464F26160F29797F7FFFFFF}
        Spacing = 10
        TabOrder = 0
        OnClick = BitBtn3Click
      end
    end
    object LabeledEdit1: TLabeledEdit
      Tag = 30
      Left = 15
      Top = 52
      Width = 66
      Height = 27
      EditLabel.Width = 60
      EditLabel.Height = 38
      EditLabel.Caption = 'Velocit'#224' '#13#10'override'
      EditLabel.Font.Charset = ANSI_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -16
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      OnClick = LabeledEdit2Click
    end
    object SpinButton1: TSpinButton
      Left = 92
      Top = 10
      Width = 50
      Height = 70
      DownGlyph.Data = {
        0E010000424D0E01000000000000360000002800000009000000060000000100
        200000000000D800000000000000000000000000000000000000008080000080
        8000008080000080800000808000008080000080800000808000008080000080
        8000008080000080800000808000000000000080800000808000008080000080
        8000008080000080800000808000000000000000000000000000008080000080
        8000008080000080800000808000000000000000000000000000000000000000
        0000008080000080800000808000000000000000000000000000000000000000
        0000000000000000000000808000008080000080800000808000008080000080
        800000808000008080000080800000808000}
      TabOrder = 2
      UpGlyph.Data = {
        0E010000424D0E01000000000000360000002800000009000000060000000100
        200000000000D800000000000000000000000000000000000000008080000080
        8000008080000080800000808000008080000080800000808000008080000080
        8000000000000000000000000000000000000000000000000000000000000080
        8000008080000080800000000000000000000000000000000000000000000080
        8000008080000080800000808000008080000000000000000000000000000080
        8000008080000080800000808000008080000080800000808000000000000080
        8000008080000080800000808000008080000080800000808000008080000080
        800000808000008080000080800000808000}
      OnDownClick = SpinButton1DownClick
      OnUpClick = SpinButton1UpClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 456
    Top = 55
    Width = 396
    Height = 398
    Caption = 'Stato postazione'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object lblPP: TLabel
      Left = 11
      Top = 245
      Width = 213
      Height = 19
      Caption = 'Programma macchina utensile'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 247
      Top = 31
      Width = 84
      Height = 60
      Caption = 'Rileggi'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C006000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFDF9F6D1A076BA7033A64A00AA4D00BB6102AC4F
        07B05409C8864DE3C0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
        F6F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC78C58A64900C26800D48101E49A01
        E39801E29700E49A06E49A0DDB8D15CE7C17BB5F15DCA97AFFFFFFFFFFFFFFFF
        FFFFFFFFF2DDC5CF7E23FFFFFFFFFFFFFFFFFFF6EBE2A74B00BD6100DF9001E2
        9701E09501DF9300E09301E0980AE39C0FE49F19E7A722EAAE2BEAAA33D18222
        C87424FAF2E8FFFFFFDFAA6DD88E2CDB932EFFFFFFFFFFFFF6EBE2A64A00D17C
        01E39801E09401DF9301DF9300E09304E1990AE39E14E5A21DE7A725E8AB2EEA
        AF36ECB53EF1BD46E4A63BCC7A1FD18632E5A940F9D66FDF9936FFFFFFFFFFFF
        A74B00D17C01E19701DF9401DF9301DF9201E09506E49B0EE5A116E8A61EE8A9
        25E9AC2CEAAF34EDB43EEEB947F0BF50F3C556EEBB51EEBA54F6CF6DF8D472E1
        9E37FFFFFFC78C58BC6100E39801DF9401DF9300E09403E2990AE7A113DB8E17
        D9902DCE8632E2AD63ECC680F9DE9EF2CB68EFBD4AF1C356F3C75EF5CB65F6CE
        6BF6D072F9D779E3A43CFDF9F6A64A00DF9101E09401DF9300E09304E29B0BE0
        9614BE6613BF6B20DFB48CF4E3D5E6C19BDBA36DCA771CE9BE76FCE5ACF2C75A
        F4CB66F5CE6BF6D272F8D477FBDC82E5A93FD1A076C26801E29700DF9201E094
        06E49B0FE19716BB6414E4BF9DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6BD8FCE
        7C1CF5D891F4CA66F6CF6DF7D273F9D678F9D97FFCDF89E8AD44BD7337D68501
        E29703E2990AE39D12E8A61BC56E17DFB48CFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDBA15BE1A852FADF96F5CB5FF5CF6DF7D271F9D77AFADA80FADC85FEE390EA
        B146AF5507C56D09D4860BE39915E9A81FE4A024C16F25FFFFFFFFFFFFFFFFFF
        FFFFFFFAF2E7CF7E22D99234F6DEA6FFF1D3FEEFD1FEE8B7FCE19EFADB80FBDC
        81FCDF88FDE492EDB74AFFFFFFF9F1EAEBD2BCC98348C36E19CA771ED89D66FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3DEC2E8BB81DC973CE6AD4DF1CC82
        FAE4B6FFF6E3FFF2D2FFECBEFFEBAFEFBB4BAD550DE8CEBBFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF5DCB8EDBF7BE7AB40EFC366F6DB99FEEECEF2C664C47622E3B060
        E3B060CF8832CF8832D29865E8CAAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E1B1F1
        BB4BB55B00EBB32BF0CA63F8E19FFFF0CAF2D496E5B167D58F36CB7C2EE0AF7C
        F1DAC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBBE78E7AD42E8AA3EECBA
        61FFFFFFFFFFFFFFFFFFBB5F03E29501E09506E39A0DE5A31CEFC55DF6D890FD
        EBBEFEEDC7F5DA9ED78F32D28324FBF2E8FFFFFFFFFFFFFFFFFFFFFFFFE3A23C
        FADD81FEE696FCE38EF9D573F4D075EFB841BC6106E29906E39A0FE49F19E7A4
        23E7A92AEAAB31ECB032F5D57FDFA54FDDA15CFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF1CF9AE7AB3EFEE491FCE491FEE592FFEAB0FCE8BAF3C665BD6509E29C09
        E39C12E5A11BE7A727E9AD31EBAF36F2CF7CCC781AE7BD8EFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFF2D6A9E39F32FADA7DFCE18FFDE392FEE38FFFF2D1F6D47CF7
        D999C1660AE39C09E39E14E6A21EE7A829EAAE35EAB036F9DFA1E9BE77D28322
        E5B274EFCDA1F9EBD8EFCB98E09E3AE4A338F7D779FDE18DFCE08EFDE28FFEE8
        AAFFF0D5EFB538FFFDF9C2690DE59C0CE49F18E7A523E9AB2CEAAF36EDB33EED
        B640F4CC6DF9E4A9F2CF8FECC376E5AA4DEEBE5EF1C964FCDE85FBDE87FCE08B
        FCE08CFEE59BFFF5DFF4CE6EF5D183FFFFFFC36B0FE59D0EE7A41BE19A1FE7A7
        35ECB339EDB540EEBA48F0BE4EF0C252F3C75DF5CC68F7D171F8D576FAD87DFA
        DA80FBDD85FCDF88FDE49BFFF2D3FBE3AAEFB63BFFFFFFFFFFFFC56F12E8A412
        D7881BC7792FCB7824E5A943EFBA43EEBC4AF0C052F2C55AF4C963F5CD6AF6D2
        71F8D477F9D87DFADA7FFBDD83FDE7A3FFF5DEFBE4AAEFB538FEF8EAFFFFFFFF
        FFFFC77113C87218D8A06AFFFFFFFAF2E9CE7E27DFA043F3CB6DF2C65CF2C759
        F4C960F4CC67F7D16EF7D573F8D778FBE098FFEFCDFEF0D4F4CE6EEFB63BFEF8
        EAFFFFFFFFFFFFFFFFFFBF6918EFD8C2FFFFFFFFFFFFFFFFFFFFFFFFE6B881D4
        8321E9BB6AF3D085F8DF99FBE5A9FCE5ABFEEBBCFFF3D8FAE3AFF4CF79EFB438
        F5D183FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF5EFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF1D3ABE6AF62DE952CE1982BEDB95BE7A938E8A934F0
        BF60F6D899FFFDF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 3
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object ComboBox1: TComboBox
      Left = 175
      Top = 31
      Width = 66
      Height = 27
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = ComboBox1Change
    end
    object StaticText8: TStaticText
      Left = 11
      Top = 34
      Width = 158
      Height = 23
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Postazione selezionata'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object LabeledEdit2: TLabeledEdit
      Tag = 507
      Left = 175
      Top = 75
      Width = 66
      Height = 27
      EditLabel.Width = 107
      EditLabel.Height = 19
      EditLabel.Caption = 'Pallet associato'
      EditLabel.Font.Charset = ANSI_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -16
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      LabelPosition = lpLeft
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      OnChange = LabeledEdit2Change
      OnClick = LabeledEdit2Click
    end
    object CheckBox1: TCheckBox
      Left = 66
      Top = 111
      Width = 133
      Height = 25
      Alignment = taLeftJustify
      Caption = 'Presenza pallet'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnMouseUp = CheckBox1MouseUp
    end
    object CheckBox2: TCheckBox
      Tag = 1
      Left = 15
      Top = 143
      Width = 184
      Height = 25
      Alignment = taLeftJustify
      Caption = 'Postazione disponibile'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnMouseUp = CheckBox1MouseUp
    end
    object CheckBox3: TCheckBox
      Tag = 2
      Left = 45
      Top = 174
      Width = 154
      Height = 25
      Alignment = taLeftJustify
      Caption = 'Pallet da scaricare'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnMouseUp = CheckBox1MouseUp
    end
    object BitBtn2: TBitBtn
      Left = 247
      Top = 139
      Width = 84
      Height = 60
      Caption = 'Scrivi'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C006000000000000000000000000000000000000FFFFFF5C8E59
        2A792B2B7F2C2C7E2D2C802D2C802D2C7D2C2C812D2C7D2C2C802D2C802D2C80
        2D2C7E2C2B7E2C2C7E2C2C7E2D2C7E2C2C7E2C2B7E2C2B7F2C2C792B5D8D58FF
        FFFF5C8F5937AF493BCF603BCD5F3CCE5F2FCB562AC85129C8502CC85128C850
        2CC85129C85029C85029C85029C8502BC8512BC8512AC9512FCA5639CE5F3DCD
        603BCF6036AE465C8E592A7C2C3BCF6139C55B39C75D39C75D21942E21942E21
        942E21942E21942E21942E21942E21942E21942E21942E21942E21942E21942E
        21942E39C75C39C75C39C65A3DCF622B7C2C2E842F3DCC623CC65D3CC85F24AC
        3EF2F1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF2F1F224AC3B39C75C3CC55B3ACA5E2C822F2E87303ACC63
        39C65E3CC86025AB3EF2F1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F1F224A83D39C75D39C55B3ACB622E
        842E2F88323ACD6639C76039C96226AC41F2F1F2FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F0F226AB3E3CC8
        5F38C55A3DCC622D852F2F8A323ACE6839C86339C76037C65D21942E1B91291B
        94291A91281B972D2C94340862090862091D962D1D962D1C922A1A90281C922A
        21922D37C45B39C75E3CC65E3ACC632E87302F8D3439CE6939C96738C86239C9
        653ACB663ACC673ACD673AD16C2C9434308735FFFFFFFFFFFF146B1333B24B3B
        D1673ACC653ACB633AC96239C86239C76039C7613ACD652F8A323090373AD16C
        39C96639C96638C96339C96639C96638CD6832B14C227723FFFFFFFFFFFFFFFF
        FFFCFDFE14681432B54E38CC6638C86339C86339C76339C7633CC8613ACD662F
        8B332B8E3337D06E38CB6938CB6639CB6A38CC673AD16F31B24E166E16FCFCFC
        FFFFFFFFFFFFFFFFFFFFFFFFFCFCFE1B761C35C35C38CB6638C86438C86439C9
        6737C86339CD672B8B2F5EB66E34D06C33C96537CC6939CC6A38CE6C32B65018
        711AFCFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6E3CF126E1136C55E
        38CB6A39CC6738C86534C86134CD665EB16B7EC68D90E5B95BD69031CC6A32CE
        6B34C7630F710FFCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC9E5D013721234C55D32CB6632CA645BD48B8FE3B57BC48B7AC58A8AE4B8
        8DE3B793E4BE6ADA982D8D34CDE7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCEE7D72D8D3269D99896E4BA8DE1B48AE2B479
        C3877AC98D89E3B98BE2B586DEAC77BC7ECDE7D7D7E9DBD9E9DBD9E9DBFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDAE9DDDAE9DDD7E9DCCEE7D776B97B89DE
        AC8BE1B18CE3B57AC4897DCA8F89E3BB88E3B983D7A680D29B80D29B71CA8B71
        CA8B96CFA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF72CA8C72CA8C79D196
        7ED29A80D29B83D6A387E2B58CE3B77AC78C7DCC928CE5BD8BE3B98BE4BB8BE5
        BB8BE5BC8BE5BE85DEAE9BD0A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C
        D2A982DEAA8CE4B989E3B789E3B68BE2B78AE1B58CE3B77CCA8F7DCE958CE6C0
        87E4BB8AE3BB8AE3BB8AE3BB8AE4BA83DDAD9FD3ABFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF9CD3A984DCAB88E3BA8BE2B787E2B78AE2B78AE2B789E4BB7D
        CB907DCF968CE6C38AE4BD8AE4BD8AE4BC8AE4BC8AE5BC83DEAE9FD5ACFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FD5AC81DDAD8BE4BA8AE3B98AE3B98AE3
        B98AE3B88CE5BD7DCD9380D1988BE8C48AE5C08AE5C08AE5BF87E4BF87E5C183
        E0B2A0D8AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2D8B180DDAE8BE5BC
        8AE4BC8AE4BB8AE4BC87E3BB8CE5BF7DCE9580D29B8BE8C68BE7C28BE7C28AE6
        C28CE7C28CE7C284E1B5A2D9B1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2
        D9B283E0B18BE5BE87E4BD8AE4C08AE4BD8AE4BD8BE6BF80D09881D49E8CEACA
        8AE7C48BE7C58AE7C48AE7C58AE8C683E0B6A7DEB9FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA5DDB885E1B68AE7C18AE5C08AE5C08AE5C08AE6C08BE7C480
        D19982D5A08BECCE8AE8C88AE8C88AE8C78AE8C78AE9C986E3BA88D49BA8DEBA
        A4DDB7A4DDB7A4DDB7A4DDB8A8DEBA87D39A85E2B68AE8C68AE6C08BE7C28AE6
        C28AE6C28DEAC880D29A94D9A78AE3BC8CEBCD8CEBCD8CEBCD8CEBCD8CEBCD8C
        EBCF8BECD089EBCF88EBCD87EBCD87EACB87EACB88EACC8AECCD8CEBCD8CEACA
        8CEACA8CEACA8CE9C98BE9C989E2B8A4DDB2FFFFFFA7E1B784DAA384D9A487DA
        A584D9A484D9A484D9A487DAA684D8A184D9A484D9A484D9A584D9A584D9A584
        D8A584D9A583D8A283D8A283D8A284D8A683D6A0A5DEB5FFFFFF}
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 3
      TabOrder = 7
      OnClick = BitBtn2Click
    end
    object CheckBox4: TCheckBox
      Left = 48
      Top = 340
      Width = 281
      Height = 25
      Caption = 'Gestione programma esclusa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnMouseUp = CheckBox4MouseUp
    end
    object mProgramMU: TMemo
      Left = 11
      Top = 270
      Width = 374
      Height = 68
      Color = clInfoBk
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 9
      OnClick = mProgramMUClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 456
    Top = 459
    Width = 396
    Height = 100
    Caption = 'Stato macchina'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    object Shape4: TShape
      Left = 153
      Top = 35
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object Shape5: TShape
      Left = 153
      Top = 62
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object Shape6: TShape
      Left = 313
      Top = 35
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object Shape7: TShape
      Left = 313
      Top = 62
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object StaticText10: TStaticText
      Left = 7
      Top = 34
      Width = 140
      Height = 23
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Porta aperta'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object StaticText11: TStaticText
      Left = 7
      Top = 61
      Width = 140
      Height = 23
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Ok cambio pallet'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object StaticText12: TStaticText
      Left = 179
      Top = 34
      Width = 128
      Height = 23
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Pallet sbloccato'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object StaticText9: TStaticText
      Left = 179
      Top = 61
      Width = 128
      Height = 23
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Pallet bloccato'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 15
    Top = 456
    Width = 403
    Height = 118
    Caption = 'Legenda stato pallet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    object Shape1: TShape
      Left = 21
      Top = 34
      Width = 52
      Height = 20
      Brush.Color = clGreen
    end
    object Shape2: TShape
      Left = 21
      Top = 60
      Width = 52
      Height = 20
      Brush.Color = clRed
    end
    object Shape3: TShape
      Left = 21
      Top = 86
      Width = 52
      Height = 20
      Brush.Color = clBlue
    end
    object Shape8: TShape
      Left = 174
      Top = 34
      Width = 52
      Height = 20
      Brush.Color = clPurple
    end
    object Shape9: TShape
      Left = 174
      Top = 59
      Width = 52
      Height = 20
      Brush.Color = clMaroon
    end
    object StaticText2: TStaticText
      Left = 79
      Top = 34
      Width = 82
      Height = 23
      Caption = 'Disponibile'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object StaticText3: TStaticText
      Left = 80
      Top = 60
      Width = 65
      Height = 23
      Caption = 'Lavorato'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object StaticText4: TStaticText
      Left = 80
      Top = 86
      Width = 128
      Height = 23
      AutoSize = False
      Caption = 'Prenotato'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object StaticText5: TStaticText
      Left = 232
      Top = 34
      Width = 169
      Height = 23
      Caption = 'Scarto semilavorato (2)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object StaticText6: TStaticText
      Left = 232
      Top = 59
      Width = 140
      Height = 23
      AutoSize = False
      Caption = 'Scarto (1)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  inline frmLayMU1: TfrmLayMU
    Left = 15
    Top = 58
    Width = 410
    Height = 390
    TabOrder = 6
    ExplicitLeft = 15
    ExplicitTop = 58
    ExplicitWidth = 410
  end
  object GroupBox4: TGroupBox
    Left = 457
    Top = 566
    Width = 396
    Height = 74
    Caption = 'Stato Segnali'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    object Shape10: TShape
      Left = 129
      Top = 19
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object Shape11: TShape
      Left = 129
      Top = 45
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
      OnMouseUp = Shape11MouseUp
    end
    object Shape12: TShape
      Left = 313
      Top = 19
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object Shape13: TShape
      Left = 313
      Top = 45
      Width = 20
      Height = 20
      Brush.Color = clLime
      Shape = stCircle
    end
    object StaticText1: TStaticText
      Left = 160
      Top = 44
      Width = 147
      Height = 23
      AutoSize = False
      Caption = 'Start enabled'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object StaticText7: TStaticText
      Left = 3
      Top = 44
      Width = 120
      Height = 23
      AutoSize = False
      Caption = 'Start command'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object StaticText13: TStaticText
      Left = 3
      Top = 22
      Width = 120
      Height = 23
      AutoSize = False
      Caption = 'Start search'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object StaticText14: TStaticText
      Left = 160
      Top = 22
      Width = 121
      Height = 23
      AutoSize = False
      Caption = 'Search complete'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object TimerOverride: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerOverrideTimer
    Left = 960
    Top = 144
  end
  object TimerUpd: TTimer
    Interval = 200
    OnTimer = TimerUpdTimer
    Left = 960
    Top = 200
  end
end
