//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Lavoro.h"
#include "ClientData.h"
#include "ModBus.h"
#include "InsNum.h"
#include "Main.h"
#include "conferma.h"
#include "DB.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LayMU"
#pragma resource "*.dfm"
TLavoroForm *LavoroForm;
//---------------------------------------------------------------------------
#define NativeScreenHeight	768
//---------------------------------------------------------------------------
__fastcall TLavoroForm::TLavoroForm(TComponent* Owner)
	: TForm(Owner)
{
	float k;
	int i;

	for(i = 0; i < MainForm->NPallet; i++) {
		frmLayMU1->P[i]->OnMouseDown = &MyShape1MouseDown;
	}
	k = (float)Screen->Height / (float)NativeScreenHeight;
	StringGrid1->ColWidths[0] *= k;
	StringGrid1->ColWidths[1] *= k;
	StringGrid1->ColWidths[2] *= k;
	StringGrid1->ColWidths[3] *= k;
	StringGrid1->DefaultRowHeight *= k;
}
//---------------------------------------------------------------------------

void TLavoroForm::CaricaComboBox(TComboBox *cb, int n)
{
	int i;

	cb->Clear();
	for (i = 1; i <= n ; i++) {
		cb->Items->Add(i);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::FormActivate(TObject *Sender)
{
	WindowState = wsMaximized;
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn3Click(TObject *Sender)
{
	Close();	
}
//---------------------------------------------------------------------------

void TLavoroForm::ColoraPallet(TMyShape *i, bool presenza, bool disponibile, bool lavorato, bool missione, bool stato)
{
	i->OnlyText = !presenza;
	if (missione) {
		i->Brush->Color = clBlue;
	} else if (disponibile) {
		i->Brush->Color = clGreen;
	} else if (lavorato) {
		if (stato == 1) {
			i->Brush->Color = clPurple;
		} else if (stato == 2) {
			i->Brush->Color = clMaroon;
		} else {
			i->Brush->Color = clRed;
		}
	} else {
		i->Brush->Color = clGray;
	}
}
//---------------------------------------------------------------------------

void TLavoroForm::Aggiorna() {
	AnsiString s;
	int pos, posm, poso, i, j, n, mw, bit;
	bool *presenza, *disponibile, *lavorato, *missione, *stato;
	TfrmLayMU* fm;
	TIndexList TabPallet;

	presenza = new bool[MainForm->NPallet];
	disponibile = new bool[MainForm->NPallet];
	lavorato = new bool[MainForm->NPallet];
	missione = new bool[MainForm->NPallet];
	stato = new bool[MainForm->NPallet];
	posm = ModBusDM->MW[855]; // MU
	poso = ModBusDM->MW[856]; // OP
	fm = frmLayMU1;
	DBDataModule->CaricaTabellaK("Pallet", "Pallet", TabPallet);
	if (posm) {
		pos = posm;
		fm->Repos(MainForm->PosMacchina);
	} else {
		pos = poso;
		fm->Repos(2);
	}
	n = (pos > 1) ? pos : 1;
	for(i = 0; i < MainForm->NPallet; i++) {
		fm->P[i]->Text = n;
		stato[i] = StrToIntDef(TabPallet[n]["Stato"], 0);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	n = (pos > 1) ? pos : 1;
	for(j = 0; j < MainForm->NPallet; j++) {
		mw = 891 + (n - 1) / 4;
		bit = (n - 1) % 4 * 4;
		presenza[j]    = ModBusDM->GetBit(mw, bit);
		disponibile[j] = ModBusDM->GetBit(mw, bit + 1);
		lavorato[j]    = ModBusDM->GetBit(mw, bit + 2);
		missione[j]    = ModBusDM->GetBit(mw, bit + 3);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	for(i = 0; i < MainForm->NPallet; i++) {
		ColoraPallet(fm->P[i], presenza[i], disponibile[i], lavorato[i], missione[i], stato[i]);
	}
	delete []presenza;
	delete []disponibile;
	delete []lavorato;
	delete []missione;
	delete []stato;
	n = ModBusDM->MW[850];
	switch(n) {
		case  95:
		case 195:
			s = " - " + _("Apertura porte");
			break;
		case 100:
		case 110:
			s = " - " + _("Carico in corso");
			break;
		case 120:
			s = " - " + _("Attesa fine rich.");
			break;
		case 190:
			s = " - " + _("Attesa scarico");
			break;
		case 200:
		case 210:
			s = " - " + _("Scarico macchina");
			break;
		case 300:
		case 400:
			s = " - " + _("Pallet in postazione operatore");
			break;
		case 500:
			s = " - " + _("Riposizionamento");
			break;
		default:
			s = "";
	}
	LabeledEdit1->Text = IntToStr(n) + s;
	LabeledEdit2->Text = ModBusDM->MW[851];
	LabeledEdit3->Text = ModBusDM->MW[852];
	LabeledEdit4->Text = ModBusDM->MW[853];
	LabeledEdit5->Text = ModBusDM->MW[854];
	if (!ComboBox1->DroppedDown)
		ComboBox1->ItemIndex = ModBusDM->MW[500] - 1;
	if (!ComboBox2->DroppedDown)
		ComboBox2->ItemIndex = ModBusDM->MW[501] - 1;
	if (!ComboBox3->DroppedDown)
		ComboBox3->ItemIndex = ModBusDM->MW[502];
	if (!ComboBox4->DroppedDown)
		ComboBox4->ItemIndex = ModBusDM->MW[504] - 1;
	if (!ComboBox5->DroppedDown)
		ComboBox5->ItemIndex = ModBusDM->MW[520] - 1;
	StaticText6->Caption = ModBusDM->MW[512];

	for (i = 0; i < MainForm->NPallet; i++)
    {
		StringGrid1->Cells[0][i + 1] = i + 1;
		StringGrid1->Cells[1][i + 1] = ModBusDM->MW[1100 + i];

		n = ModBusDM->MW[1130 + i];
		switch(n) {
			case 0:
				s = " - " + _("Non in corso");
				break;
			case 1:
				s = " - " + _("Carico in corso");
				break;
			case 2:
				s = " - " + _("Attesa scarico");
				break;
			case 3:
				s = " - " + _("Scarico in corso");
				break;
			default:
				s = "";
		}
		StringGrid1->Cells[2][i + 1] = IntToStr(n) + s;

		n = ModBusDM->MW[1160 + i];
		switch(n) {
			case 0:
				s = " - " + _("Carico/Scarico");
				break;
			case 1:
				s = " - " + _("Carico");
				break;
			case 2:
				s = " - " + _("Scarico");
				break;
			default:
				s = "";
		}
		StringGrid1->Cells[3][i + 1] =  IntToStr(n) + s;
	}
	CheckBox1->Checked = (ModBusDM->GetBit(10, 0));
	GroupBox3->Visible = !(ModBusDM->GetBit(5, 0));
	GroupBox4->Visible = (ModBusDM->GetBit(5, 0));
	BitBtn8->Visible = (MainForm->pwdlevel > 1);
	if (ModBusDM->GetBit(4, 3))
		BitBtn8->Caption = _("Demo\nmode ON");
	else
		BitBtn8->Caption = _("Demo\nmode OFF");
	StringGrid1->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::FormCreate(TObject *Sender)
{
	int i;

	TranslateComponent(this);
	CaricaComboBox(ComboBox1, MainForm->NPallet);
	CaricaComboBox(ComboBox2, MainForm->NPallet);
	CaricaComboBox(ComboBox4, MainForm->NPallet);
	StringGrid1->RowCount = MainForm->NPallet + 1;
	for (i = 1; i <= MainForm->NPallet; i++) {
		StringGrid1->Cells[0][i] = IntToStr(i);
	}
	StringGrid1->Cells[0][0] = _("Buffer");
	StringGrid1->Cells[1][0] = _("Pallet");
	StringGrid1->Cells[2][0] = _("Stato buffer");
	StringGrid1->Cells[3][0] = _("Tipo buffer");
}
//---------------------------------------------------------------------------


void __fastcall TLavoroForm::ComboBox1Change(TObject *Sender)
{
	ModBusDM->SetWord(500, StrToFloat(ComboBox1->Text));
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::ComboBox2Change(TObject *Sender)
{
	ModBusDM->SetWord(501, StrToFloat(ComboBox2->Text));
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::ComboBox3Change(TObject *Sender)
{
	ModBusDM->SetWord(502, StrToFloat(ComboBox3->ItemIndex));
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn1Click(TObject *Sender)
{
	if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente resettare la missione?")) == mrOk) {
		if (ConfermaDlg(_("ATTENZIONE"), _("Sei sicuro di voler resettare la missione?")) == mrOk) {
			ModBusDM->SetBit(510, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn2Click(TObject *Sender)
{
	ModBusDM->SetBit(503, 0);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn4Click(TObject *Sender)
{
	if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente cancellare il buffer ") + ComboBox1->Text + "?") == mrOk) {
		ModBusDM->SetBit(503, 1);
    }
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::ComboBox4Change(TObject *Sender)
{
	ModBusDM->SetWord(504, StrToFloat(ComboBox4->Text));
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::MyShape1MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	int i;

	i = StrToInt(((TMyShape*)Sender)->Text);
	ModBusDM->SetWord(504, i);
	ModBusDM->SetWord(501, i);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn5Click(TObject *Sender)
{
	ModBusDM->SetBit(505, 1);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn6Click(TObject *Sender)
{
	ModBusDM->SetBit(505, 0);
}
//---------------------------------------------------------------------------


void __fastcall TLavoroForm::CheckBox1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TCheckBox *cb;

	cb = (TCheckBox*)Sender;
	if (cb->Checked) {
		ModBusDM->SetBit(10, cb->Tag);
	} else {
		ModBusDM->ResetBit(10, cb->Tag);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::Label1Click(TObject *Sender)
{
	int i;

	i = StrToInt(((TLabel*)Sender)->Caption);
	ModBusDM->SetWord(504, i);
	ModBusDM->SetWord(501, i);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::FormDeactivate(TObject *Sender)
{
	ModBusDM->SetWord(500, 0);
	ModBusDM->SetWord(501, 0);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::StringGrid1SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
	ComboBox1->ItemIndex = ARow - 1;
	ModBusDM->SetWord(500, StrToFloat(ComboBox1->Text));
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn7MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->SetBit(505, 2);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::BitBtn7MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->ResetBit(505, 2);
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::ComboBox5Change(TObject *Sender)
{
	ModBusDM->SetWord(520, StrToFloat(ComboBox5->Text));
}
//---------------------------------------------------------------------------


void __fastcall TLavoroForm::BitBtn8Click(TObject *Sender)
{
	if (ModBusDM->GetBit(4, 3)) {
		ModBusDM->ResetBit(4, 3);
		BitBtn8->Caption = _("Demo\nmode OFF");
	} else {
		ModBusDM->SetBit(4, 3);
		BitBtn8->Caption = _("Demo\nmode ON");
	}
}
//---------------------------------------------------------------------------

void __fastcall TLavoroForm::StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State)
{
	AnsiString s;

	if ((ACol == 0) && (ARow > 0) && (ARow == ModBusDM->MW[851])) {
		StringGrid1->Canvas->Brush->Color = clLime;
		StringGrid1->Canvas->FillRect(Rect);
		Rect.Left += 5;
		s = _(StringGrid1->Cells[ACol][ARow]);
		DrawText(StringGrid1->Canvas->Handle, s.c_str(), -1, &Rect, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	}
}
//---------------------------------------------------------------------------

