//---------------------------------------------------------------------------

#ifndef InsNumH
#define InsNumH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Touch.Keyboard.hpp>
//---------------------------------------------------------------------------
class TInsNumForm : public TForm
{
__published:	// IDE-managed Components
	TTouchKeyboard *TouchKeyboard1;
	TLabeledEdit *Valore;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	void __fastcall FormActivate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TInsNumForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TInsNumForm *InsNumForm;
//---------------------------------------------------------------------------
#endif
