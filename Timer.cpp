//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Timer.h"
#include "ClientData.h"
#include "ModBus.h"
#include "InsNum.h"
#include "Main.h"
#include "DB.h"
#include "conferma.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTimerForm *TimerForm;
//---------------------------------------------------------------------------
__fastcall TTimerForm::TTimerForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TTimerForm::FormActivate(TObject *Sender)
{
	Aggiorna();
	WindowState = wsMaximized;
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::BitBtn3Click(TObject *Sender)
{
	Close();	
}
//---------------------------------------------------------------------------

void TTimerForm::Aggiorna() {
	int index;

	if (MainForm->pwdlevel == 0) {
    	Close();
	}
	BitBtn1->Enabled =  (MainForm->pwdlevel > 1);
	BitBtn2->Enabled =  (MainForm->pwdlevel > 1);

	LabeledEdit1->Text = ModBusDM->MW[100];
	LabeledEdit2->Text = ModBusDM->MW[101];
	LabeledEdit3->Text = ModBusDM->MW[102];
	LabeledEdit4->Text = ModBusDM->MW[103];
	LabeledEdit5->Text = ModBusDM->MW[104];
	LabeledEdit6->Text = ModBusDM->MW[105];
	LabeledEdit7->Text = ModBusDM->MW[106];
	LabeledEdit8->Text = ModBusDM->MW[107];
	LabeledEdit17->Text = ModBusDM->MW[108];
	LabeledEdit9->Text = ModBusDM->MW[109];
	LabeledEdit10->Text = ModBusDM->MW[110];
	LabeledEdit11->Text = ModBusDM->MW[111];
	LabeledEdit12->Text = ModBusDM->MW[112];
	LabeledEdit13->Text = ModBusDM->MW[113];
	LabeledEdit14->Text = ModBusDM->MW[114];
	LabeledEdit15->Text = ModBusDM->MW[115];
	LabeledEdit16->Text = ModBusDM->MW[116];
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::BitBtn1Click(TObject *Sender)
{
	TCursor Save_Cursor = Screen->Cursor;

	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	try {
		DBDataModule->SalvaParametri(ModBusDM->MW);
	} catch (...) {
	}
	Screen->Cursor = Save_Cursor;
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::BitBtn2Click(TObject *Sender)
{
	WORD MWR[2000];
	TCursor Save_Cursor = Screen->Cursor;

	if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente sovrascrivere i parametri?")) == mrOk) {
		Screen->Cursor = crHourGlass;    // Show hourglass cursor
		try {
			DBDataModule->LeggiParametri(MWR);
			ModBusDM->SetWord(30, MWR[30]);
			ModBusDM->WriteBlock(40, MWR + 40, 50);
			ModBusDM->WriteBlock(100, MWR + 100, 3);
			ModBusDM->WriteBlock(200, MWR + 200, 23);
			ModBusDM->WriteBlock(300, MWR + 300, 81);
			ModBusDM->WriteBlock(400, MWR + 400, 23);
		} catch (...) {
		}
		Screen->Cursor = Save_Cursor;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::LabeledEdit1Click(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------

void __fastcall TTimerForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

