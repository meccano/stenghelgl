//---------------------------------------------------------------------------

#ifndef GenericKeyboardH
#define GenericKeyboardH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Touch.Keyboard.hpp>
//---------------------------------------------------------------------------
class TfGenericKeyboard : public TForm
{
__published:	// IDE-managed Components
	TTouchKeyboard *TouchKeyboard1;
	TEdit *Edit1;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfGenericKeyboard(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfGenericKeyboard *fGenericKeyboard;
//---------------------------------------------------------------------------
#endif
