//---------------------------------------------------------------------------

#ifndef ModbusH
#define ModbusH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <IdModbusClient.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "ModbusTypes.hpp"
//---------------------------------------------------------------------------
#define TAGW_WORK_NUMBER_MU   			605		//1210 array y1..32 byte
#define TAGW_AREA_VERSO_MAZAK   	 	621		//1242
#define BIT_WORK_NUMBER_SEARCH_START_MU	0		//1242.0
#define BIT_CYCLE_START_COMMAND_MU   	1		//1242.1
#define BIT_RESET_COMMAND_MU   			2		//1242.2
#define BIT_BATCH_COMPLETE_MU   		3		//1242.3
//---------------------------------------------------------------------------
#define TAGW_CURRENT_WORK_NUMBER_MU   	650		//1300 array y1..32 byte
#define TAGW_AREA_DA_MAZAK   	 		666		//1332
#define BIT_WORK_NUMBER_SEARCH_FINISHED	0		//1332.0
#define BIT_CYCLE_START_ENABLED_MU   	1		//1332.1
#define BIT_MACHINE_RUNNING_MU 			2		//1332.2
#define BIT_END_OF_CYCLE_MU   			3		//1332.3
//---------------------------------------------------------------------------
//1206.11
#define TAGW_AREA_MNUALI   	 			603		//1206
#define BIT_SEGNALA_PALLET_IN:POSIZIONE	11		//11

struct tool {
	int n;
	int time1;
	int time2;
	int curtime;
	int rotto;
	int gemello;
};

//---------------------------------------------------------------------------
class TModBusDM : public TDataModule
{
__published:	// IDE-managed Components
	TIdModBusClient *IdModBusClient1;
	TTimer *TimerPLCRead;
	TTimer *TimerAllarmi;
	TTimer *TimerPrg;
	TTimer *TimerRunProgram;
	TTimer *timerscanprogram;
	TTimer *TimerFineScansione;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall TimerPLCReadTimer(TObject *Sender);
	void __fastcall IdModBusClient1Disconnected(TObject *Sender);
	void __fastcall IdModBusClient1Connected(TObject *Sender);
	void __fastcall IdModBusClient1ResponseError(const BYTE FunctionCode, const BYTE ErrorCode,
          const TModBusResponseBuffer &ResponseBuffer);
	void __fastcall TimerAllarmiTimer(TObject *Sender);
	void __fastcall TimerPrgTimer(TObject *Sender);
	void __fastcall TimerRunProgramTimer(TObject *Sender);
	//void __fastcall tLettureMUTimer(TObject *Sender);
	//void __fastcall tChekToolsTimer(TObject *Sender);
	void __fastcall tLogCommessaTimer(TObject *Sender);
	void __fastcall timerscanprogramTimer(TObject *Sender);
	void __fastcall TimerFineScansioneTimer(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TModBusDM(TComponent* Owner);
	bool GetBit(int mx, int bit);
	bool SetBit(int mx, int bit);
	bool ResetBit(int mx, int bit);
	long int GetDWord(int md);
	bool SetDWord(int md, float value, int decimal = 0);
	bool SetWord(int address, float value, int decimal = 0);
	bool WriteBlock(int address, WORD *data, int length);
	AnsiString GetString(int address, int length);
	void LeggiTestiAllarmi(AnsiString FileName);
	int NextActiveAlarm(int actall);
	bool CaricaTabellaUtensili(AnsiString FileName);
	bool UtensileDisponibile(int n, int primo);
	int TuttiUtensiliDisponibili(AnsiString FileName);
	int UtensiliDisponibili(AnsiString FileName);
	AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore);
	int CaricaDatiCommessa(AnsiString FileName, AnsiString &ordine, AnsiString &articolo, AnsiString &fase, AnsiString &disegno);
	void LogNuovaCommessa(int n);
	bool WriteWorkProgram(AnsiString WorkProgram);
	bool CheckWorkProgram();

	WORD MW[2000];
	int step;
	bool PLCErr;
	bool allbak[256];
	AnsiString alltxt[256];
	AnsiString ProgramName;
	int PalletDaMettereInMacchina;
	int PalletToltoDaMacchina;
	int PalletBak;
	int CambioPallet;
	int EsclusioneM30;
	tool TOOLS_TAB[500];
};
//---------------------------------------------------------------------------
extern PACKAGE TModBusDM *ModBusDM;
//---------------------------------------------------------------------------
#endif
