//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Segnalazioni.h"
#include "modbus.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#define NativeScreenHeight	768
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSegnalazioniForm *SegnalazioniForm;
//---------------------------------------------------------------------------
__fastcall TSegnalazioniForm::TSegnalazioniForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSegnalazioniForm::FormActivate(TObject *Sender)
{
	float k = (float)Screen->Height / (float)NativeScreenHeight;

	WindowState = wsMaximized;
	StringGrid1->ColWidths[0] = 10 * k;
	StringGrid1->ColWidths[1] = 100 * k;
	StringGrid1->ColWidths[2] = 800 * k;
	StringGrid1->DefaultRowHeight *= k;
	StringGrid1->Cells[1][0] = _("Tipo");
	StringGrid1->Cells[2][0] = _("Messaggio");
	Aggiorna();
	Timer1->Enabled = true;
	Timer2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSegnalazioniForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------


void TSegnalazioniForm::Aggiorna()
{
	int n, i, j, nall;

	nall = 0;
	for (i = 1; i <= 112; i++) {
		if (ModBusDM->allbak[i]) {
			nall++;
		}
	}
	StringGrid1->RowCount = nall + 1;
	n = 1;
	for (i = 1; i <= 112; i++) {
		if (ModBusDM->allbak[i]) {
			if (i < 96) {
				StringGrid1->Cells[1][n] = _("Allarme");
			} else {
				StringGrid1->Cells[2][n] = _("Warning");
			}
			StringGrid1->Cells[2][n] = ModBusDM->alltxt[i];
			n++;
		}
	}
}
//---------------------------------------------------------------------------

void TSegnalazioniForm::AggiornaPLC()
{
	TADOQuery *ADOQuery;
	AnsiString strsql;
	int n;

	LabeledEdit9->Text = FloatToStrF(ModBusDM->GetDWord(460), ffFixed, 15, 0);
	LabeledEdit10->Text = FloatToStrF(ModBusDM->GetDWord(461), ffFixed, 15, 0);
	LabeledEdit11->Text = FloatToStrF(ModBusDM->GetDWord(462), ffFixed, 15, 0);
	LabeledEdit12->Text = FloatToStrF(ModBusDM->GetDWord(463), ffFixed, 15, 0);
	LabeledEdit1->Text = ModBusDM->GetString(1000, 40);
	LabeledEdit3->Text = ModBusDM->GetString(1020, 40);
	LabeledEdit4->Text = ModBusDM->GetString(1040, 40);
	LabeledEdit5->Text = ModBusDM->GetString(1060, 40);
}
//---------------------------------------------------------------------------

void __fastcall TSegnalazioniForm::BitBtn1Click(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TSegnalazioniForm::BitBtn3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TSegnalazioniForm::Timer1Timer(TObject *Sender)
{
	Timer1->Enabled = false;
	Aggiorna();
	Timer1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSegnalazioniForm::Timer2Timer(TObject *Sender)
{
	Timer2->Enabled = false;
	AggiornaPLC();
	Timer2->Enabled = true;
}
//---------------------------------------------------------------------------



void __fastcall TSegnalazioniForm::StringGrid1DrawCell(TObject *Sender, int ACol,
          int ARow, TRect &Rect, TGridDrawState State)
{
	if ((ARow > 0) && (ACol > 1)) {
		if (StringGrid1->Cells[1][ARow] == _("Allarme")) {
			StringGrid1->Canvas->Brush->Color  = clRed;
			StringGrid1->Canvas->Font->Color = clWhite;
			StringGrid1->Canvas->Font->Style = TFontStyles() << fsBold;
		} else {
			StringGrid1->Canvas->Brush->Color = clYellow;
		}
		StringGrid1->Canvas->TextRect(Rect,Rect.left+4,Rect.Top+4,StringGrid1->Cells[ACol][ARow]);
	}
}
//---------------------------------------------------------------------------
void __fastcall TSegnalazioniForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

