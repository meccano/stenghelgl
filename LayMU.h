//---------------------------------------------------------------------------

#ifndef LayMUH
#define LayMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Graphics.hpp>
#include "MyShape.h"
//---------------------------------------------------------------------------
class TfrmLayMU : public TFrame
{
__published:	// IDE-managed Components
	TImage *imgMU;
	TImage *imgOP;
private:	// User declarations
public:		// User declarations
	__fastcall TfrmLayMU(TComponent* Owner);
	__fastcall ~TfrmLayMU();
	void Repos(int Tipo);
	TMyShape **P;
    int InitWidth, InitHeight;
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmLayMU *frmLayMU;
//---------------------------------------------------------------------------
#endif
