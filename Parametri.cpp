//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Parametri.h"
#include "ClientData.h"
#include "ModBus.h"
#include "InsNum.h"
#include "Main.h"
#include "DB.h"
#include "conferma.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TParametriForm *ParametriForm;
//---------------------------------------------------------------------------
__fastcall TParametriForm::TParametriForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TParametriForm::FormActivate(TObject *Sender)
{
	Aggiorna();
	WindowState = wsMaximized;
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::BitBtn3Click(TObject *Sender)
{
	Close();	
}
//---------------------------------------------------------------------------

void TParametriForm::Aggiorna() {
	int index;

	if (MainForm->pwdlevel == 0) {
    	Close();
	}
	BitBtn1->Enabled =  (MainForm->pwdlevel > 1);
	BitBtn2->Enabled =  (MainForm->pwdlevel > 1);
 	LabeledEdit1->Text = FloatToStrF(ModBusDM->GetDWord(25) / 100.0, ffFixed, 15, 2);
	LabeledEdit2->Text = FloatToStrF(ModBusDM->GetDWord(20) / 100.0, ffFixed, 15, 2);
	LabeledEdit3->Text = FloatToStrF(ModBusDM->GetDWord(21) / 100.0, ffFixed, 15, 2);
	LabeledEdit4->Text = FloatToStrF(ModBusDM->GetDWord(22) / 100.0, ffFixed, 15, 2);
	LabeledEdit5->Text = FloatToStrF(ModBusDM->GetDWord(23) / 100.0, ffFixed, 15, 2);
	LabeledEdit6->Text = FloatToStrF(ModBusDM->GetDWord(30) / 100.0, ffFixed, 15, 2);
	LabeledEdit7->Text = FloatToStrF(ModBusDM->GetDWord(32) / 100.0, ffFixed, 15, 2);
	LabeledEdit8->Text = FloatToStrF(ModBusDM->GetDWord(24) / 100.0, ffFixed, 15, 2);
	LabeledEdit9->Text = FloatToStrF(ModBusDM->GetDWord(33) / 100.0, ffFixed, 15, 2);
	LabeledEdit10->Text = FloatToStrF(ModBusDM->GetDWord(34) / 100.0, ffFixed, 15, 2);
	LabeledEdit11->Text = FloatToStrF(ModBusDM->GetDWord(35) / 100.0, ffFixed, 15, 2);
	LabeledEdit12->Text = FloatToStrF(ModBusDM->GetDWord(36) / 100.0, ffFixed, 15, 2);
	LabeledEdit13->Text = FloatToStrF(ModBusDM->GetDWord(38) / 100.0, ffFixed, 15, 2);
	LabeledEdit14->Text = FloatToStrF(ModBusDM->GetDWord(40) / 100.0, ffFixed, 15, 2);
	LabeledEdit15->Text = FloatToStrF(ModBusDM->GetDWord(41) / 100.0, ffFixed, 15, 2);
	LabeledEdit16->Text = FloatToStrF(ModBusDM->GetDWord(42) / 100.0, ffFixed, 15, 2);
	LabeledEdit17->Text = FloatToStrF(ModBusDM->GetDWord(43) / 100.0, ffFixed, 15, 2);
	LabeledEdit20->Text = FloatToStrF(ModBusDM->GetDWord(44) / 100.0, ffFixed, 15, 2);
	LabeledEdit36->Text = FloatToStrF(ModBusDM->GetDWord(45) / 100.0, ffFixed, 15, 2);
	LabeledEdit37->Text = FloatToStrF(ModBusDM->GetDWord(46) / 100.0, ffFixed, 15, 2);
	LabeledEdit38->Text = FloatToStrF(ModBusDM->GetDWord(47) / 100.0, ffFixed, 15, 2);
	LabeledEdit19->Text = FloatToStrF(ModBusDM->GetDWord(26) / 100.0, ffFixed, 15, 2);
	index = 100 + (TabControl1->TabIndex * 50);
	LabeledEdit21->Text = FloatToStrF(ModBusDM->GetDWord(index) / 100.0, ffFixed, 15, 2);
	LabeledEdit22->Text = FloatToStrF(ModBusDM->GetDWord(index + 1) / 100.0, ffFixed, 15, 2);
	LabeledEdit23->Text = FloatToStrF(ModBusDM->GetDWord(index + 2) / 100.0, ffFixed, 15, 2);
	LabeledEdit24->Text = FloatToStrF(ModBusDM->GetDWord(index + 3), ffFixed, 15, 0);
	LabeledEdit25->Text = FloatToStrF(ModBusDM->GetDWord(index + 4), ffFixed, 15, 0);
	LabeledEdit26->Text = FloatToStrF(ModBusDM->GetDWord(index + 5), ffFixed, 15, 0);
	LabeledEdit27->Text = FloatToStrF(ModBusDM->GetDWord(index + 6), ffFixed, 15, 0);
	LabeledEdit28->Text = FloatToStrF(ModBusDM->GetDWord(index + 7), ffFixed, 15, 0);
	LabeledEdit29->Text = FloatToStrF(ModBusDM->GetDWord(index + 8) / 100.0, ffFixed, 15, 2);
	LabeledEdit30->Text = ModBusDM->MW[index*2 + 18];
	LabeledEdit31->Text = ModBusDM->MW[index*2 + 19];
	LabeledEdit32->Text = ModBusDM->MW[index*2 + 20];
	LabeledEdit33->Text = ModBusDM->MW[index*2 + 21];
	LabeledEdit34->Text = ModBusDM->MW[index*2 + 22];
	LabeledEdit35->Text = FloatToStrF(ModBusDM->GetDWord(28) / 100.0, ffFixed, 15, 2);
	LabeledEdit39->Text = FloatToStrF(ModBusDM->GetDWord(450) / 100.0, ffFixed, 15, 2);
	LabeledEdit40->Text = FloatToStrF(ModBusDM->GetDWord(451) / 100.0, ffFixed, 15, 2);
	LabeledEdit41->Text = FloatToStrF(ModBusDM->GetDWord(452) / 100.0, ffFixed, 15, 2);
	LabeledEdit42->Text = FloatToStrF(ModBusDM->GetDWord(27) / 100.0, ffFixed, 15, 2);
	LabeledEdit43->Text = FloatToStrF(ModBusDM->GetDWord(29) / 100.0, ffFixed, 15, 2);
	if (TabControl1->TabIndex == 1) {
		LabeledEdit18->Text = ModBusDM->MW[index*2 + 80];
		LabeledEdit18->Visible = true;
	} else {
		LabeledEdit18->Visible = false;
	}
	if (TabControl1->TabIndex == 2) {
		LabeledEdit44->Text = ModBusDM->MW[index*2 + 80];
		LabeledEdit44->Visible = true;
	} else {
		LabeledEdit44->Visible = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::LabeledEdit2Click(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetDWord(le->Tag, StrToFloat(le->Text), 2);
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::LabeledEdit1Click(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::LabeledEdit21Click(TObject *Sender)
{
	int index;
	TLabeledEdit *le;

	index = 100 + (TabControl1->TabIndex * 50);
	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetDWord(index + le->Tag, StrToFloat(le->Text), 2);
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::LabeledEdit24Click(TObject *Sender)
{
	int index;
	TLabeledEdit *le;

	index = 100 + (TabControl1->TabIndex * 50);
	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetDWord(index + le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::LabeledEdit30Click(TObject *Sender)
{
	int index;
	TLabeledEdit *le;

	index = 100 + (TabControl1->TabIndex * 50);
	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(index*2 + le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::BitBtn1Click(TObject *Sender)
{
	TCursor Save_Cursor = Screen->Cursor;

	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	try {
		DBDataModule->SalvaParametri(ModBusDM->MW);
	} catch (...) {
	}
	Screen->Cursor = Save_Cursor;
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::BitBtn2Click(TObject *Sender)
{
	WORD MWR[2000];
	TCursor Save_Cursor = Screen->Cursor;

	if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente sovrascrivere i parametri?")) == mrOk) {
		Screen->Cursor = crHourGlass;    // Show hourglass cursor
		try {
			DBDataModule->LeggiParametri(MWR);
			ModBusDM->SetWord(30, MWR[30]);
			ModBusDM->WriteBlock(40, MWR + 40, 50);
			ModBusDM->WriteBlock(100, MWR + 100, 3);
			ModBusDM->WriteBlock(200, MWR + 200, 23);
			ModBusDM->WriteBlock(300, MWR + 300, 81);
			ModBusDM->WriteBlock(400, MWR + 400, 23);
		} catch (...) {
		}
		Screen->Cursor = Save_Cursor;
	}
}
//---------------------------------------------------------------------------


void __fastcall TParametriForm::LEPalletNumberClick(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK)
    {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(le->Tag, StrToInt(le->Text), 0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TParametriForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

