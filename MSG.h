//---------------------------------------------------------------------------

#ifndef MSGH
#define MSGH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TMSGForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TTimer *Timer1;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMSGForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMSGForm *MSGForm;
//---------------------------------------------------------------------------
#endif
