//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Conferma.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TConfermaForm *ConfermaForm;
//---------------------------------------------------------------------------
__fastcall TConfermaForm::TConfermaForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

int ConfermaDlg(AnsiString Titolo, AnsiString Testo)
{
	int res;

	ConfermaForm->Caption = Titolo;
    ConfermaForm->Testo->Caption = Testo;
	res = ConfermaForm->ShowModal();
	return res;
}
//---------------------------------------------------------------------------
void __fastcall TConfermaForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

