object DBDataModule: TDBDataModule
  OldCreateOrder = False
  Height = 321
  Width = 449
  object ADOConnection1: TADOConnection
    CommandTimeout = 3
    ConnectionString = 
      'ConnectionString=Provider=SQLOLEDB.1;Persist Security Info=False' +
      ';User ID=sa;Initial Catalog=COMPACOTTOMAN;Data Source=(local);'
    ConnectionTimeout = 3
    LoginPrompt = False
    Provider = 'SQLNCLI10.1'
    Left = 32
    Top = 8
  end
  object TimerDBCheck: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerDBCheckTimer
    Left = 32
    Top = 88
  end
  object TimerClr: TTimer
    Enabled = False
    Interval = 3600000
    OnTimer = TimerClrTimer
    Left = 32
    Top = 160
  end
end
