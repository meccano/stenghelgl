object ManualiForm: TManualiForm
  Left = 459
  Top = 109
  Caption = 'Manuali'
  ClientHeight = 649
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 19
  object sAgganciSbloccato: TShape
    Left = 190
    Top = 499
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sAgganciBloccato: TShape
    Left = 190
    Top = 520
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sInnestiAvanti: TShape
    Left = 190
    Top = 432
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sInnestiIndietro: TShape
    Left = 190
    Top = 458
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sPulizia: TShape
    Left = 461
    Top = 368
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sAttivoSblocco: TShape
    Left = 190
    Top = 541
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sSbloccoPallet: TShape
    Left = 190
    Top = 592
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sCtrlPalletSbloccato: TShape
    Left = 459
    Top = 447
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sEvPresenzaPallet: TShape
    Left = 543
    Top = 552
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object sEvControlloPEL: TShape
    Left = 543
    Top = 592
    Width = 15
    Height = 15
    Brush.Color = clLime
    Shape = stCircle
  end
  object DBCheckBox2: TDBCheckBox
    Left = 734
    Top = 8
    Width = 17
    Height = 17
    Color = clBtnFace
    DataField = 'Ribaltamento'
    ParentColor = False
    TabOrder = 0
    ValueChecked = '1'
    ValueUnchecked = '0'
    Visible = False
  end
  object Panel7: TPanel
    Left = 0
    Top = 0
    Width = 992
    Height = 49
    Align = alTop
    Alignment = taLeftJustify
    Caption = '   Manuali'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 839
    Top = 49
    Width = 153
    Height = 600
    Align = alRight
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 500
      Width = 151
      Height = 99
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 151
        Height = 10
        Align = alTop
        Shape = bsTopLine
        ExplicitWidth = 223
      end
      object BitBtn3: TBitBtn
        Left = 16
        Top = 18
        Width = 116
        Height = 65
        Caption = 'Chiudi'
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C006000000000000000000000000000000000000FFFFFF6763CF
          1712B61813BA1812BA1812BA1812BA1712B91712B91712B91711B91711B91711
          B91711B91711B81711B81711B81711B81711B81711B81711B71610B36662CDFF
          FFFF6966D4201DD52724ED2624ED2623ED2622ED2622ED2621ED2621ED2520ED
          2520ED251FED251EED241EED241DED241DED241DED241CED241CED241CED241C
          ED241CED1E17D36763D11A16BD2929EE2829EE2828EE2828EE2220DA2321DF27
          26ED2725ED2725ED2724ED2724ED2624ED2623ED2622ED2622ED2621ED1A15C2
          211CDF2520ED251FED251EED241EED1812B91D19C42A2EEE2A2DEE2A2DEE2526
          E1140FAA140FAA2525E1292AEE2929EE2929EE2829EE2828EE2828EE2827ED28
          26ED1A16C1130DA6140FAA2320E02724ED2624ED2623ED1A15C01E1BC72C33EE
          2C32EE272BE21611AF6C69CA403CBA1611AF2629E22B2FEE2A2EEE2A2DEE2A2D
          EE2A2CEE2A2CEE1C19C5140FABA7A5DF6C69CA1610AF2524E12828EE2828EE1C
          17C31F1DC92D37EE2930E31813B46D6ACDFFFFFFF0F0FA423DBF1813B4282CE3
          2C33EE2C32EE2C31EE2C31EE2324D71611B0A8A5E1FFFFFFFFFFFF6D6ACD1812
          B42526DF2A2CEE1D19C6201FCC2F3DEF2225CE1812B6A9A6E4FFFFFFFFFFFFF1
          F0FA6E6BD11915B92930E42D37EE2D37EE2528D91812B6A8A6E3FFFFFFFFFFFF
          F1F0FB433FC41914B9282CE32C31EE1F1BC92120CE3141EF3141EF2225CE1A14
          BBA9A7E6FFFFFFFFFFFFFFFFFF706CD41B16BF2B34E5272CDB1A14BBA9A7E5FF
          FFFFFFFFFFFFFFFF4540C81B16BE2931E42D37EE2D36EE1F1DCB2222D13346EF
          3245EF3245EF2427D11B15C0AAA7E8FFFFFFFFFFFFFFFFFF716DD71D17C31B15
          C0AAA7E7FFFFFFFFFFFFFFFFFF716DD81D17C32B35E62F3CEE2F3BEE2E3BEE21
          1ECD2423D3344AEF344AEF3449EF3449EF2932DC1D16C4AAA8E9FFFFFFFFFFFF
          FFFFFF716DDA716DDAFFFFFFFFFFFFFFFFFF726DDB1E18C72D3AE73141EF3141
          EF3040EF3040EF2220D02525D5374EF0374DEF364DEF364CEF364CEF2D38E01E
          16C9ABA8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF726EDD1F19CB2F3EE8
          3346EF3346EF3245EF3245EF3244EF2322D35656E04C61F23951F03950F03850
          F0384FF0384FF02E3AE21F17CD817DE3FFFFFFFFFFFFFFFFFFFFFFFF736EE01F
          17CD3141E8354BEF354BEF344AEF344AEF3449EF475AF15454DE5D5DE36D7EF4
          6679F45066F23E55F03A52F03A52F03547EA211CD3736FE2FFFFFFFFFFFFFFFF
          FFFFFFFF817DE52018D12F3AE4384EF0374EF03A50F04D60F16274F36879F35B
          5BE15E5EE56E81F56E81F56E80F46E80F45E73F34E5EEE302BDA746FE5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABA8EF2F27D84752EA5C6FF36B7CF46B7C
          F46B7CF46A7CF45D5CE35F5FE77184F57083F57083F57083F56B79F25A56E497
          94EDFFFFFFFFFFFFFFFFFF9E9BEF9490EDFFFFFFFFFFFFFFFFFFC1BFF45953E3
          656EEE6D7EF46C7EF46C7EF46C7EF45D5DE55F60E97286F57286F57186F56D7C
          F25B56E79794EFFFFFFFFFFFFFFFFFFFC1BFF65953E65953E69794F0FFFFFFFF
          FFFFFFFFFFC1BFF65953E66164EC6E81F56E80F46E80F45E5EE76060EA7489F5
          7488F56E7EF35B56EA7873EDF5F4FEFFFFFFFFFFFFC1BFF75953E96973F06D7C
          F35B56EA9794F1FFFFFFFFFFFFFFFFFFC1BFF75953E96265EE7083F57083F55F
          5FE86061EC758BF57080F35C57EC7974EFF5F4FEFFFFFFFFFFFFC1BFF85A54EC
          6A75F17488F57488F56F7EF35C57EC9894F3FFFFFFFFFFFFFFFFFFC1BFF85A54
          EC646AF07185F55F60EA6162EE778DF56E7BF35C58EF9894F5FFFFFFFFFFFFC1
          BFF95A54EE6A73F2758BF5758AF5758AF5758AF57080F45C57EF7974F2F5F4FE
          FFFFFF9894F55C57EF6F7FF47388F56060EB6263EF798FF5798FF57384F45D58
          F09895F6C2BFFA5B55F0666AF2788EF5778DF5778DF5778DF5778CF5778CF571
          82F45D58F07A75F39895F65C58F07081F4758AF5758AF56061ED6263F07B92F6
          7B91F67B91F67586F55D59F25B55F2676BF37A90F57A90F5798FF5798FF5798F
          F5798FF5798FF5788EF57384F45D59F25D59F27283F4778DF5778DF5778CF561
          62EE6161F17D93F67D93F67D93F67D93F67787F56A70F47C92F67C92F67C92F6
          7C92F67B91F67B91F67A91F57A90F57A90F57A90F57485F4707EF4798FF5798F
          F5798FF5798FF5605FEF9797F6717CF47E94F67E94F67E94F67E94F67E94F67E
          94F67E94F67E94F67D93F67D93F67D93F67D93F67D93F67D92F67C92F67C92F6
          7C92F67C92F67B91F67B91F66F7AF39796F5FFFFFF9897F86261F36465F36465
          F36465F36465F36465F36465F36465F36465F36465F36465F36465F36465F364
          65F36465F36465F36465F26464F26464F26160F29797F7FFFFFF}
        Spacing = 10
        TabOrder = 0
        OnClick = BitBtn3Click
      end
    end
  end
  object Panel2: TPanel
    Left = 24
    Top = 64
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Jog rotazione'
    ParentBackground = False
    TabOrder = 3
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel4: TPanel
    Tag = 1
    Left = 24
    Top = 136
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Jog verticale'
    ParentBackground = False
    TabOrder = 4
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel5: TPanel
    Tag = 2
    Left = 24
    Top = 208
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Jog sfilo'
    ParentBackground = False
    TabOrder = 5
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel6: TPanel
    Tag = 3
    Left = 24
    Top = 280
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Riparo macchina'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 6
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel8: TPanel
    Tag = 4
    Left = 24
    Top = 352
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Sollevatore pallet'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 7
    OnMouseDown = PanelMX2MouseDown
  end
  object LabeledEdit1: TLabeledEdit
    Left = 208
    Top = 64
    Width = 89
    Height = 27
    EditLabel.Width = 136
    EditLabel.Height = 19
    EditLabel.Caption = 'Posizione rotazione'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 8
  end
  object GroupBox1: TGroupBox
    Left = 584
    Top = 64
    Width = 241
    Height = 386
    Caption = 'Setup encoder'
    TabOrder = 9
    object Panel9: TPanel
      Left = 16
      Top = 32
      Width = 209
      Height = 58
      BevelWidth = 3
      Caption = 'Setup rotazione'
      ParentBackground = False
      TabOrder = 0
      OnMouseDown = PanelMX3MouseDown
    end
    object Panel10: TPanel
      Tag = 1
      Left = 16
      Top = 102
      Width = 209
      Height = 58
      BevelWidth = 3
      Caption = 'Setup verticale'
      ParentBackground = False
      TabOrder = 1
      OnMouseDown = PanelMX3MouseDown
    end
    object Panel11: TPanel
      Tag = 2
      Left = 16
      Top = 172
      Width = 209
      Height = 58
      BevelWidth = 3
      Caption = 'Setup sfilo'
      ParentBackground = False
      TabOrder = 2
      OnMouseDown = PanelMX3MouseDown
    end
    object Panel12: TPanel
      Tag = 3
      Left = 16
      Top = 242
      Width = 209
      Height = 58
      BevelWidth = 3
      Caption = 'Setup ultrasuoni'
      ParentBackground = False
      TabOrder = 3
      OnMouseDown = PanelMX3MouseDown
    end
    object Panel21: TPanel
      Tag = 4
      Left = 16
      Top = 312
      Width = 209
      Height = 58
      BevelWidth = 3
      Caption = 'Home macchina'
      ParentBackground = False
      TabOrder = 4
      OnMouseDown = Panel21MouseDown
    end
  end
  object LabeledEdit2: TLabeledEdit
    Left = 208
    Top = 136
    Width = 89
    Height = 27
    EditLabel.Width = 129
    EditLabel.Height = 19
    EditLabel.Caption = 'Posizione verticale'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 10
  end
  object LabeledEdit3: TLabeledEdit
    Left = 208
    Top = 208
    Width = 89
    Height = 27
    EditLabel.Width = 100
    EditLabel.Height = 19
    EditLabel.Caption = 'Posizione sfilo'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 11
  end
  object LabeledEdit4: TLabeledEdit
    Left = 208
    Top = 239
    Width = 89
    Height = 27
    EditLabel.Width = 169
    EditLabel.Height = 19
    EditLabel.Caption = 'Quota riposizionamento'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 12
  end
  object LabeledEdit5: TLabeledEdit
    Left = 208
    Top = 167
    Width = 89
    Height = 27
    EditLabel.Width = 169
    EditLabel.Height = 19
    EditLabel.Caption = 'Quota riposizionamento'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 13
  end
  object CheckBox2: TCheckBox
    Tag = 1
    Left = 568
    Top = 548
    Width = 265
    Height = 25
    Caption = 'Esclusione controllo presenza'
    TabOrder = 14
    OnMouseUp = CheckBox2MouseUp
  end
  object Panel13: TPanel
    Left = 600
    Top = 456
    Width = 209
    Height = 58
    BevelWidth = 3
    Caption = 'Manuali manutenzione'
    ParentBackground = False
    TabOrder = 15
    OnMouseDown = Panel13MouseDown
  end
  object LabeledEdit6: TLabeledEdit
    Tag = 21
    Left = 208
    Top = 272
    Width = 89
    Height = 27
    Color = clBtnFace
    EditLabel.Width = 166
    EditLabel.Height = 19
    EditLabel.Caption = 'Quota pallet magazzino'
    LabelPosition = lpRight
    ReadOnly = True
    TabOrder = 16
  end
  object Panel14: TPanel
    Tag = 5
    Left = 24
    Top = 426
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Innesti'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 17
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel15: TPanel
    Tag = 6
    Left = 24
    Top = 498
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Agganci'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 18
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel16: TPanel
    Tag = 7
    Left = 24
    Top = 570
    Width = 160
    Height = 58
    BevelWidth = 3
    Caption = 'Blocco/sblocco pallet'
    ParentBackground = False
    TabOrder = 19
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel17: TPanel
    Tag = 8
    Left = 320
    Top = 352
    Width = 133
    Height = 58
    BevelWidth = 3
    Caption = 'Soffi'
    ParentBackground = False
    TabOrder = 20
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel18: TPanel
    Tag = 9
    Left = 320
    Top = 426
    Width = 133
    Height = 58
    BevelWidth = 3
    Caption = 'Controlli'
    ParentBackground = False
    TabOrder = 21
    OnMouseDown = PanelMX2MouseDown
  end
  object Panel19: TPanel
    Tag = 1
    Left = 320
    Top = 498
    Width = 133
    Height = 58
    BevelWidth = 3
    Caption = 'Sblocca pallet'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 22
    OnMouseDown = PanelMX4MouseDown
  end
  object Panel20: TPanel
    Tag = 2
    Left = 320
    Top = 570
    Width = 133
    Height = 58
    BevelWidth = 3
    Caption = 'Blocca pallet'
    Enabled = False
    FullRepaint = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 23
    OnMouseDown = PanelMX4MouseDown
  end
  object CheckBox1: TCheckBox
    Tag = 2
    Left = 568
    Top = 588
    Width = 265
    Height = 25
    Caption = 'Esclusione PEL macchina utensile'
    TabOrder = 24
    OnMouseUp = CheckBox2MouseUp
  end
  object StaticText12: TStaticText
    Left = 208
    Top = 499
    Width = 59
    Height = 20
    Caption = 'Sbloccato'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 25
  end
  object StaticText1: TStaticText
    Left = 208
    Top = 520
    Width = 51
    Height = 20
    Caption = 'Bloccato'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 26
  end
  object StaticText2: TStaticText
    Left = 208
    Top = 432
    Width = 72
    Height = 20
    Caption = 'Com. avanti'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 27
  end
  object StaticText3: TStaticText
    Left = 208
    Top = 458
    Width = 81
    Height = 20
    Caption = 'Com. indietro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 28
  end
  object StaticText4: TStaticText
    Left = 479
    Top = 368
    Width = 40
    Height = 20
    Caption = 'Pulizia'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 29
  end
  object StaticText5: TStaticText
    Left = 208
    Top = 541
    Width = 82
    Height = 20
    Caption = 'Attivo sblocco'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 30
  end
  object StaticText6: TStaticText
    Left = 211
    Top = 574
    Width = 78
    Height = 57
    AutoSize = False
    Caption = 'Comando sblocco pallet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 31
  end
  object StaticText7: TStaticText
    Left = 477
    Top = 432
    Width = 92
    Height = 52
    AutoSize = False
    Caption = 'Ctrl pallet sbloccato'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 32
  end
  object StaticText8: TStaticText
    Left = 459
    Top = 541
    Width = 80
    Height = 37
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Ev presenza pallet'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 33
  end
  object StaticText9: TStaticText
    Left = 459
    Top = 581
    Width = 80
    Height = 37
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Ev controllo PEL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 34
  end
  object Timer1: TTimer
    Left = 1240
    Top = 64
  end
  object TimerUpd: TTimer
    Interval = 200
    OnTimer = TimerUpdTimer
    Left = 856
    Top = 64
  end
end
