//---------------------------------------------------------------------------

#pragma hdrstop

#include "Modbus.h"
#include "DB.h"
#include "ping.h"
#include <inifiles.hpp>
#include <math.hpp>
#include <stdio.h>
#include "LogEventi.h"
#include "Util.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma link "ModbusTypes"
#pragma resource "*.dfm"
TModBusDM *ModBusDM;

//---------------------------------------------------------------------------
__fastcall TModBusDM::TModBusDM(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::DataModuleCreate(TObject *Sender)
{
	AnsiString PLCAddress, lang, FileName;
	TIniFile *Ini;
	int i;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	PLCAddress = Ini->ReadString("PLC", "PLCAddress", "127.0.0.1");
	lang = Ini->ReadString("GENERALE", "Language", "it");
	delete(Ini);
	if (PLCAddress != "")
		IdModBusClient1->Host = PLCAddress;
	else
		IdModBusClient1->Host = "127.0.0.1";
	for (i = 0 ; i < 256; i++) {
		allbak[i] = false;
		alltxt[i] = "";
	}
	FileName.printf("Allarmi-%s.txt", lang.c_str());
	LeggiTestiAllarmi(FileName);
	step = 0;
	try {
		IdModBusClient1->Connect();
		PLCErr = false;
	} catch(...) {
		PLCErr = true;
	}
	PalletBak = 0;
	ProgramName = "";
	CambioPallet = 0;
	TimerPLCRead->Enabled 	= true;
	TimerAllarmi->Enabled 	= true;
	TimerPrg->Enabled 		= true;
}
//---------------------------------------------------------------------------
// La lettura massima e' di 125  word
// per cui word finale e' = ( addres + nword -1 )


void __fastcall TModBusDM::TimerPLCReadTimer(TObject *Sender)
{
	int address, nword;

	TimerPLCRead->Enabled = false;
	if (PLCErr) {
		try {
			IdModBusClient1->Connect();
//			PLCErr = false;
		} catch(...) {
//			PLCErr = true;
		}
		TimerPLCRead->Enabled = true;
		return;
	}
	switch(step) {
		case 0:
			address = 1;
			nword = 125;
			break;
		case 1:
			address = 200;
			nword = 100;
			break;
		case 2:
			address = 300;
			nword = 100;
			break;
		case 3:
			address = 400;
			nword = 100;
			break;
		case 4:
			address = 500;
			nword = 100;   //110
			break;
		case 5:                //nn c'era
			address = 600;
			nword = 100;
			break;
		case 6:
			address = 800;
			nword = 100;
			break;
		case 7:
			address = 900;
			nword = 100;
			break;
		case 8:
			address = 1000;
			nword = 80;
			break;
		case 9:
			address = 1100;
			nword = 100;
			break;
//		case 9:
//			address = 1200;
//			nword = 100;
//			break;
//		case 10:
//			address = 1300;
//			nword = 100;
//			break;
	}
	try {
		if (IdModBusClient1->ReadHoldingRegisters(address, nword, MW + address, nword * 2)) {//address, nword, MW + address, nword * 2)) {
			(step < 9) ? step++ : step = 0;
		} else {
			IdModBusClient1->Disconnect(false);
			PLCErr = true;
		}
	} catch(...) {
		IdModBusClient1->Disconnect(false);
		PLCErr = true;
	}
	TimerPLCRead->Enabled = true;
}
//---------------------------------------------------------------------------

bool TModBusDM::GetBit(int mx, int bit)
{
	bool res;

	res = ((MW[mx] & (1 << bit)) != 0);
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetBit(int mx, int bit)
{
	bool res = false;
	WORD val;

	try {
		val = (MW[mx] | (1 << bit));
		MW[mx] = val;
		res = IdModBusClient1->WriteRegister(mx, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::ResetBit(int mx, int bit)
{
	bool res = false;
	WORD val;

	try {
		val = (MW[mx] & ~(1 << bit));
		MW[mx] = val;
		res = IdModBusClient1->WriteRegister(mx, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

long int TModBusDM::GetDWord(int md)
{
	long int res;

	res = (MW[md*2 + 1] << 16) + MW[md*2];
	return res;
}
//---------------------------------------------------------------------------

AnsiString TModBusDM::GetString(int address, int length)
{
	int i;
	AnsiString res = "";

	for (i = 0; i < (length/2); i++) {
		res = res + (char)LOBYTE(MW[address + i]) + (char)HIBYTE(MW[address + i]);
	}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetDWord(int md, float value, int decimal)
{
	bool res = false;
	DWORD val;

	try {
		val = RoundTo(value * pow10(decimal), 0);
		MW[md*2 + 1] = HIWORD(val);
		MW[md*2] = LOWORD(val);
		val =  (MW[md*2] << 16) + MW[md*2 + 1];
		res = IdModBusClient1->WriteDWord(md * 2, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetWord(int address, float value, int decimal)
{
	bool res = false;
	int val;

	try {
		val = RoundTo(value * pow10(decimal), 0);
		MW[address] = val;
		res = IdModBusClient1->WriteRegister(address, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::WriteBlock(int address, WORD *data, int length)
{
	bool res = false;

	try {
		Move(data, MW + address, length * 2);
		res = IdModBusClient1->WriteRegisters(address, data, length);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::IdModBusClient1Disconnected(TObject *Sender)
{
	PLCErr = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::IdModBusClient1Connected(TObject *Sender)
{
	PLCErr = false;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::IdModBusClient1ResponseError(const BYTE FunctionCode, const BYTE ErrorCode,
		  const TModBusResponseBuffer &ResponseBuffer)
{
	PLCErr = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerAllarmiTimer(TObject *Sender)
{
	bool allarmi[256];
	int i, j, nall;

	TimerAllarmi->Enabled = false;
	try {
		// dati letti con successo: li elaboro
		for (j = 0; j < 7; j++) {
			for (i = 0; i < 16; i++) {
				nall = j * 16 + i + 1;
				// Aggiorno allarmi
				allarmi[nall] = ModBusDM->GetBit(800 + j, i);
				if (allarmi[nall] && !allbak[nall]) {
					// Nuovo allarme
					if (j < 6) {
						DBDataModule->Segnalazione(nall, alltxt[nall], true);
					}
				} else if (!allarmi[nall] && allbak[nall]) {
					// Allarme resettato
					DBDataModule->AcquisisciSegnalazioneAttiva(nall);
				}
				allbak[nall] = allarmi[nall];
			}
		}
	} __finally {
		TimerAllarmi->Enabled = true;
	}
}
//---------------------------------------------------------------------------
void clean(char *s) {
	char *p;
	int fld = 0;

	if (strlen(s) <= 0) {
		return;
	}
	p = s + strlen(s) - 1;
	while ((*p == 10) || (*p == 13)) {
		*p = 0;
		p = s + strlen(s);
	}
}
//---------------------------------------------------------------------------

void parse(char *record, char *delim, char arr[][100], int *fldcnt) {
	char *p = strtok(record, delim);
	int fld = 0;

	while (p) {
		strcpy(arr[fld], p);
		clean(arr[fld]);
		fld++;
		p = strtok('\0', delim);
	}
	*fldcnt = fld;
}
//---------------------------------------------------------------------------

void TModBusDM::LeggiTestiAllarmi(AnsiString FileName) {
	char tmp[1024];
	int n, fldcnt=0;
	char arr[5][100];
	FILE *in;

	tmp[0]=0x0;
	arr[0][0]=0x0;
	arr[1][0]=0x0;
	arr[2][0]=0x0;
	arr[3][0]=0x0;
	arr[4][0]=0x0;
	in = fopen(FileName.c_str(), "r");
	if (in != NULL) {
		while (fgets(tmp, sizeof(tmp), in) != 0) /* read a record */ {
			int i = 0;
			parse(tmp, ";", arr, &fldcnt); /* whack record into fields */
			n = StrToInt(arr[0]);
			alltxt[n].sprintf("(MX%s.%s) %s", arr[1], arr[2], arr[3]);
		}
		fclose(in);
	}
}
//---------------------------------------------------------------------------

int TModBusDM::NextActiveAlarm(int actall) {
	int i, j, nall;

	i = actall + 1;
	if (i > 112)
		i = 0;
	while (i != actall) {
		if ((i > 0) && allbak[i]) {
			return i;
		}
		i++;
		if (i > 112)
			i = 0;
	}
	if ((actall > 0) && allbak[actall])
		return actall;
	return 0;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerPrgTimer(TObject *Sender)
{
	int CambioPalletNEW;

	TimerPrg->Enabled = false;
	CambioPalletNEW = ModBusDM->GetBit(600, 2);
	if (!CambioPallet && CambioPalletNEW && ModBusDM->GetBit(600, 3)) {
		PalletDaMettereInMacchina = ModBusDM->MW[601];
		// Evento cambio pallet
		LogEvent("P");
		if (PalletDaMettereInMacchina && !DBDataModule->LeggiEsclusioneProgramma()) {
			// Verifico se pallet precedente terminato per mancanza utensili
//			if (LSV2DM->Values["\\PLC\\memory\\D\\280"] == 150000) {
//				// Imposto stato pallet parzialmente lavorato
//				PalletToltoDaMacchina = DBDataModule->LeggiPalletInMacchina();
//				if (PalletToltoDaMacchina > 0)
//					DBDataModule->ImpostaStatoPallet(PalletToltoDaMacchina, 1);
//			}
			// Resetto il flag InMacchina per tutti i pallet
			DBDataModule->NessunPalletInMacchina();
			// Imposto prox pallet in macchina
			DBDataModule->ImpostaPalletInMacchina(PalletDaMettereInMacchina, 1);
			// Memorizzo il programma del prossimo pallet
			ProgramName = DBDataModule->LeggiProgrammaPallet(PalletDaMettereInMacchina);
//			LSV2DM->EnableHostFunction();
//			LSV2DM->ProgramName = "";
//			LSV2DM->ProgramActive = false;

			// Imposto stato pallet ok
			DBDataModule->ImpostaStatoPallet(PalletDaMettereInMacchina, 0);
			// Nessun controllo utensili, avvio programma
			//TimerRunProgram->Enabled = true;
/*
			// Cancello file precedenti
			DeleteFile("TOOL.T");
			DeleteFile("PRG.HDR");
			// Scarico la tabella utensili
			LSV2DM->Login("FILE");
			LSV2DM->SysCommand(6);     // LSV2_SET_BUF3072
			LSV2DM->DownloadFile(LSV2DM->ToolsFile.c_str(), "TOOL.T");
			// Scarico intestazione programma per lista utensili da verificare
			LSV2DM->DownloadFile(ProgramName.c_str(), "PRG.HDR", ";fine scarto");
			// Avvio controllo utensili
			tChekToolsTimer(Sender);
*/
			WriteWorkProgram(ProgramName);
			ModBusDM->ResetBit(600, 2);

			timerscanprogram->Enabled = true;
		} else {
			ModBusDM->ResetBit(600, 2);
			PalletDaMettereInMacchina = 0;
		}
	}
	CambioPallet = (CambioPalletNEW && ModBusDM->GetBit(600, 3));
	TimerPrg->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerRunProgramTimer(TObject *Sender)
{
	bool bStartCycle,bPrgOnCycle;
	TimerRunProgram->Enabled = false;

	if (!ProgramName.IsEmpty()) {
		bStartCycle = GetBit(TAGW_AREA_DA_MAZAK, BIT_CYCLE_START_ENABLED_MU);
		bPrgOnCycle = GetBit(TAGW_AREA_DA_MAZAK, BIT_MACHINE_RUNNING_MU);
		//Controllo se ho il consenso allo start ciclo e non ci sono programmi in esecuzione
		if ((bStartCycle) && !(bPrgOnCycle)) {
		   //	if (CheckWorkProgram()) {
				//Alzo il bit di start programma
				ResetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU);
//				ModBusDM->ResetBit(600, 2);
//			}
		} else {
			Tag++;
			if (Tag > 10) {
			//Log
			//DBDataModule->Log("SERVER", "EVENT", "MU%d: TimeOut Evento Cycle Start. Set NC Reset", MU);
			//Cancello l'evento
			//EraseEvent(i--);
			//Log
			//DBDataModule->Log("SERVER", "EVENT", "MU%d: Cancello Evento Cycle Start", MU);    }
				if (GetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU)) {
					//resetto il bit
					ResetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU);
					Tag = 1;
				}
			} else {
				TimerRunProgram->Enabled = true; //riprovo
			}
		}
	}
}
//---------------------------------------------------------------------------

bool TModBusDM::CaricaTabellaUtensili(AnsiString FileName)
{
	FILE *f;
	char s[256];
	int i, r;
	bool res = false;

	f = fopen(FileName.c_str(), "r");
	if (f) {
		while (!feof(f)) {
			if (fgets(s, 256, f)) {
				r = sscanf(s, "%d", &i);
				if ((r > 0) && (i >0) && (i < 500)) {
					TOOLS_TAB[i].curtime = atoi(s + 128);
					TOOLS_TAB[i].time1 = atoi(s + 116);
					TOOLS_TAB[i].time2 = atoi(s + 122);
					TOOLS_TAB[i].rotto = ((s[107] != ' ') ? 1 : 0);
					TOOLS_TAB[i].gemello = atoi(s + 110);
				}
			}
		}
		fclose(f);
		res = true;
	}
	return res;
}
//---------------------------------------------------------------------------
bool TModBusDM::UtensileDisponibile(int n, int primo) {
	if (n >= 500) {
        return false;
	}
	if (((TOOLS_TAB[n].time1 > 0) && (TOOLS_TAB[n].curtime >= TOOLS_TAB[n].time1)) ||
		((TOOLS_TAB[n].time2 > 0) && (TOOLS_TAB[n].curtime >= TOOLS_TAB[n].time2)) ||
		(TOOLS_TAB[n].rotto)) {
		if ((TOOLS_TAB[n].gemello <= 0) || (TOOLS_TAB[n].gemello == primo)) {
			return false;
		} else {
			return UtensileDisponibile(TOOLS_TAB[n].gemello, primo);
		}
	} else {
		return true;
    }
}
//---------------------------------------------------------------------------
int TModBusDM::TuttiUtensiliDisponibili(AnsiString FileName)
{
	FILE *f;
	char s[256], newval[256];
	int val;
	AnsiString vals;
	int res = 0;

	f = fopen(FileName.c_str(), "r");
	if (f) {
		while (!feof(f) && (fgets(s, 256, f))) {
			vals = Parser(" ;t", s, "\n");
			val = vals.ToIntDef(0);
			if (val > 0) {
				if (!UtensileDisponibile(val, val)) {
					fclose(f);
					return 1;
				}
			}
		}
		fclose(f);
		res = 2;
	}
	return res;
}
//---------------------------------------------------------------------------
int TModBusDM::UtensiliDisponibili(AnsiString FileName)
{
	FILE *f;
	char s[256], newval[256];
	int val;
	AnsiString vals;
	int res = 0;

	f = fopen(FileName.c_str(), "r");
	if (f) {
		while (!feof(f) && (fgets(s, 256, f))) {
			vals = Parser(" ;t", s, "\n");
			val = vals.ToIntDef(0);
			if (val > 0) {
				if (UtensileDisponibile(val, val)) {
					fclose(f);
					return 2;
				}
			}
		}
		fclose(f);
		res = 1;
	}
	return res;
}
//---------------------------------------------------------------------------
int TModBusDM::CaricaDatiCommessa(AnsiString FileName, AnsiString &ordine, AnsiString &articolo, AnsiString &fase, AnsiString &disegno)
{
	FILE *f;
	char s[256], newval[256];
	AnsiString vals;
	int res = 0;

	f = fopen(FileName.c_str(), "r");
	if (f) {
		while (!feof(f) && (fgets(s, 256, f))) {
			vals = Parser("COMMESSA=", s, "\n");
			if (vals != "") {
				ordine = vals;
			}
			vals = Parser("MATRICE=", s, "\n");
			if (vals != "") {
				articolo = vals;
			}
			vals = Parser("FILE ISO=", s, "\n");
			if (vals != "") {
				fase = vals;
			}
			vals = Parser("CAMISTA=", s, "\n");
			if (vals != "") {
				disegno = vals;
			}
		}
		fclose(f);
		res = 1;
	}
	return res;
}
//---------------------------------------------------------------------------
AnsiString TModBusDM::Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore)
{
	int inizio, fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		fine = tg.Pos(Separatore);
		if (fine <= 0) {
			fine = tg.Length();
		}
		if (fine > 0) {
			res = tg.SubString(1, fine - 1);
		}
	}
	return res;
}
//---------------------------------------------------------------------------
void TModBusDM::LogNuovaCommessa(int n) {
	int tipofile, npallet, nprogramma;
	AnsiString nomefile, ext;

	tipofile = n / 100000;
	npallet = (n / 10000) % 10;
	nprogramma = n % 10000;
	ext = ((tipofile == 1) ? ".h" : ".i");
	nomefile = "TNC:\\LAVORO\\PAL" + IntToStr(npallet) + IntToStr(nprogramma) + ext;
//	LSV2DM->Login("FILE");
//	// Scarico intestazione programma per log commessa
//	LSV2DM->DownloadFile(nomefile.c_str(), "SUBPRG.HDR", ";FINE INTESTAZIONE");
//	tLogCommessa->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TModBusDM::tLogCommessaTimer(TObject *Sender)
{
	int res;
	AnsiString ordine, articolo, fase, disegno;

//	tChekTools->Enabled = false;
	CaricaDatiCommessa("SUBPRG.HDR", ordine, articolo, fase, disegno);
	LogEvent("P", ordine, articolo, fase, disegno);
	DeleteFile("SUBPRG.HDR");
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::timerscanprogramTimer(TObject *Sender)
{
	timerscanprogram->Enabled = false;
	if (GetBit(TAGW_AREA_DA_MAZAK, BIT_CYCLE_START_ENABLED_MU)){
		SetBit(TAGW_AREA_VERSO_MAZAK, BIT_WORK_NUMBER_SEARCH_START_MU);
	} else {//riprovo
		timerscanprogram->Enabled = true;
	}
	//Log
	//DBDataModule->Log("SERVER", "EVENT", "MU%d: Alzo la ricerca numero programma", MU);
	//TimerFineScansione->Enabled = true; //attivo la verifica di fine scansione
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerFineScansioneTimer(TObject *Sender)
{
	bool bstartenabled, bSearcComplete, bPrgOnCycle, bsearchstarted, battesaok;


	TimerFineScansione->Enabled = false;
	bsearchstarted  = GetBit(TAGW_AREA_VERSO_MAZAK, BIT_WORK_NUMBER_SEARCH_START_MU);
	bstartenabled 	= GetBit(TAGW_AREA_DA_MAZAK, BIT_CYCLE_START_ENABLED_MU);
	bSearcComplete	= GetBit(TAGW_AREA_DA_MAZAK,BIT_WORK_NUMBER_SEARCH_FINISHED);
	bPrgOnCycle 	= GetBit(TAGW_AREA_DA_MAZAK, BIT_MACHINE_RUNNING_MU);
	battesaok		= (Tag>0);

	if ((bsearchstarted)&&(bSearcComplete)&&(!TimerRunProgram->Enabled)&&(!bPrgOnCycle)&&(battesaok)) {
		//Resetto il bit di inizio ricerca programma
		ResetBit(TAGW_AREA_VERSO_MAZAK, BIT_WORK_NUMBER_SEARCH_START_MU);
		SetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU);
		TimerRunProgram->Tag = 0;
		TimerRunProgram->Enabled = true;
		Tag = 0;
		//Log
		//DBDataModule->Log("SERVER", "EVENT", "MU%d: Reset Bit Work Number Search Complete", MU);
	} else {
		if(	(bsearchstarted)&&(bSearcComplete)&&(!TimerRunProgram->Enabled)&&(!bPrgOnCycle)&&(!battesaok)){
			Tag++;;
		}
		//TimerRunProgram->Enabled = true;
		//Creo evento di start solo nel caso in cui ho selezionato un programma
		//NewEvent(EV_CYCLE_START_PERMISSION_MU(MU), param);
		//Log
		//DBDataModule->Log("SERVER", "EVENT", "MU%d: Creo Evento di Cycle Start Permission Signal", MU);
		//Cancello l'evento
	}
	TimerFineScansione->Enabled = true;
}
//---------------------------------------------------------------------------
bool TModBusDM::CheckWorkProgram(){
	bool res;
	AnsiString WorkNum_W, WorkNum_R;
	WorkNum_W = GetString(TAGW_WORK_NUMBER_MU,32);
	WorkNum_R = GetString(TAGW_CURRENT_WORK_NUMBER_MU,32);
	res =(WorkNum_W == WorkNum_R);
	return res;
}

bool TModBusDM::WriteWorkProgram(AnsiString WorkProgram)
{
	bool res;
	WORD *data;
	WORD prova;
	int i, len, len1;
	AnsiString WorkProgramReversed;

	len = 32;//m_Taglen[TAGW_WORK_NUMBER_MU(MU)];
	data = new WORD[len];
	memset(data, 0, sizeof(WORD)*len);
	len1 = WorkProgram.Length();
	WorkProgramReversed=L"";

	for (i = len1 ; i>0 ; i--) {
		WorkProgramReversed = WorkProgramReversed + WorkProgram[i];
	}
	for (i = 0;(i*2)+1 <= WorkProgramReversed.Length() && i < len; i++){
		if(((i*2)+1) < WorkProgramReversed.Length()){//ho due caratteri

			//prova = WorkProgramReversed[(i*2)+1] << 8 && WorkProgramReversed[(i*2) +2];
			prova = MAKEWORD(WorkProgramReversed[(i*2)+1],WorkProgramReversed[(i*2) +2]);
			data[i]=prova;
		} else {//ho 1 carattere
			//prova = WorkProgramReversed[(i*2)+1] << 8 ;
			prova = MAKEWORD(WorkProgramReversed[(i*2)+1],0);
			data[i]=prova;
		}
	}
	res = WriteBlock(TAGW_WORK_NUMBER_MU, data, len);
	delete [] data;

	return res;
}
//---------------------------------------------------------------------------

