//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Password.h"
#include "Main.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
TPasswordDlg *PasswordDlg;
//---------------------------------------------------------------------
__fastcall TPasswordDlg::TPasswordDlg(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------

void __fastcall TPasswordDlg::FormActivate(TObject *Sender)
{
	Password->SetFocus();	
}
//---------------------------------------------------------------------------


void __fastcall TPasswordDlg::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

