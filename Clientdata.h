//---------------------------------------------------------------------------
#ifndef ClientDataH
#define ClientDataH
//---------------------------------------------------------------------------

#define N_TORNI			43
#define N_CASSONI		45
#define N_POSTAZIONI	51
#define N_PALLET		5

typedef struct {
	int lettore_tornio_mem[N_TORNI];
	int lettore_lavatrice_mem[2];
	int dest_cassetta_mem[2];
	int pos_cassetta_mem[2];
} TDatiMEM;

typedef struct {
	int destinazione_cassetta_lavatrice[2];
	int posizione_cassetta_lavatrice[2];   // 1=A, 2=B, se Dest 51 scarto 1=scarto, 2=non letta
	int postazione_piena_A[N_POSTAZIONI];
	int postazione_piena_B[N_POSTAZIONI];
	int cassette_su_pallet[5];
	int manovra_eseguita[2];
} TDatiPLC1;

typedef struct {
	// PLC 2 ASI
	int lettore_tornio[N_TORNI];
	int lettore_uscita_mag;
	int lettore_lavatrice[2];
	int lettore_ribaltatore[2];
	int lettore_stazione[2];
	int ok_lettura_tornio[43];
	int entrata_in_corso_tornio[N_TORNI];
	int conferma_entrata_tornio[N_TORNI];
	int valore_scrittura_stazione[2];
	int start_scrittura_stazione[2];
	int cassetta_uscita_mag_per_lavatrice[2];
	int presenza_cassetta_uscita_mag;
} TDatiPLC2;

typedef struct {
	// PLC 3-4 Lavatrice 1-2
	int lavaggio_ingresso_lavatrice;
	int avanzamento_prelievo_ribaltatore;
	int presenza_cassetta_entrata;
	int presenza_cassetta_ribaltatore;
	int numero_cassette_postazione[5];
	int lavaggio_postazione[5];
} TDatiPLC34;

typedef struct {
} TDatiPLC4;

typedef struct {
	// Inizio pacchetto per sincronismo trasmissione dati
	char Sync[5];
	// Generali
	int watchdog;
	bool PLC_Connected[4];
	bool TAB_Connected;
	// PLC
	TDatiPLC1 PLC1;
	TDatiPLC2 PLC2;
	TDatiPLC34 PLC3;
	TDatiPLC34 PLC4;
	// PC
	TDatiMEM MEM;
} TClientData;

extern TClientData ClientData;

#endif
