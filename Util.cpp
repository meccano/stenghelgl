//---------------------------------------------------------------------------

#pragma hdrstop

#include "Util.h"
#include <time.h>
#include <stdio.h>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)

void TGetFiles::GetFileList(TStringDynArray &fileslist) {
	fileslist = files;
}
//---------------------------------------------------------------------------

__fastcall TGetFiles::TGetFiles(bool CreateSuspended, AnsiString fn)
	: TThread(CreateSuspended)
{
	FolderName = fn;
}
//---------------------------------------------------------------------------
void __fastcall TGetFiles::Execute()
{
	time_t t;

	NameThreadForDebugging((AnsiString)("GetFiles"), ThreadID);
	//---- Place thread code here ----
	t = clock();
	while (!Terminated) {
		try {
			if ((clock() - t) > 20000) {
				if (DirectoryExists(FolderName))
					files = TDirectory::GetFiles(FolderName, "*.H", TSearchOption::soAllDirectories);
				t = clock();
            }
		} catch (...) {};
		Sleep(1000);
	}
	Suspend();
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, int NumCaratteri)
{
	int inizio, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		res = tg.SubString(1, NumCaratteri);
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma)
{
	int i, inizio, fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		for(i = 1; i < tg.Length(); i++) {
			if (!isdigit((wchar_t)(tg[i]))) {
				fine = i - 1;
				break;
			}
		}
		if (fine <= 0) {
			fine = tg.Length() - 1;
		}
		if (fine > 0) {
			res = tg.SubString(1, fine);
		}
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore)
{
	int inizio, fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		fine = tg.Pos(Separatore);
		if (fine <= 0) {
			fine = tg.Length();
		}
		if (fine > 0)
			res = tg.SubString(1, fine - 1);
	}
	return res;
}
//---------------------------------------------------------------------------

int NumeroProgramma(AnsiString filename) {
	FILE *f;
	char s[256];
	AnsiString field;
	int res, val;

	res = -2;
	val = 0;
	try {
		f = fopen(filename.c_str(), "r");
		if (f) {
			while (fgets(s, 256, f) && !val) {
				field = Parser("O", s);
				if (field.IsEmpty()) {
					field = Parser(":", s);
				}
				val = atoi(field.c_str());
			}
			fclose(f);
			res = val;
		}
	} catch (...) {
		res = -1;
	}
	return res;
}
//---------------------------------------------------------------------------

char DecimalToBcd(char decimal) {
    return (char) ((decimal / 10)*16)+(decimal % 10);
}
//---------------------------------------------------------------------------

double ltod(long value)
{
	return ((double)value)/1000.0;
}
//---------------------------------------------------------------------------

long dtol(double value)
{
	double tmp = value*1000.0;
	return (long)tmp;
}
//---------------------------------------------------------------------------

