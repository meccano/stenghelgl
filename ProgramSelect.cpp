//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ProgramSelect.h"
//#include "LSV2.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TProgramSelectDlg *ProgramSelectDlg;
//---------------------------------------------------------------------------
__fastcall TProgramSelectDlg::TProgramSelectDlg(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDlg::FormActivate(TObject *Sender)
{
//	LSV2DM->Login("FILE");
//	LSV2DM->Dir();
//	LSV2DM->DriveInfo();
//	LSV2DM->Logout("FILE");
	TimerUpd->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDlg::FormDeactivate(TObject *Sender)
{
	TimerUpd->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDlg::TimerUpdTimer(TObject *Sender)
{
//	LEFolder->Text = LSV2DM->ActiveDir;
//	if (LSV2DM->Updated) {
//		LBFolders->Items = LSV2DM->Dirs;
//		LBFiles->Items = LSV2DM->Files;
//		LSV2DM->Updated = false;
//	}
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDlg::LBFoldersClick(TObject *Sender)
{
	AnsiString path;
	int n;

	n = LBFolders->ItemIndex;
	if (n < 0) return;
	path = LBFolders->Items->Strings[n];
//	LSV2DM->ChDir(path.c_str());
//	LSV2DM->Dir();
//	LSV2DM->DriveInfo();
//	LSV2DM->Logout("FILE");
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDlg::LBFilesClick(TObject *Sender)
{
	int n;

	n = LBFiles->ItemIndex;
	if (n < 0) return;
//	LEFile->Text = LSV2DM->ActiveDir + LBFiles->Items->Strings[n];
}
//---------------------------------------------------------------------------


void __fastcall TProgramSelectDlg::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}

//---------------------------------------------------------------------------

