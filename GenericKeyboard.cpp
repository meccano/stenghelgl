//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GenericKeyboard.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfGenericKeyboard *fGenericKeyboard;
//---------------------------------------------------------------------------
__fastcall TfGenericKeyboard::TfGenericKeyboard(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfGenericKeyboard::FormShow(TObject *Sender)
{
	Edit1->SetFocus();
}
//---------------------------------------------------------------------------

