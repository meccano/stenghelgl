//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Sinottico.h"
#include "ModBus.h"
#include "InsNum.h"
#include "conferma.h"
#include "DB.h"
#include "ProgramSelect.h"
#include "ProgramSelectDB.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LayMU"
#pragma resource "*.dfm"
TSinotticoForm *SinotticoForm;
//---------------------------------------------------------------------------
__fastcall TSinotticoForm::TSinotticoForm(TComponent* Owner)
	: TForm(Owner)
{
	int i;

	for(i = 0; i < MainForm->NPallet; i++) {
        frmLayMU1->P[i]->OnMouseDown = &MyShape1MouseDown;
    }
}
//---------------------------------------------------------------------------

void TSinotticoForm::CaricaComboBox(TComboBox *cb, int n)
{
	int i;

	cb->Clear();
	for (i = 1; i <= n ; i++) {
		cb->Items->Add(i);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::FormActivate(TObject *Sender)
{
	WindowState = wsMaximized;
	CaricaComboBox(ComboBox1, MainForm->NPallet);
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	Release();
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::BitBtn3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void TSinotticoForm::ColoraPallino(int stato, TShape *t, TColor ColorON, TColor ColorOFF, TColor ColorALL)
{
	if (stato == 1) {
		t->Brush->Color = ColorON;
	} else if (stato >= 2) {
		t->Brush->Color = ColorALL;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------

void TSinotticoForm::ColoraPallet(TMyShape *i, bool presenza, bool disponibile, bool lavorato, bool missione, bool stato)
{
	i->OnlyText = !presenza;
	if (missione) {
		i->Brush->Color = clBlue;
	} else if (disponibile) {
		i->Brush->Color = clGreen;
	} else if (lavorato) {
		if (stato == 1) {
			i->Brush->Color = clPurple;
		} else if (stato == 2) {
			i->Brush->Color = clMaroon;
		} else {
			i->Brush->Color = clRed;
		}
	} else {
		i->Brush->Color = clGray;
	}
}
//---------------------------------------------------------------------------

void TSinotticoForm::Aggiorna() {
	int pos, posm, poso, i, j, n, mw, bit;
	bool *presenza, *disponibile, *lavorato, *missione, *stato;
	TfrmLayMU* fm;
	TIndexList TabPallet;

	presenza = new bool[MainForm->NPallet];
	disponibile = new bool[MainForm->NPallet];
	lavorato = new bool[MainForm->NPallet];
	missione = new bool[MainForm->NPallet];
	stato = new bool[MainForm->NPallet];
	posm = ModBusDM->MW[855]; // MU
	poso = ModBusDM->MW[856]; // OP
	fm = frmLayMU1;
	DBDataModule->CaricaTabellaK("Pallet", "Pallet", TabPallet);
	if (posm) {
		pos = posm;
		fm->Repos(MainForm->PosMacchina);
	} else {
		pos = poso;
		fm->Repos(2);
	}
	n = (pos > 1) ? pos : 1;
	for(i = 0; i < MainForm->NPallet; i++) {
		fm->P[i]->Text = n;
		stato[i] = StrToIntDef(TabPallet[n]["Stato"], 0);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	n = (pos > 1) ? pos : 1;
	for(j = 0; j < MainForm->NPallet; j++) {
		mw = 891 + (n - 1) / 4;
		bit = (n - 1) % 4 * 4;
		presenza[j]    = ModBusDM->GetBit(mw, bit);
		disponibile[j] = ModBusDM->GetBit(mw, bit + 1);
		lavorato[j]    = ModBusDM->GetBit(mw, bit + 2);
		missione[j]    = ModBusDM->GetBit(mw, bit + 3);
		n = (n > MainForm->NPallet - 1) ? 1 : n + 1;
	}
	for(i = 0; i < MainForm->NPallet; i++) {
		ColoraPallet(fm->P[i], presenza[i], disponibile[i], lavorato[i], missione[i], stato[i]);
	}
	delete []presenza;
	delete []disponibile;
	delete []lavorato;
	delete []missione;
	delete []stato;
	if (!ComboBox1->DroppedDown)
		ComboBox1->ItemIndex = ModBusDM->MW[506] - 1;
	if (!TimerOverride->Enabled)
		LabeledEdit1->Text = ModBusDM->MW[30];
	LabeledEdit2->Text = ModBusDM->MW[507];
	CheckBox1->Checked = (ModBusDM->GetBit(508, 0));
	CheckBox2->Checked = (ModBusDM->GetBit(508, 1));
	CheckBox3->Checked = (ModBusDM->GetBit(508, 2));
	ColoraPallino(ModBusDM->GetBit(882, 0), Shape4);
	ColoraPallino(ModBusDM->GetBit(882, 1), Shape5);
	ColoraPallino(ModBusDM->GetBit(882, 2), Shape6);
	ColoraPallino(ModBusDM->GetBit(882, 3), Shape7);

	ColoraPallino(ModBusDM->GetBit(TAGW_AREA_VERSO_MAZAK, BIT_WORK_NUMBER_SEARCH_START_MU), Shape10);
	ColoraPallino(ModBusDM->GetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU), Shape11);
	ColoraPallino(ModBusDM->GetBit(TAGW_AREA_DA_MAZAK, BIT_WORK_NUMBER_SEARCH_FINISHED), Shape12);
	ColoraPallino(ModBusDM->GetBit(TAGW_AREA_DA_MAZAK, BIT_CYCLE_START_ENABLED_MU), Shape13);





	CheckBox4->Checked = DBDataModule->LeggiEsclusioneProgramma();
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::TimerUpdTimer(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::ComboBox1Change(TObject *Sender)
{
	ModBusDM->SetWord(506, StrToFloat(ComboBox1->Text));
}
//---------------------------------------------------------------------------


void __fastcall TSinotticoForm::BitBtn1Click(TObject *Sender)
{
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::MyShape1MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	ModBusDM->SetWord(506, StrToFloat(((TMyShape*)Sender)->Text));
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::LabeledEdit2Click(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
		ModBusDM->SetWord(le->Tag, StrToFloat(le->Text));
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::CheckBox1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TCheckBox *cb;

	cb = (TCheckBox*)Sender;
	if (cb->Checked) {
		ModBusDM->SetBit(508, cb->Tag);
	} else {
		ModBusDM->ResetBit(508, cb->Tag);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::BitBtn2Click(TObject *Sender)
{
	if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente sovrascrivere i dati pallet?")) == mrOk) {
       	ModBusDM->SetBit(509, 1);
	}
	ModBusDM->SetBit(509, 0);
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::SpinButton1DownClick(TObject *Sender)
{
	int vo;

	vo = StrToIntDef(LabeledEdit1->Text, 0);
	if (vo > 0) {
		vo = vo - 1;
		LabeledEdit1->Text = vo;
		TimerOverride->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::SpinButton1UpClick(TObject *Sender)
{
	int vo;

	vo = StrToIntDef(LabeledEdit1->Text, 0);
	if (vo < 100) {
		vo = vo + 1;
		LabeledEdit1->Text = vo;
		TimerOverride->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::TimerOverrideTimer(TObject *Sender)
{
	ModBusDM->SetWord(30, StrToFloat(LabeledEdit1->Text));
	TimerOverride->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::CheckBox4MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	if (CheckBox4->Checked) {
		DBDataModule->MemorizzaEsclusioneProgramma(1);
	} else {
		DBDataModule->MemorizzaEsclusioneProgramma(0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::LabeledEdit2Change(TObject *Sender)
{
	AnsiString prg;

	prg = DBDataModule->LeggiProgrammaPallet(ModBusDM->MW[507]);
	mProgramMU->Text = prg;
}
//---------------------------------------------------------------------------


void __fastcall TSinotticoForm::mProgramMUClick(TObject *Sender)
{
	ProgramSelectDBDlg->CaricaProgrammi();
	if (ProgramSelectDBDlg->ShowModal() == IDOK) {
		//mProgramMU->Text = ProgramSelectDlg->LEFile->Text;
		mProgramMU->Text = ProgramSelectDBDlg->LEProgrammaSelezionato->Text;
		DBDataModule->MemorizzaProgrammaPallet(ModBusDM->MW[507], mProgramMU->Text);
	}

}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

void __fastcall TSinotticoForm::Shape11MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	if (ModBusDM->GetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU)) {
		ModBusDM->ResetBit(TAGW_AREA_VERSO_MAZAK, BIT_CYCLE_START_COMMAND_MU);
	}
}
//---------------------------------------------------------------------------

