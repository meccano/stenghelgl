//---------------------------------------------------------------------------


#pragma hdrstop

#include "Modbus.h"
#include "DB.h"
#include "ping.h"
#include "LSV2.h"
#include <inifiles.hpp>
#include <math.hpp>
#include <stdio.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TModBusDM *ModBusDM;

//---------------------------------------------------------------------------
__fastcall TModBusDM::TModBusDM(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::DataModuleCreate(TObject *Sender)
{
	AnsiString PLCAddress;
	TIniFile *Ini;
	int i;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	PLCAddress = Ini->ReadString("PLC", "PLCAddress", "127.0.0.1");
	if (PLCAddress != "")
		IdModBusClient1->Host = PLCAddress;
	else
		IdModBusClient1->Host = "127.0.0.1";
	delete Ini;
	for (i = 0 ; i < 256; i++) {
		allbak[i] = false;
		alltxt[i] = "";
	}
	LeggiTestiAllarmi();
	step = 0;
	try {
		IdModBusClient1->Connect();
		PLCErr = false;
	} catch (...) {
		PLCErr = true;
	}
	PalletBak = 0;
	ProgramName = "";
	CambioPallet = 0;
	TimerPLCRead->Enabled = true;
	TimerAllarmi->Enabled = true;
	TimerPrg->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerPLCReadTimer(TObject *Sender)
{
	int address, nword;

	TimerPLCRead->Enabled = false;
	if (PLCErr) {
		try {
			if (Ping(IdModBusClient1->Host)) {
				IdModBusClient1->Connect();
			}
		} catch (...) {}
		TimerPLCRead->Enabled = true;
		return;
	}
	switch(step) {
		case 0:
			address = 1;
			nword = 110;
			break;
		case 1:
			address = 200;
			nword = 125;
			break;
		case 2:
			address = 325;
			nword = 125;
			break;
		case 3:
			address = 500;
			nword = 110;
			break;
		case 4:
			address = 800;
			nword = 100;
			break;
		case 5:
			address = 900;
			nword = 100;
			break;
		case 6:
			address = 1000;
			nword = 80;
			break;
	}
	try {
		if (IdModBusClient1->ReadHoldingRegisters(address, nword, MW + address, nword * 2)) {//address, nword, MW + address, nword * 2)) {
			(step < 6) ? step++: step = 0;
		} else {
			IdModBusClient1->Disconnect();
			PLCErr = true;
		}
	} catch(...) {
			IdModBusClient1->Disconnect();
			PLCErr = true;
	}
	TimerPLCRead->Enabled = true;
}
//---------------------------------------------------------------------------

bool TModBusDM::GetBit(int mx, int bit)
{
	bool res;

	res = ((MW[mx] & (1 << bit)) != 0);
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetBit(int mx, int bit)
{
	bool res = false;
	WORD val;

	try {
		val = (MW[mx] | (1 << bit));
		MW[mx] = val;
		res = IdModBusClient1->WriteRegister(mx, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::ResetBit(int mx, int bit)
{
	bool res = false;
	WORD val;

	try {
		val = (MW[mx] & ~(1 << bit));
		MW[mx] = val;
		res = IdModBusClient1->WriteRegister(mx, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

long int TModBusDM::GetDWord(int md)
{
	long int res;

	res = (MW[md*2 + 1] << 16) + MW[md*2];
	return res;
}
//---------------------------------------------------------------------------

AnsiString TModBusDM::GetString(int address, int length)
{
	int i;
	AnsiString res = "";

	for (i = 0; i < (length/2); i++) {
		res = res + (char)LOBYTE(MW[address + i]) + (char)HIBYTE(MW[address + i]);
	}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetDWord(int md, float value, int decimal)
{
	bool res = false;
	DWORD val;

	try {
		val = RoundTo(value * pow10(decimal), 0);
		MW[md*2 + 1] = HIWORD(val);
		MW[md*2] = LOWORD(val);
		val =  (MW[md*2] << 16) + MW[md*2 + 1];
		res = IdModBusClient1->WriteDWord(md * 2, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::SetWord(int address, float value, int decimal)
{
	bool res = false;
	int val;

	try {
		val = RoundTo(value * pow10(decimal), 0);
		MW[address] = val;
		res = IdModBusClient1->WriteRegister(address, val);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------

bool TModBusDM::WriteBlock(int address, WORD *data, int length)
{
	bool res = false;

	try {
		Move(data, MW + address, length * 2);
		res = IdModBusClient1->WriteRegisters(address, data, length);
	} catch (...) {}
	return res;
}
//---------------------------------------------------------------------------


void __fastcall TModBusDM::IdModBusClient1Disconnected(TObject *Sender)
{
	PLCErr = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::IdModBusClient1Connected(TObject *Sender)
{
	PLCErr = false;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::IdModBusClient1ResponseError(const BYTE FunctionCode, const BYTE ErrorCode,
		  const TModBusResponseBuffer &ResponseBuffer)
{
	PLCErr = true;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerAllarmiTimer(TObject *Sender)
{
	bool allarmi[256];
	int i, j, nall;

	TimerAllarmi->Enabled = false;
	try {
		// dati letti con successo: li elaboro
		for (j = 0; j < 7; j++) {
			for (i = 0; i < 16; i++) {
				nall = j * 16 + i + 1;
				// Aggiorno allarmi
				allarmi[nall] = ModBusDM->GetBit(800 + j, i);
				if (allarmi[nall] && !allbak[nall]) {
					// Nuovo allarme
					if (j < 6) {
						DBDataModule->Segnalazione(nall, alltxt[nall], true);
					}
				} else if (!allarmi[nall] && allbak[nall]) {
					// Allarme resettato
					DBDataModule->AcquisisciSegnalazioneAttiva(nall);
				}
				allbak[nall] = allarmi[nall];
			}
		}
	} __finally {
		TimerAllarmi->Enabled = true;
	}
}
//---------------------------------------------------------------------------
void clean(char *s) {
	char *p;
	int fld = 0;

	if (strlen(s) <= 0) {
    	return;
	}
	p = s + strlen(s) - 1;
	while ((*p == 10) || (*p == 13)) {
		*p = 0;
		p = s + strlen(s);
	}
}
//---------------------------------------------------------------------------

void parse(char *record, char *delim, char arr[][100], int *fldcnt) {
	char *p = strtok(record, delim);
	int fld = 0;

	while (p) {
		strcpy(arr[fld], p);
		clean(arr[fld]);
		fld++;
		p = strtok('\0', delim);
	}
	*fldcnt = fld;
}
//---------------------------------------------------------------------------

void TModBusDM::LeggiTestiAllarmi() {
	char tmp[1024]={0x0};
	int n, fldcnt=0;
	char arr[5][100]={0x0};
	FILE *in=fopen("Allarmi.txt","r");

	if (in != NULL) {
		while (fgets(tmp, sizeof(tmp), in) != 0) /* read a record */ {
			int i = 0;
			parse(tmp, ";", arr, &fldcnt); /* whack record into fields */
			n = StrToInt(arr[0]);
			alltxt[n].sprintf("(MX%s.%s) %s", arr[1], arr[2], arr[3]);
		}
		fclose(in);
	}
}
//---------------------------------------------------------------------------

int TModBusDM::NextActiveAlarm(int actall) {
	int i, j, nall;

	i = actall + 1;
	if (i > 112)
		i = 0;
	while (i != actall) {
		if ((i > 0) && allbak[i]) {
			return i;
		}
		i++;
		if (i > 112)
			i = 0;
	}
	if ((actall > 0) && allbak[actall])
		return actall;
	return 0;
}
//---------------------------------------------------------------------------

void __fastcall TModBusDM::TimerPrgTimer(TObject *Sender)
{
	int Pallet, CambioPalletNEW;

	TimerPrg->Enabled = false;
	CambioPalletNEW = ModBusDM->GetBit(600, 2);
	if (!CambioPallet && CambioPalletNEW && ModBusDM->GetBit(600, 3)) {
		Pallet = ModBusDM->MW[601];
		if (Pallet && !DBDataModule->LeggiEsclusioneProgramma()) {
			ProgramName = DBDataModule->LeggiProgrammaPallet(Pallet);
			LSV2DM->EnableHostFunction();
			LSV2DM->ProgramName = "";
			LSV2DM->ProgramActive = false;
			TimerRunProgramTimer(Sender);
		} else {
			ModBusDM->ResetBit(600, 2);
			Pallet = 0;
		}
	}
	CambioPallet = ( CambioPalletNEW && ModBusDM->GetBit(600, 3));
	TimerPrg->Enabled = true;
}
//---------------------------------------------------------------------------


void __fastcall TModBusDM::TimerRunProgramTimer(TObject *Sender)
{
	TimerRunProgram->Enabled = false;
	if (!LSV2DM->ProgramActive) {
		if (ProgramName != "") {
			LSV2DM->Login("DNC");
			LSV2DM->RunProgram(ProgramName.c_str());
			if (!LSV2DM->ProgramActive) {
				TimerRunProgram->Interval = 3000;
				TimerRunProgram->Enabled = true;
			}
		}
	}
	if (LSV2DM->ProgramActive  || ! ModBusDM->GetBit(600, 2))  {
		ModBusDM->ResetBit(600, 2);
		ModBusDM->SetWord(601, 0);
		TimerRunProgram->Enabled = false;
	}
}
//---------------------------------------------------------------------------

