//---------------------------------------------------------------------------
#ifndef UtilH
#define UtilH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

class TGetFiles : public TThread
{
protected:
	void __fastcall Execute();
	TStringDynArray files;
public:
	__fastcall TGetFiles(bool CreateSuspended, AnsiString fn);
	void GetFileList(TStringDynArray &fileslist);
	AnsiString FolderName;
};
//---------------------------------------------------------------------------

// Ritorna il Massimo tra 2 valori numerici
template <class T> T TMax(T l1, T l2)
{
	if( l1 > l2 )
		return l1;
	else
		return l2;
}
//---------------------------------------------------------------------------

// Ritorna il Minimo tra 2 valori numerici
template <class T> T TMin(T l1, T l2)
{
	if( l1 < l2 )
		return l1;
	else
		return l2;
}
//---------------------------------------------------------------------------

// Scambia i valori di "val1" e "val2"
template <class T> void TSwap(T &val1, T &val2)
{
	T tmp = val1;
	val1 = val2;
	val2 = tmp;
}
//---------------------------------------------------------------------------

// Controllo se un valore � compreso in un Min e Max
template <class T> bool TBetweenEqual(T Value, T Min, T Max)
{
	if (Value >= Min && Value <= Max)
		return true;
	 return false;
}
//---------------------------------------------------------------------------

// Controllo se un valore � compreso in un Min e Max
template <class T> bool TBetweenNotEqual(T Value, T Min, T Max)
{
	if (Value > Min && Value < Max)
		return true;
	 return false;
}
//---------------------------------------------------------------------------

extern AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, int NumCaratteri);
extern AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma);
extern AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore);
extern int NumeroProgramma(AnsiString filename);
extern char DecimalToBcd(char decimal);
extern double ltod(long value);
extern long dtol(double value);

#endif
