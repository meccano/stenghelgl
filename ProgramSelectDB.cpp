//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ProgramSelectDB.h"
#include "gnugettext.hpp"
#include "DB.h"
#include "conferma.h"
#include "GenericKeyboard.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TProgramSelectDBDlg *ProgramSelectDBDlg;
//---------------------------------------------------------------------------

__fastcall TProgramSelectDBDlg::TProgramSelectDBDlg(TComponent* Owner)
	: TForm(Owner)
{
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::TimerUpdTimer(TObject *Sender)
{
	TimerUpd->Enabled = false;

	CaricaProgrammi();

	TimerUpd->Enabled = true;
}

//---------------------------------------------------------------------------

void  TProgramSelectDBDlg::CaricaProgrammi()
{
	int i;
	AnsiString s;
	TRecordList TabProgrammi;
	LBProgrammi->Items->Clear();
	s = "Programmi";
	if (CheckBox1->Checked) {
		s = "Programmi WHERE PROGRAMMA LIKE ('%" + LabeledEdit1->Text + "%')";
	}
	DBDataModule->CaricaTabella(s, TabProgrammi);
	for (i = 0; i < TabProgrammi.size(); i++) {
		if (LBProgrammi->Items->IndexOf(TabProgrammi[i]["Programma"]) < 0) {
			LBProgrammi->Items->Add(TabProgrammi[i]["Programma"]);
		}
	}
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::BitBtn3Click(TObject *Sender)
{
	if (LBProgrammi->Items->IndexOf(LabeledEdit1->Text) < 0) {
		if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente salvare il programma?")) == mrOk) {
			DBDataModule->SalvaProgramma(LabeledEdit1->Text);
		}
	}
	CaricaProgrammi();
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::LBProgrammiClick(TObject *Sender)
{
	if (LBProgrammi->ItemIndex>=0){
		LEProgrammaSelezionato->Text = LBProgrammi->Items->Strings[LBProgrammi->ItemIndex];
	}
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::BitBtn4Click(TObject *Sender)
{
	if (LEProgrammaSelezionato->Text  >= 0) {
		if (ConfermaDlg(_("ATTENZIONE"), _("Vuoi veramente eliminare il programma?")) == mrOk) {
			DBDataModule->EliminaProgramma(LEProgrammaSelezionato->Text );
		}
	}
	CaricaProgrammi();
}

//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::LabeledEdit1Click(TObject *Sender)
{
	fGenericKeyboard->Edit1->Text = LabeledEdit1->Text;
	if (fGenericKeyboard->ShowModal() == mrOk) {
		LabeledEdit1->Text = fGenericKeyboard->Edit1->Text;
	}
	CaricaProgrammi();
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::CheckBox1Click(TObject *Sender)
{
	CaricaProgrammi();
}
//---------------------------------------------------------------------------

void __fastcall TProgramSelectDBDlg::FormShow(TObject *Sender)
{
	LabeledEdit1->SetFocus();
}
//---------------------------------------------------------------------------

