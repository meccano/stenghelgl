object ModBusDM: TModBusDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 198
  Width = 421
  object IdModBusClient1: TIdModBusClient
    OnDisconnected = IdModBusClient1Disconnected
    OnConnected = IdModBusClient1Connected
    ConnectTimeout = 100
    Host = '127.0.0.1'
    IPVersion = Id_IPv4
    ReadTimeout = 100
    UseNagle = False
    AutoConnect = False
    TimeOut = 100
    OnResponseError = IdModBusClient1ResponseError
    Left = 56
    Top = 24
  end
  object TimerPLCRead: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerPLCReadTimer
    Left = 160
    Top = 24
  end
  object TimerAllarmi: TTimer
    Enabled = False
    OnTimer = TimerAllarmiTimer
    Left = 56
    Top = 104
  end
  object TimerPrg: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerPrgTimer
    Left = 240
    Top = 32
  end
  object TimerRunProgram: TTimer
    Enabled = False
    OnTimer = TimerRunProgramTimer
    Left = 328
    Top = 32
  end
  object timerscanprogram: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = timerscanprogramTimer
    Left = 144
    Top = 112
  end
  object TimerFineScansione: TTimer
    Interval = 3000
    OnTimer = TimerFineScansioneTimer
    Left = 304
    Top = 112
  end
end
