//---------------------------------------------------------------------------

#pragma hdrstop

#include <vcl.h>
#include "stdio.h"
#include "io.h"
#include "time.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

FILE *logfile;

int	ApriLog() {
	time_t t;
	struct tm *lt;
	AnsiString filename;
	char timestr[100];

	t = time(NULL);
	lt = localtime(&t);
	if (!DirectoryExists("Log")) {
		CreateDir("Log");
	}
//	strftime(timestr, 100, "Log\\%Y%m%d.csv", lt);
//	filename = timestr;
	filename = "Log\\Eventi.csv";
	if ((logfile = fopen(filename.c_str(), "a+")) == NULL) {
		return 1;
	}
	return 0;
}

void ChiudiLog()
{
	fflush(logfile);
	fclose(logfile);
}

void LogEvent(AnsiString TipoEvento, AnsiString ordine, AnsiString articolo, AnsiString fase, AnsiString disegno)
{
	time_t t;
	struct tm *lt;
	char timestr[100];

	try {
		if (ApriLog() == 0) {
			t = time(NULL);
			lt = localtime(&t);
			strftime(timestr, 100, "%Y/%m/%d %H:%M:%S", lt);
			fprintf(logfile, "%s\t%s\t%s\t%s\t%s\t%s\n", timestr, TipoEvento.c_str(), ordine.c_str(), articolo.c_str(), fase.c_str(), disegno.c_str());
			ChiudiLog();
		}
	} catch (...) {
	}
}

