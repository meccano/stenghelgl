//---------------------------------------------------------------------------

#ifndef DBH
#define DBH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <DB.hpp>
#include <map>
//---------------------------------------------------------------------------
typedef std::map<int, std::map<AnsiString, AnsiString> > TRecordList;
typedef std::map<AnsiString, std::map<AnsiString, AnsiString> > TIndexList;
//---------------------------------------------------------------------------
class TDBDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TADOConnection *ADOConnection1;
	TTimer *TimerDBCheck;
	TTimer *TimerClr;
	void __fastcall TimerDBCheckTimer(TObject *Sender);
	void __fastcall TimerClrTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TDBDataModule(TComponent* Owner);
	void CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList);
	void CaricaTabella(AnsiString TableName, TRecordList &RecList) ;
	void SalvaParametri(WORD *MW);
	void LeggiParametri(WORD *MW);
	void LeggiSegnalazioneAttiva(AnsiString &msg, int &all);
	void AcquisisciSegnalazioneAttiva(int nmsg);
	void AcquisisciSegnalazioni();
	void Segnalazione(int n, AnsiString msg, int all);
	void MemorizzaProgrammaPallet(int pallet, AnsiString prg);
	AnsiString LeggiProgrammaPallet(int pallet);
	void MemorizzaEsclusioneProgramma(int val);
	void MemorizzaEsclusioneM30(int val);
	int LeggiEsclusioneProgramma();
	int LeggiEsclusioneM30();
	void ClearHistory();
	void ImpostaStatoPallet(int pallet, int stato);
	int LeggiStatoPallet(int pallet);
	int LeggiPalletInMacchina();
	void ImpostaPalletInMacchina(int pallet, int InMacchina);
	void NessunPalletInMacchina();
	void SalvaProgramma(AnsiString prog);
	void EliminaProgramma(AnsiString prog);

};
//---------------------------------------------------------------------------
extern PACKAGE TDBDataModule *DBDataModule;
//---------------------------------------------------------------------------
Variant ReadField(TADOQuery *q, AnsiString f);
int ReadInt(TADOQuery *q, AnsiString f);
AnsiString ReadString(TADOQuery *q, AnsiString f);

#endif
